<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//public pages

Route::get('/', 'Front\ManagePageController@HomePage')->name('front.home');
Route::get('/tour/{id}', 'Front\ManagePageController@TourDetails')->name('front.details');
Route::get('/tour/by/city/{city}', 'Front\ManagePageController@GetTourbyCity')->name('front.by.city');
Route::get('/tour/by/theme/{theme}', 'Front\ManagePageController@GetTourbyTheme')->name('front.by.theme');
Route::get('/tour/by/tag/{tag}', 'Front\ManagePageController@GetTourbyTag')->name('front.by.tag');
Route::get('/tour/map/{id}', 'Front\ManagePageController@TourMapDetails')->name('front.map.id');
Route::get('/tours', 'Front\ManagePageController@GetAllTour')->name('front.all');
Route::post('/tours/collectleads', 'Front\ManagePageController@SaveLeadForm')->name('front.collect.leads');
Route::get('/about', 'Front\ManagePageController@About')->name('front.about');
Route::get('/contact', 'Front\ManagePageController@Contact')->name('front.contact');
Route::get('/terms', 'Front\ManagePageController@Terms')->name('front.terms');
Route::get('/privacy', 'Front\ManagePageController@Privacy')->name('front.pirvacy');
Route::get('/car/all', 'Front\ManagePageController@AllCars')->name('front.car.all');
Route::get('/car/all/{id}', 'Front\ManagePageController@CarDetails')->name('front.car.details');






Route::get('/tour', function () {
    return view('frontend.page.tourdetails');
});

Route::get('/adminuser', function () {
    return view('admin.pages.auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');




/// admin route
Route::prefix('admin')->group(function() {
    Route::get('/login', 'Admin\Auth\AdminLogin@showLoginForm')->name('admin.login');
    Route::post('/login', 'Admin\Auth\AdminLogin@login')->name('admin.login.submit');
    Route::get('/home', 'Admin\Auth\AdminController@index')->name('admin.home');

});


//city managment

Route::get('admin/city/create', 'Admin\City\ManageCity@Create')->name('admin.create.city');
Route::post('admin/city/create', 'Admin\City\ManageCity@Save')->name('admin.save.city');
Route::get('admin/city', 'Admin\City\ManageCity@Index')->name('admin.city.index');
Route::get('admin/city/{id}', 'Admin\City\ManageCity@Details')->name('admin.city.details');
Route::post('admin/city/update/{id}', 'Admin\City\ManageCity@Update')->name('admin.city.update');
Route::post('admin/city/add/gallery/{id}', 'Admin\City\ManageCity@UploadGallery')->name('admin.city.add.gallery');
Route::post('admin/city/update/sm/{id}', 'Admin\City\ManageCity@UpdatesmImg')->name('admin.city.update.sm.image');
Route::post('admin/city/update/bg/{id}', 'Admin\City\ManageCity@UpdateBGImg')->name('admin.city.update.bg.image');
Route::get('admin/city/gallery/delete/{id}', 'Admin\City\ManageCity@DeleteGallery')
    ->name('admin.city.delete.gallery');
Route::get('admin/city/api/datatables', 'Admin\City\ManageCity@DatatableApi')->name('admin.city.datatables');



// place

Route::get('admin/place/create', 'Admin\Places\ManagePlace@Create')->name('admin.create.place');
Route::post('admin/place/create', 'Admin\Places\ManagePlace@Save')->name('admin.save.place');
Route::get('admin/place', 'Admin\Places\ManagePlace@Index')->name('admin.place.index');
Route::get('admin/place/{id}', 'Admin\Places\ManagePlace@Details')->name('admin.place.details');
Route::post('admin/place/update/{id}', 'Admin\Places\ManagePlace@Update')->name('admin.place.update');
Route::post('admin/place/add/gallery/{id}', 'Admin\Places\ManagePlace@UploadGallery')->name('admin.place.add.gallery');
Route::post('admin/place/update/sm/{id}', 'Admin\Places\ManagePlace@UpdatesmImg')->name('admin.place.update.sm.image');
Route::post('admin/place/update/bg/{id}', 'Admin\Places\ManagePlace@UpdateBGImg')->name('admin.place.update.bg.image');
Route::get('admin/place/gallery/delete/{id}', 'Admin\Places\ManagePlace@DeleteGallery')
    ->name('admin.place.delete.gallery');
Route::get('admin/place/api/datatables', 'Admin\Places\ManagePlace@DatatableApi')->name('admin.place.datatables');





// cab

Route::get('admin/cab/create', 'Admin\Cab\ManageCab@Create')->name('admin.create.cab');
Route::post('admin/cab/create', 'Admin\Cab\ManageCab@Save')->name('admin.save.cab');
Route::get('admin/cab', 'Admin\Cab\ManageCab@Index')->name('admin.cab.index');
Route::get('admin/cab/{id}', 'Admin\Cab\ManageCab@Details')->name('admin.cab.details');
Route::post('admin/cab/update/{id}', 'Admin\Cab\ManageCab@Update')->name('admin.cab.update');
Route::post('admin/cab/add/gallery/{id}', 'Admin\Cab\ManageCab@UploadGallery')->name('admin.cab.add.gallery');
Route::post('admin/cab/update/sm/{id}', 'Admin\Cab\ManageCab@UpdatesmImg')->name('admin.cab.update.sm.image');
Route::post('admin/cab/update/bg/{id}', 'Admin\Cab\ManageCab@UpdateBGImg')->name('admin.cab.update.bg.image');
Route::get('admin/cab/gallery/delete/{id}', 'Admin\Cab\ManageCab@DeleteGallery')
    ->name('admin.cab.delete.gallery');
Route::get('admin/cab/api/datatables', 'Admin\Cab\ManageCab@DatatableApi')->name('admin.cab.datatables');



//hotel

Route::get('admin/hotel/create', 'Admin\Hotel\ManageHotel@Create')->name('admin.create.hotel');
Route::post('admin/hotel/create', 'Admin\Hotel\ManageHotel@Save')->name('admin.save.hotel');
Route::get('admin/hotel', 'Admin\Hotel\ManageHotel@Index')->name('admin.hotel.index');
Route::get('admin/hotel/{id}', 'Admin\Hotel\ManageHotel@Details')->name('admin.hotel.details');
Route::post('admin/hotel/update/{id}', 'Admin\Hotel\ManageHotel@Update')->name('admin.hotel.update');
Route::post('admin/hotel/add/gallery/{id}', 'Admin\Hotel\ManageHotel@UploadGallery')->name('admin.hotel.add.gallery');
Route::post('admin/hotel/update/sm/{id}', 'Admin\Hotel\ManageHotel@UpdatesmImg')->name('admin.hotel.update.sm.image');
Route::post('admin/hotel/update/bg/{id}', 'Admin\Hotel\ManageHotel@UpdateBGImg')->name('admin.hotel.update.bg.image');
Route::get('admin/hotel/gallery/delete/{id}', 'Admin\Hotel\ManageHotel@DeleteGallery')
    ->name('admin.hotel.delete.gallery');
Route::get('admin/hotel/api/datatables', 'Admin\Hotel\ManageHotel@DatatableApi')->name('admin.hotel.datatables');



//room

Route::get('admin/room/create/{id}', 'Admin\Hotel\ManageRoom@Create')->name('admin.create.room');
Route::post('admin/room/create', 'Admin\Hotel\ManageRoom@Save')->name('admin.save.room');
Route::get('admin/room', 'Admin\Hotel\ManageRoom@Index')->name('admin.room.index');
Route::get('admin/room/{id}', 'Admin\Hotel\ManageRoom@Details')->name('admin.room.details');
Route::post('admin/room/update/{id}', 'Admin\Hotel\ManageRoom@Update')->name('admin.room.update');
Route::post('admin/room/add/gallery/{id}', 'Admin\Hotel\ManageRoom@UploadGallery')->name('admin.room.add.gallery');
Route::post('admin/room/update/sm/{id}', 'Admin\Hotel\ManageRoom@UpdatesmImg')->name('admin.room.update.sm.image');
Route::post('admin/room/update/bg/{id}', 'Admin\Hotel\ManageRoom@UpdateBGImg')->name('admin.room.update.bg.image');
Route::get('admin/room/gallery/delete/{id}', 'Admin\Hotel\ManageRoom@DeleteGallery')
    ->name('admin.room.delete.gallery');
Route::get('admin/room/api/datatables', 'Admin\Hotel\ManageRoom@DatatableApi')->name('admin.room.datatables');



//tour

Route::get('admin/tour/create', 'Admin\Tour\ManageTour@Create')->name('admin.create.tour');
Route::post('admin/tour/create', 'Admin\Tour\ManageTour@Save')->name('admin.save.tour');
Route::post('admin/tag/create', 'Admin\Tour\ManageTour@CreateTagsave')->name('admin.save.tag');
Route::get('admin/tour', 'Admin\Tour\ManageTour@Index')->name('admin.tour.index');
Route::get('admin/tour/{id}', 'Admin\Tour\ManageTour@Details')->name('admin.tour.details');
Route::post('admin/tour/update/{id}', 'Admin\Tour\ManageTour@Update')->name('admin.tour.update');
Route::post('admin/tour/add/gallery/{id}', 'Admin\Tour\ManageTour@UploadGallery')->name('admin.tour.add.gallery');
Route::post('admin/tour/update/sm/{id}', 'Admin\Tour\ManageTour@UpdatesmImg')->name('admin.tour.update.sm.image');
Route::post('admin/tour/update/bg/{id}', 'Admin\Tour\ManageTour@UpdateBGImg')->name('admin.tour.update.bg.image');
Route::get('admin/tour/gallery/delete/{id}', 'Admin\Tour\ManageTour@DeleteGallery')
    ->name('admin.tour.delete.gallery');
Route::get('admin/tour/api/datatables', 'Admin\Tour\ManageTour@DatatableApi')->name('admin.tour.datatables');

Route::post('admin/tour/tag/create/{id}', 'Admin\Tour\ManageTour@CreateTagsave')->name('admin.save.tour.tag');
Route::get('admin/tour/tag/delete/{id}', 'Admin\Tour\ManageTour@CreateTagdel')->name('admin.delete.tour.tag');
Route::get('admin/tour/theme/delete/{id}', 'Admin\Tour\ManageTour@CreateThemedel')->name('admin.delete.tour.theme');
Route::post('admin/tour/theme/create/{id}', 'Admin\Tour\ManageTour@CreateThemesave')->name('admin.save.tour.theme');
Route::post('admin/tour/icon/create/{id}', 'Admin\Tour\ManageTour@CreateIconsave')->name('admin.save.tour.icon');
Route::get('admin/tour/icon/delete/{id}', 'Admin\Tour\ManageTour@CreateIcondel')->name('admin.delete.tour.icon');


Route::post('admin/tour/inclusion/create', 'Admin\Tour\ManageTour@CreateTourInclusion')
    ->name('admin.save.tour.inclusion');

Route::get('admin/tour/inclusion/delete/{id}', 'Admin\Tour\ManageTour@DeleteTourInclusion')
    ->name('admin.delete.tour.inclusion');
Route::post('admin/tour/exclusion/create', 'Admin\Tour\ManageTour@CreateTourExclusion')
    ->name('admin.save.tour.exclusion');

Route::get('admin/tour/exclusion/delete/{id}', 'Admin\Tour\ManageTour@DeleteTourExclusion')
    ->name('admin.delete.tour.exclusion');

Route::post('admin/tour/to/city/create/{id}', 'Admin\Tour\ManageTour@SaveTourToCity')
    ->name('admin.save.tour.to.city');

Route::get('admin/tour/to/city/delete/{id}', 'Admin\Tour\ManageTour@DeleteTourToCity')
    ->name('admin.delete.tour.tour.to.city');



////tour itinary

Route::get('admin/itinerary/create/{id}', 'Admin\Tour\ManageTourItinary@Create')->name('admin.create.itinerary');
Route::post('admin/itinerary/create', 'Admin\Tour\ManageTourItinary@Save')->name('admin.save.itinerary');
Route::get('admin/itinerary', 'Admin\Tour\ManageTourItinary@Index')->name('admin.itinerary.index');
Route::get('admin/itinerary/{id}', 'Admin\Tour\ManageTourItinary@Details')->name('admin.itinerary.details');
Route::post('admin/itinerary/update/{id}', 'Admin\Tour\ManageTourItinary@Update')->name('admin.itinerary.update');
Route::post('admin/itinerary/add/gallery/{id}', 'Admin\Tour\ManageTourItinary@UploadGallery')->name('admin.itinerary.add.gallery');
Route::post('admin/itinerary/update/sm/{id}', 'Admin\Tour\ManageTourItinary@UpdatesmImg')->name('admin.itinerary.update.sm.image');
Route::post('admin/itinerary/update/bg/{id}', 'Admin\Tour\ManageTourItinary@UpdateBGImg')->name('admin.itinerary.update.bg.image');
Route::get('admin/itinerary/delete/{id}', 'Admin\Tour\ManageTourItinary@DeleteIti')
    ->name('admin.itinerary.delete');


//tags

Route::get('admin/master/tags/create', 'Admin\Master\ManageTags@Create')->name('admin.create.master.tag');
Route::post('admin/master/tags/create', 'Admin\Master\ManageTags@Save')->name('admin.save.master.tag');
Route::get('admin/master/tags/api/datatables', 'Admin\Master\ManageTags@DatatableApi')->name('admin.master.tag.datatables');
Route::get('admin/master/tags/delete/{id}', 'Admin\Master\ManageTags@Delete')
    ->name('admin.master.tag.delete');


//Theme

Route::get('admin/master/theme/create', 'Admin\Master\ManageTheme@Create')->name('admin.create.master.theme');
Route::post('admin/master/theme/create', 'Admin\Master\ManageTheme@Save')->name('admin.save.master.theme');
Route::get('admin/master/theme/api/datatables', 'Admin\Master\ManageTheme@DatatableApi')->name('admin.master.theme.datatables');
Route::get('admin/master/theme/delete/{id}', 'Admin\Master\ManageTheme@Delete')
    ->name('admin.master.theme.delete');



//Icon

Route::get('admin/master/icon/create', 'Admin\Master\ManageIcon@Create')->name('admin.create.master.icon');
Route::get('admin/master/icon/reference', 'Admin\Master\ManageIcon@IconReference')->name('admin.reference.master.icon');
Route::post('admin/master/icon/create', 'Admin\Master\ManageIcon@Save')->name('admin.save.master.icon');
Route::get('admin/master/icon/api/datatables', 'Admin\Master\ManageIcon@DatatableApi')->name('admin.master.icon.datatables');
Route::get('admin/master/icon/delete/{id}', 'Admin\Master\ManageIcon@Delete')
    ->name('admin.master.icon.delete');


//slider

Route::get('admin/master/slider/delete/{id}', 'Admin\Master\ManageSlider@Delete')->name('admin.slider.master.delete');
Route::get('admin/master/slider', 'Admin\Master\ManageSlider@create')->name('admin.slider.master.create');
Route::post('admin/master/slider/create', 'Admin\Master\ManageSlider@Save')->name('admin.save.master.slider');


// customers

Route::get('admin/customer', 'Admin\Customer\ManageCustomer@Index')->name('admin.all.customer');
Route::get('admin/customer/create', 'Admin\Customer\ManageCustomer@Create')->name('admin.create.customer');
Route::get('admin/customer/details/{id}', 'Admin\Customer\ManageCustomer@Details')->name('admin.details.customer');
Route::post('admin/customer/create', 'Admin\Customer\ManageCustomer@Save')->name('admin.save.customer');
Route::get('admin/customer/api/datatables', 'Admin\Customer\ManageCustomer@DatatableApi')->name('admin.customer.datatables');
Route::get('admin/customer/to/order/api/datatables/{id}', 'Admin\Customer\ManageCustomer@DatatableApiOrders')->name('admin.customer.to.order.datatables');
Route::get('admin/customer/delete/{id}', 'Admin\Customer\ManageCustomer@Delete')
    ->name('admin.customer.delete');


// orders

Route::get('admin/order', 'Admin\Order\ManageOrder@Index')->name('admin.all.order');
Route::get('admin/order/api/datatable', 'Admin\Order\ManageOrder@DatatableApi')->name('admin.datatable.order');
Route::get('admin/order/details/{id}', 'Admin\Order\ManageOrder@Details')->name('admin.order.details');
Route::post('admin/order/estimate', 'Admin\Order\ManageOrder@CreateTourToOrder')->name('admin.order.estimate.save');
Route::get('admin/order/estimate/view/{id}', 'Admin\Order\ManageOrder@ViewEstimate')->name('admin.order.estimate.view');
Route::get('admin/order/estimate/downloadpdf/{id}', 'Admin\Order\ManageOrder@ViewPDF')->name('admin.order.estimate.pfd.download');
Route::get('admin/order/tourplan/downloadpdf/{id}', 'Admin\Order\ManageOrder@ViewTourPlan')
    ->name('admin.order.tour.plan.pfd.download');


