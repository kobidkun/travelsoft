<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItinariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itinaries', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable();
            $table->longText('description')->nullable();
            $table->longText('city')->nullable();
            $table->longText('day')->nullable();
            $table->longText('img')->nullable();
            $table->text('create_tour_id')->nullable();
            $table->longText('create_hotel_id')->nullable();
            $table->text('create_city_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itinaries');
    }
}
