<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateTourToItinaryToCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_tour_to_itinary_to_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->text('create_tour_to_itinary_id')->nullable();
            $table->text('create_tour_id')->nullable();
            $table->text('create_city_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_tour_to_itinary_to_cities');
    }
}
