<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerToToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_to_tours', function (Blueprint $table) {
            $table->increments('id');
            $table->text('customer_id')->nullable();
            $table->text('create_tour_id')->nullable();
            $table->text('tour_title')->nullable();
            $table->text('ip')->nullable();
            $table->text('adult')->nullable();
            $table->text('children')->nullable();
            $table->text('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_to_tours');
    }
}
