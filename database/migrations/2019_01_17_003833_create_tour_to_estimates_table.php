<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourToEstimatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_to_estimates', function (Blueprint $table) {
            $table->increments('id');
            $table->text('secure_url')->nullable();
            $table->text('customer_to_tour_id')->nullable();
            $table->text('customer_id')->nullable();
            $table->text('create_tour_id')->nullable();
            $table->text('customer_to_tours')->nullable();
            $table->text('tour_title')->nullable();
            $table->text('ip')->nullable();
            $table->text('adult')->nullable();
            $table->text('children')->nullable();
            $table->text('date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_to_estimates');
    }
}
