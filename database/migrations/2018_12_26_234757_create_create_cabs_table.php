<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateCabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_cabs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->text('description')->nullable();
            $table->text('type')->nullable();
            $table->text('capacity')->nullable();
            $table->text('segment')->nullable();
            $table->text('price')->nullable();
            $table->text('tax_per')->nullable();
            $table->text('cess_per')->nullable();
            $table->text('cess')->nullable();
            $table->text('tax')->nullable();
            $table->text('total')->nullable();
            $table->text('sm_img')->nullable();
            $table->text('bg_img')->nullable();
            $table->text('color')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_cabs');
    }
}
