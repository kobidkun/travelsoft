<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->text('create_city_id')->nullable();
            $table->text('create_hotel_id')->nullable();
            $table->text('name')->nullable();
            $table->text('room_capacity')->nullable();
            $table->text('star')->nullable();
            $table->text('description')->nullable();
            $table->text('price')->nullable();
            $table->text('tax')->nullable();
            $table->text('tax_per')->nullable();
            $table->text('cess')->nullable();
            $table->text('cess_per')->nullable();
            $table->text('total')->nullable();
            $table->text('rating')->nullable();
            $table->text('img_bg')->nullable();
            $table->text('img_sm')->nullable();
            $table->text('img_lg')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_rooms');
    }
}
