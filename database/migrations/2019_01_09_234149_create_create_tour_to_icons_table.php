<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateTourToIconsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_tour_to_icons', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('icon');
            $table->text('slug');
            $table->text('create_icon_id');
            $table->text('create_tour_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_tour_to_icons');
    }
}
