<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourToEstimateToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_to_estimate_to_items', function (Blueprint $table) {
            $table->increments('id');
            $table->text('tour_to_estimate_id');
            $table->text('customer_id')->nullable();
            $table->text('create_tour_id')->nullable();
            $table->text('item');
            $table->text('price');
            $table->text('tax')->nullable();
            $table->text('tax_per')->nullable();
            $table->text('qty');
            $table->text('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_to_estimate_to_items');
    }
}
