<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_tours', function (Blueprint $table) {
            $table->increments('id');
            $table->text('create_city_id')->nullable();
            $table->text('title')->nullable();
            $table->text('days')->nullable();
            $table->text('night')->nullable();
            $table->longText('description')->nullable();
            $table->text('price')->nullable();
            $table->text('tax_per')->nullable();
            $table->text('tax')->nullable();
            $table->text('cess')->nullable();
            $table->text('cess_per')->nullable();
            $table->text('total')->nullable();
            $table->text('img_bg')->nullable();
            $table->text('img_sm')->nullable();
            $table->text('img_lg')->nullable();
            $table->longText('inclusion')->nullable();
            $table->longText('exclusion')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_tours');
    }
}
