<?php

namespace App\Http\Controllers\Admin\Hotel;


use App\Model\Hotel\CreateHotel;
use App\Model\Hotel\Room\CreateRoom;
use App\Model\Hotel\Room\CreateRoomToImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ManageRoom extends Controller
{
    public function Create($id){


        $hotel = CreateHotel::findorfail($id);

        $linkedcity = $hotel->create_cities;

        return view('admin.pages.hotel.room.create')->with([
          'hotel' => $hotel,
            'linkedcity' => $linkedcity
        ]);

    }

    public function Index(){

        return view('admin.pages.room.index');

    }

    public function Save(Request $request){


        if ( $request->sm_img  === null ){
            return 'no Small image ';
        } else if ( $request->bg_img  === null ){
            return 'no HD image ';
        }


        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/room/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/room/hd', $imglocname);


        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/room/sm/'.$imglocsmname;
        $imglocsm = $request->sm_img->storeAs('public/images/room/sm',$imglocsmname);




        $a = new CreateRoom();

        $a->name = $request->name;
        $a->create_city_id = $request->create_city_id;
        $a->create_hotel_id = $request->create_hotel_id;
        $a->description = $request->description;
        $a->tax_per = $request->tax_per;
        $a->cess = $request->cess;
        $a->cess_per = $request->cess_per;
        $a->price = $request->price;
        $a->tax = $request->tax;
        $a->total = $request->total;
        $a->room_capacity = $request->room_capacity;
        $a->img_sm = $imglocsmnamepath;
        $a->img_bg = $imglocnamepath;
        $a->save();

        return back();

    }



    public function Details($id){

        $a = CreateRoom::find($id);

        $linkedcity = $a->create_cities;
        $hotel = $a->create_hotel;
        $images = CreateRoomToImage::where('create_room_id',$id)->get();
        $linkedplaces = $a->create_places;

        $allavlbleroom = CreateRoom::where('create_hotel_id',$hotel->id)->get();

      //  dd($allavlbleroom);






        return view('admin.pages.hotel.room.details')->with([
            'city' => $a,
            'linkedcity' => $linkedcity,
            'hotel' => $hotel,
            'images' => $images,
            'linkedplaces' => $linkedplaces,
            'avlroms' => $allavlbleroom,

        ]);

    }

    public function Edit($id){

    }

    public function Update($id,Request $request){

        $a =  CreateRoom::findorfail($id);

        $a->name = $request->name;
        $a->description = $request->description;
        $a->lat = $request->lat;
        $a->long = $request->long;
        $a->google_name = $request->google_name;
        $a->google_details = $request->google_details;

        $a->save();

        return back();

    }

    public function UpdatesmImg($id,Request $request){





        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/room/sm/'.$imglocsmname;
        $imglocsm = $request->sm_img->storeAs('public/images/room/sm',$imglocsmname);


        $a =  CreateRoom::findorfail($id);

        $a->img_sm = $imglocsmnamepath;


        $a->save();

        return back();

    }

    public function UpdateBGImg($id,Request $request){

        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/room/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/room/hd', $imglocname);


        $a =  CreateRoom::findorfail($id);

        $a->img_bg = $imglocnamepath;


        $a->save();

        return back();

    }

    public function Delete($id){

    }

    public function UploadGallery($id, Request $request){
        if ( $request->file  === null ){
            return 'no Small image ';
        }

        // bcrypt()



        $imglocname =  time().'-'.$request->file->getClientOriginalName();
        $imglocnamepath = '/images/room/gallery/'.$imglocname;

        $imgloc = $request->file->storeAs('public/images/room/gallery', $imglocname);


        $a = new CreateRoomToImage();

        $a->create_room_id = $id;
        $a->name = $imglocnamepath;
        $a->location = $imglocnamepath;
        $a->type = 'gallery';

        $a->save();



        return response([
            'message' => 'Image saved Successfully'
        ], 200);
    }


}
