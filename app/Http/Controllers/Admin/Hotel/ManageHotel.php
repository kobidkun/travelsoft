<?php

namespace App\Http\Controllers\Admin\Hotel;

use App\Model\City\CreateCity;
use App\Model\Hotel\CreateHotel;
use App\Model\Hotel\CreateHotelToImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ManageHotel extends Controller
{
    public function Create(){

$city = CreateCity::all();

        return view('admin.pages.hotel.create')->with([
          'cities' => $city
        ]);

    }

    public function Index(){

        return view('admin.pages.hotel.index');

    }

    public function Save(Request $request){


        if ( $request->sm_img  === null ){
            return 'no Small image ';
        } else if ( $request->bg_img  === null ){
            return 'no HD image ';
        }


        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/hotel/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/hotel/hd', $imglocname);


        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/hotel/sm/'.$imglocsmname;
        $imglocsm = $request->sm_img->storeAs('public/images/hotel/sm',$imglocsmname);




        $a = new CreateHotel();

        $a->name = $request->name;
        $a->create_place_id = $request->create_place_id;
        $a->address = $request->address;
        $a->g_url = $request->g_url;
        $a->g_place_id = $request->g_place_id;
        $a->phone = $request->phone;
        $a->google_name = $request->google_name;
        $a->star = $request->star;
        $a->description = $request->description;
        $a->g_data = $request->g_data;
        $a->g_star = $request->g_star;
        $a->tax_per = $request->tax_per;
        $a->cess = $request->cess;
        $a->cess_per = $request->cess_per;
        $a->price = $request->price;
        $a->tax = $request->tax;
        $a->total = $request->total;
        $a->lat = $request->lat;
        $a->long = $request->long;
        $a->create_city_id = $request->create_city_id;
        $a->img_sm = $imglocsmnamepath;
        $a->img_bg = $imglocnamepath;
        $a->save();

        return back();

    }



    public function Details($id){

        $a = CreateHotel::find($id);

        $allcity = CreateCity::all();

        $b = $a->create_hotel_to_images;
        $linkedcity = $a->create_cities;
        $linkedplaces = $a->create_places;
        $rooms = $a->create_rooms;

      //  dd($linkedplaces);

        return view('admin.pages.hotel.details')->with([
            'city' => $a,
            'images' => $b,
            'linkedcity' =>$linkedcity,
            'linkedplaces' =>$linkedplaces,
            'allcity' => $allcity,
            'rooms' => $rooms
        ]);

    }

    public function Edit($id){

    }

    public function Update($id,Request $request){

        $a =  CreateHotel::findorfail($id);

        $a->name = $request->name;
        $a->description = $request->description;
        $a->lat = $request->lat;
        $a->long = $request->long;
        $a->google_name = $request->google_name;
        $a->google_details = $request->google_details;

        $a->save();

        return back();

    }

    public function UpdatesmImg($id,Request $request){





        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/hotel/sm/'.$imglocsmname;
        $imglocsm = $request->sm_img->storeAs('public/images/hotel/sm',$imglocsmname);


        $a =  CreateHotel::findorfail($id);

        $a->img_sm = $imglocsmnamepath;


        $a->save();

        return back();

    }

    public function UpdateBGImg($id,Request $request){

        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/hotel/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/hotel/hd', $imglocname);


        $a =  CreateHotel::findorfail($id);

        $a->img_bg = $imglocnamepath;


        $a->save();

        return back();

    }

    public function Delete($id){

    }

    public function UploadGallery($id, Request $request){
        if ( $request->file  === null ){
            return 'no Small image ';
        }

        // bcrypt()



        $imglocname =  time().'-'.$request->file->getClientOriginalName();
        $imglocnamepath = '/images/hotel/gallery/'.$imglocname;

        $imgloc = $request->file->storeAs('public/images/hotel/gallery', $imglocname);


        $a = new CreateHotelToImages();

        $a->create_hotel_id = $id;
        $a->name = $imglocnamepath;
        $a->location = $imglocnamepath;
        $a->type = 'gallery';

        $a->save();



        return response([
            'message' => 'Image saved Successfully'
        ], 200);
    }


    public function DatatableApi(){



        $users = CreateHotel::select([
            'id',
            'price',
            'star',
            'name',
            'create_city_id',

        ]);


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-primary" href="'.route('admin.hotel.details',$user->id).'"></i> Details</a>';
            })

            ->addColumn('create_city_id', function ($user) {
                $a = CreateCity::find($user->create_city_id)->name;
                return $a;
            })

            ->make(true);






    }
}
