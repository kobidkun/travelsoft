<?php

namespace App\Http\Controllers\Admin\Master;

use App\Model\Master\CreateIcon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ManageIcon extends Controller
{
    public function create(){




        return view('admin.pages.master.icon.create')->with([

        ]);

    }

    public function IconReference(){




        return view('admin.pages.master.icon.iconreference')->with([

        ]);

    }

    public function Save(Request $request){

        $a = new CreateIcon();

        $a->title = $request->title;
        $a->icon = $request->icon;
        $a->slug = $request->tag.time();

        $a->save();

        return back();







    }


    public function Delete($id){

        $a = CreateIcon::findorfail($id);

        $a->delete();

        return back();





    }

    public function DatatableApi(){



        $users = CreateIcon::select([
            'id',
            'title',
            'icon',

        ]);


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-danger" href="'.route('admin.master.icon.delete',$user->id).'"></i> Delete</a>';
            })

            ->editColumn('icon', function ($user) {
                return '<i style="size: 100px" class="'.$user->icon.'" ></i>';
            })
            ->rawColumns(['action', 'icon'])

            ->make(true);






    }
}
