<?php

namespace App\Http\Controllers\Admin\Master;

use App\Model\Master\CreateTheme;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;


class ManageTheme extends Controller
{
    public function create(){




        return view('admin.pages.master.theme.create')->with([

        ]);

    }

    public function Save(Request $request){

        $a = new CreateTheme();

        $a->theme = $request->theme;
        $a->slug = $request->tag.time();

        $a->save();

        return back();







    }


    public function Delete($id){

        $a = CreateTheme::findorfail($id);

        $a->delete();

        return back();





    }

    public function DatatableApi(){



        $users = CreateTheme::select([
            'id',
            'theme',

        ]);


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-danger" href="'.route('admin.master.tag.delete',$user->id).'"></i> Delete</a>';
            })

            ->make(true);






    }
}
