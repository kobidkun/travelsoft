<?php

namespace App\Http\Controllers\Admin\Master;

use App\Model\Master\CreateTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;


class ManageTags extends Controller
{
    public function create(){




        return view('admin.pages.master.tag.create')->with([

        ]);

    }

    public function Save(Request $request){

        $a = new CreateTag();

        $a->tag = $request->tag;
        $a->slug = $request->tag.time();

        $a->save();

        return back();







    }


    public function Delete($id){

        $a = CreateTag::findorfail($id);

        $a->delete();

        return back();





    }

    public function DatatableApi(){



        $users = CreateTag::select([
            'id',
            'tag',

        ]);


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-danger" href="'.route('admin.master.tag.delete',$user->id).'"></i> Delete</a>';
            })

            ->make(true);






    }
}
