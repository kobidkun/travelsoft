<?php

namespace App\Http\Controllers\Admin\Master;

use App\Master\CreateSlider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;


class ManageSlider extends Controller
{
    public function create(){


        $all = CreateSlider::all();




        return view('admin.pages.master.Slider.create')->with([

            'sliders' => $all
        ]);

    }

    // apiKey=myapikey&filename=my-image.jpg&timestamp=123456789

    function createSignature($myapikey, $filename, $timestamp){
        $signature = 'apiKey='.$myapikey.'&filename='.$filename.'&timestamp='.$timestamp;

        $signature = hash_hmac("sha1", $signature,  true);

        return $signature;
    }




    public function Save(Request $request){

        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/hotel/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/hotel/hd', $imglocname);








        $a = new CreateSlider();

        $a->title = $request->title;
        $a->desc = $request->desc;
        $a->url = $request->url;
        $a->imgurl = $imglocnamepath;
        $a->cdnurl = $imglocnamepath;
        $a->location = $imglocnamepath;

        $a->save();

        return back();







    }


    public function Delete($id){

        $a = CreateSlider::findorfail($id);

        $a->delete();

        return back();





    }


}
