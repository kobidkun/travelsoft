<?php

namespace App\Http\Controllers\Admin\Cab;

use App\Model\Cab\CreateCab;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;


class ManageCab extends Controller
{
    public function Create(){

        return view('admin.pages.cab.create');

    }

    public function Index(){

        return view('admin.pages.cab.index');

    }

    public function Save(Request $request){


        if ( $request->sm_img  === null ){
            return 'no Small image ';
        } else if ( $request->bg_img  === null ){
            return 'no HD image ';
        }


        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/cab/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/cab/hd', $imglocname);


        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/cab/sm/'.$imglocsmname;
        $imglocsm = $request->sm_img->storeAs('public/images/cab/sm',$imglocsmname);




        $a = new CreateCab();

        $a->name = $request->name;
        $a->description = $request->description;
        $a->type = $request->type;
        $a->capacity = $request->capacity;
        $a->segment = $request->segment;
        $a->price = $request->price;
        $a->tax = $request->tax;
        $a->tax_per = $request->tax_per;
        $a->cess_per = $request->cess_per;
        $a->cess = $request->cess;
        $a->total = $request->total;
        $a->sm_img = $imglocsmnamepath;
        $a->bg_img = $imglocnamepath;
        $a->color = $request->color;
        $a->save();

        return back();

    }



    public function Details($id){

        $a = CreateCab::find($id);

        $b = $a->create_city_to_images;
        $places = $a->create_places;

        //dd($b);

        return view('admin.pages.cab.details')->with([
            'city' => $a,
            'images' => $b,
            'places' => $places
        ]);

    }

    public function Edit($id){

    }

    public function Update($id,Request $request){

        $a =  CreateCab::findorfail($id);

        $a->name = $request->name;
        $a->description = $request->description;
        $a->type = $request->type;
        $a->capacity = $request->capacity;
        $a->segment = $request->segment;
        $a->price = $request->price;
        $a->tax = $request->tax;
        $a->tax_per = $request->tax_per;
        $a->cess_per = $request->cess_per;
        $a->cess = $request->cess;
        $a->total = $request->total;

        $a->save();

        return back();

    }

    public function UpdatesmImg($id,Request $request){





        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/cab/sm/'.$imglocsmname;
        $imglocsm = $request->sm_img->storeAs('public/images/cab/sm',$imglocsmname);


        $a =  CreateCab::findorfail($id);

        $a->sm_img = $imglocsmnamepath;


        $a->save();

        return back();

    }

    public function UpdateBGImg($id,Request $request){

        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/cab/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/cab/hd', $imglocname);


        $a =  CreateCab::findorfail($id);

        $a->bg_img = $imglocnamepath;


        $a->save();

        return back();

    }

    public function Delete($id){

    }



    public function DatatableApi(){



        $users = CreateCab::select([
            'id',
            'name',
            'capacity',
            'price',
            'sm_img',
        ]);

        //src=".asset('.'/storage/'.$user->sm_img.')"


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-primary" href="'.route('admin.cab.details',$user->id).'"></i> Details</a>';
            })

            ->editColumn('sm_img', function ($user) {
                return '<img class="tableimage" src="'.asset('/storage'.$user->sm_img).'"/>';
            })
            ->rawColumns(['action', 'sm_img'])

            ->make(true);






    }
}
