<?php

namespace App\Http\Controllers\Admin\Tour;

use App\Model\City\CreateCity;
use App\Model\Hotel\CreateHotel;
use App\Model\Master\CreateIcon;
use App\Model\Master\CreateTag;
use App\Model\Master\CreateTheme;
use App\Model\Tour\CreateTour;
use App\Model\Tour\CreateTourToCity;
use App\Model\Tour\CreateTourToExclusion;
use App\Model\Tour\CreateTourToIcon;
use App\Model\Tour\CreateTourToImage;
use App\Model\Tour\CreateTourToInclusion;
use App\Model\Tour\CreateTourToTag;
use App\Model\Tour\CreateTourToTheme;
use App\Model\Tour\Itinary\CreateTourToItinary;
use Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ManageTour extends Controller
{
    public function Create(){

        $city = CreateCity::all();

        return view('admin.pages.tour.create')->with([
            'cities' => $city
        ]);

    }

    public function Index(){

        return view('admin.pages.tour.index');

    }

    public function Save(Request $request){


        if ( $request->sm_img  === null ){
            return 'no Small image ';
        } else if ( $request->bg_img  === null ){
            return 'no HD image ';
        }


        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/tour/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/tour/hd', $imglocname);


        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/tour/sm/'.$imglocsmname;
        $imglocsm = $request->sm_img->storeAs('public/images/tour/sm',$imglocsmname);




        $a = new CreateTour();

        $a->title = $request->title;
        $a->description = $request->description;
        $a->tax_per = $request->tax_per;
        $a->days = $request->days;
        $a->night = $request->night;
        $a->cess = $request->cess;
        $a->cess_per = $request->cess_per;
        $a->price = $request->price;
        $a->tax = $request->tax;
        $a->total = $request->total;
        $a->create_city_id = $request->create_city_id;
        $a->img_sm = $imglocsmnamepath;
        $a->img_bg = $imglocnamepath;
        $a->save();

        return back();

    }






    public function Details($id){

        $a = CreateTour::find($id);

        $allcity = CreateCity::all();

        $allthemes = CreateTheme::all();

        $b = $a->create_tour_to_images;
        $linkedcity = $a->create_cities;
        $itinaey = $a->create_tour_to_itinaries;



        $hotel = $a->create_hotels;

        $mappoints = $a->create_tour_to_places;
        $tourInclusion = $a->create_tour_to_inclusions;
        $tourExclusion = $a->create_tour_to_exclusions;
        $touricons = $a->create_tour_to_icons;
        $tourtags = $a->create_tour_to_tags;
        $tourtocities = $a->create_tour_to_cities;
        $tourtothemes = $a->create_tour_to_themes;

     //   dd($tourtocity);




       //

        $collection = collect($itinaey);

        $multiplied = $collection->map(function ($item, $key) {
            return $item->create_hotel_id;
        });

        $multiplied->all();

        $models = CreateHotel::findMany($multiplied);






        $allTags = CreateTag::all();
        $allIcons = CreateIcon::all();





        // return response()->json($models);

        return view('admin.pages.tour.details')->with([
            'city' => $a,
            'images' => $b,
            'linkedcity' =>$linkedcity,
            'allcity' => $allcity,
            'itineraries' => $itinaey,
            'places' => $mappoints,
            'hotels' => $models,
            'tourinclusions' => $tourInclusion,
            'tourexclusions' => $tourExclusion,
            'alltags' => $allTags,
            'allicons' => $allIcons,
            'touricons' => $touricons,
            'tourtags' => $tourtags,
            'tourtocities' => $tourtocities,
            'allthemes' => $allthemes,
            'tourtothemes' => $tourtothemes,

        ]);

    }

    public function Edit($id){

    }

    public function Update($id,Request $request){




        $a =  CreateTour::findorfail($id);

        $a->title = $request->title;
        $a->description = $request->description;
        $a->tax_per = $request->tax_per;
        $a->days = $request->days;
        $a->night = $request->night;
        $a->cess = $request->cess;
        $a->cess_per = $request->cess_per;
        $a->price = $request->price;
        $a->tax = $request->tax;
        $a->total = $request->total;
        $a->create_city_id = $request->create_city_id;

        $a->save();

        return back();


    }

    public function UpdatesmImg($id,Request $request){





        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/tour/sm/'.$imglocsmname;

        $imglocsm = $request->sm_img->storeAs('public/images/tour/sm',$imglocsmname);











       //


        $a =  CreateTour::findorfail($id);

        $a->img_sm = $imglocsmnamepath;


       $a->save();

        return back();

    }

    public function UpdateBGImg($id,Request $request){

        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/tour/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/tour/hd', $imglocname);


        $a =  CreateTour::findorfail($id);

        $a->img_bg = $imglocnamepath;


        $a->save();

        return back();

    }

    public function Delete($id){

    }

    public function DeleteGallery($id){
        $a = CreateTourToImage::find($id);

        $a->delete();
        return back();
    }

    public function UploadGallery($id, Request $request){
        if ( $request->file  === null ){
            return 'no Small image ';
        }

        // bcrypt()



        $imglocname =  time().'-'.$request->file->getClientOriginalName();
        $imglocnamepath = '/images/tour/gallery/'.$imglocname;

        $imgloc = $request->file->storeAs('public/images/tour/gallery', $imglocname);


        $a = new CreateTourToImage();

        $a->create_tour_id = $id;
        $a->name = $imglocnamepath;
        $a->location = $imglocnamepath;
        $a->type = 'gallery';

        $a->save();



        return response([
            'message' => 'Image saved Successfully'
        ], 200);
    }


    public function CreateTourInclusion(Request $request){
        $a = new CreateTourToInclusion();
        $a->create_tour_id = $request->create_tour_id;
        $a->text = $request->text;
        $a->save();
        return redirect(route('admin.tour.details',$request->create_tour_id));
    }

    public function DeleteTourInclusion($id){

        $a = CreateTourToInclusion::findorfail($id);
        $a->delete();

        return redirect(route('admin.tour.details',$id));
    }

    public function CreateTourExclusion(Request $request){
        $a = new CreateTourToExclusion();
        $a->create_tour_id = $request->create_tour_id;
        $a->text = $request->text;
        $a->save();
        return redirect(route('admin.tour.details',$request->create_tour_id));
    }

    public function DeleteTourExclusion($id){
        $a = CreateTourToExclusion::findorfail($id);
        $a->delete();

        return redirect(route('admin.tour.details',$id));

    }

    public function CreateTagsave($id, Request $request){

        $tags = $request->tagid;
        $tourid = $id;

        foreach ($tags as $tag){

            $findtag = CreateTag::findorfail($tag);

            $savenewtag = new CreateTourToTag();

            $savenewtag->tag = $findtag->tag;
            $savenewtag->slug = $findtag->slug;
            $savenewtag->create_tag_id = $findtag->id;
            $savenewtag->create_tour_id = $tourid;

            $savenewtag->save();

        }

        return redirect(route('admin.tour.details',$tourid));



    }



    public function CreateTagdel($id){


        $a = CreateTourToTag::findorfail($id);

        $a->delete();



        return back();



    }


    public function CreateThemesave(Request $request){

        $tags = $request->themeid;
        $tourid = $request->id;

        foreach ($tags as $tag){

            $findtag = CreateTheme::findorfail($tag);

            $savenewtag = new CreateTourToTheme();

            $savenewtag->theme = $findtag->theme;
            $savenewtag->slug = $findtag->slug;
            $savenewtag->create_theme_id = $findtag->id;
            $savenewtag->create_tour_id = $tourid;

            $savenewtag->save();

        }

        return redirect(route('admin.tour.details',$tourid));



    }


    public function CreateThemedel($id){

        $a = CreateTourToTheme::findorfail($id);

        $a->delete();



        return back();



    }


    public function CreateIconsave($id, Request $request){

        $tags = $request->iconid;
        $tourid = $id;

        foreach ($tags as $tag){

            $findtag = CreateIcon::findorfail($tag);

            $savenewtag = new CreateTourToIcon();

            $savenewtag->icon = $findtag->icon;
            $savenewtag->title = $findtag->title;
            $savenewtag->slug = $findtag->slug;
            $savenewtag->create_icon_id = $findtag->id;
            $savenewtag->create_tour_id = $tourid;

            $savenewtag->save();

        }

        return redirect(route('admin.tour.details',$tourid));



    }




    public function CreateIcondel($id){

        $a = CreateIcon::findorfail($id);

        $a->delete();



        return back();



    }

    public function SaveTourToCity($id, Request $request){
        $cities= $request->cityid;

       // dd($cities);
        $tourid = $id;








        foreach ($cities as $tag){

            $findtag = CreateCity::findorfail($tag);

            $savenewtag = new CreateTourToCity();

            $savenewtag->name = $findtag->name;
            $savenewtag->description = $findtag->description;
            $savenewtag->lat = $findtag->lat;
            $savenewtag->long = $findtag->long;
            $savenewtag->google_name = $findtag->google_name;
            $savenewtag->google_details = $findtag->google_details;
            $savenewtag->bg_img = $findtag->bg_img;
            $savenewtag->sm_img = $findtag->sm_img;
            $savenewtag->is_featured = $findtag->is_featured;
            $savenewtag->color = $findtag->color;
            $savenewtag->create_tour_id = $tourid;

            $savenewtag->save();

        }

        return redirect(route('admin.tour.details',$tourid));

    }

    public function DeleteTourToCity($id){

        $a = CreateTourToCity::findorfail($id);
            $a->delete();

            return back();


    }




    public function DatatableApi(){



        $users = CreateTour::select([
            'id',
            'title',
            'price',
            'days',
            'create_city_id',

        ]);


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-primary" href="'.route('admin.tour.details',$user->id).'"></i> Details</a>';
            })

            ->addColumn('create_city_id', function ($user) {
                $a = CreateCity::find($user->create_city_id)->name;
                return $a;
            })

            ->make(true);






    }
}
