<?php

namespace App\Http\Controllers\Admin\Tour;

use App\Model\City\CreateCity;
use App\Model\Hotel\CreateHotel;
use App\Model\Master\CreateIcon;
use App\Model\Place\CreatePlace;
use App\Model\Tour\CreateTour;
use App\Model\Tour\Itinary\CreateTourToItinary;
use App\Model\Tour\Itinary\CreateTourToItinaryToInclusion;
use App\Model\Tour\Itinary\CreateTourToItinaryToPlace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageTourItinary extends Controller
{
    public function Create($id){


        $tour = CreateTour::findorfail($id);

        $hotel = CreateHotel::all();
        $allicons = CreateIcon::all();

        $linkedcity = $tour->create_cities;
        $allPlaces = CreatePlace::all();
        $allcity = CreateCity::all();
        $itinaries = $tour->create_tour_to_itinaries;
        $itiicons = $tour->create_tour_to_itinary_to_inclusions;

        return view('admin.pages.tour.itinary.create')->with([
            'hotels' => $hotel,
            'tour' => $tour,
            'linkedcity' => $linkedcity,
                'places' =>$allPlaces,
            'allcities' => $allcity,
            'itinaries' => $itinaries,
            'allicons' => $allicons,
            'itiicons' => $itiicons
        ]);

    }

    public function DeleteIti($id){

        $a = CreateTourToItinary::find($id);

        $a->delete();
        return back();



    }

    public function Index(){

        return view('admin.pages.room.index');

    }

    public function Save(Request $request){

       // dd($request);


        if ( $request->sm_img  === null ){
            return 'no Small image ';
        } else if ( $request->bg_img  === null ){
            return 'no HD image ';
        }


        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/room/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/room/hd', $imglocname);


        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/room/sm/'.$imglocsmname;
        $imglocsm = $request->sm_img->storeAs('public/images/room/sm',$imglocsmname);




        $a = new CreateTourToItinary();

        $a->title = $request->title;

        $a->description = $request->description;
        $a->city = $request->city;
        $a->day = $request->day;
        $a->img = $imglocsmnamepath;


        $a->create_tour_id = $request->create_tour_id;
        $a->create_hotel_id = $request->create_hotel_id;
        $a->create_city_id = $request->create_city_id;


        $a->save();


        //create place

        $places = $request->placeid;

        foreach ($places as $place){
            $b = new CreateTourToItinaryToPlace();
            $b->create_place_id = $place;
            $b->create_tour_id = $request->create_tour_id;
            $b->create_tour_to_itinary_id = $a->id;
            $b->create_city_id = $request->create_city_id;

            $b->save();
        }


        $activities = $request->activities;

      //  $tags = $request->iconid;
      //  $tourid = $id;

        foreach ($activities as $activitie){

            $findtag = CreateIcon::findorfail($activitie);

            $savenewtag = new CreateTourToItinaryToInclusion();

            $savenewtag->text = $findtag->icon;

            $savenewtag->create_tour_to_itinary_id = $a->id;
            $savenewtag->create_tour_id = $request->create_tour_id;






            $savenewtag->save();

        }






        return back();

    }



    public function Details($id){

        $a = CreateRoom::find($id);

        $linkedcity = $a->create_cities;
        $hotel = $a->create_hotel;
        $images = CreateRoomToImage::where('create_room_id',$id)->get();
        $linkedplaces = $a->create_places;

        $allavlbleroom = CreateRoom::where('create_hotel_id',$hotel->id)->get();

        //  dd($allavlbleroom);






        return view('admin.pages.hotel.room.details')->with([
            'city' => $a,
            'linkedcity' => $linkedcity,
            'hotel' => $hotel,
            'images' => $images,
            'linkedplaces' => $linkedplaces,
            'avlroms' => $allavlbleroom,

        ]);

    }

    public function Edit($id){

    }

    public function Update($id,Request $request){

        $a =  CreateRoom::findorfail($id);

        $a->name = $request->name;
        $a->description = $request->description;
        $a->lat = $request->lat;
        $a->long = $request->long;
        $a->google_name = $request->google_name;
        $a->google_details = $request->google_details;

        $a->save();

        return back();

    }

    public function UpdatesmImg($id,Request $request){





        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/room/sm/'.$imglocsmname;
        $imglocsm = $request->sm_img->storeAs('public/images/room/sm',$imglocsmname);


        $a =  CreateRoom::findorfail($id);

        $a->img_sm = $imglocsmnamepath;


        $a->save();

        return back();

    }

    public function UpdateBGImg($id,Request $request){

        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/room/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/room/hd', $imglocname);


        $a =  CreateRoom::findorfail($id);

        $a->img_bg = $imglocnamepath;


        $a->save();

        return back();

    }

    public function Delete($id){

    }

    public function UploadGallery($id, Request $request){
        if ( $request->file  === null ){
            return 'no Small image ';
        }

        // bcrypt()



        $imglocname =  time().'-'.$request->file->getClientOriginalName();
        $imglocnamepath = '/images/room/gallery/'.$imglocname;

        $imgloc = $request->file->storeAs('public/images/room/gallery', $imglocname);


        $a = new CreateRoomToImage();

        $a->create_room_id = $id;
        $a->name = $imglocnamepath;
        $a->location = $imglocnamepath;
        $a->type = 'gallery';

        $a->save();



        return response([
            'message' => 'Image saved Successfully'
        ], 200);
    }
}
