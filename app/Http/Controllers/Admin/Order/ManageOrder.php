<?php

namespace App\Http\Controllers\Admin\Order;

use App\Model\City\CreateCity;
use App\Model\Customer\Customer;
use App\Model\Customer\CustomerToTours;
use App\Model\Customer\Tour\TourToEstimate;
use App\Model\Customer\Tour\TourToEstimateToItem;
use App\Model\Hotel\CreateHotel;
use App\Model\Master\CreateIcon;
use App\Model\Place\CreatePlace;
use App\Model\Tour\CreateTour;
use App\Model\Tour\Itinary\CreateTourToItinary;
use App\Model\Tour\Itinary\CreateTourToItinaryToInclusion;
use App\Model\Tour\Itinary\CreateTourToItinaryToPlace;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;


class ManageOrder extends Controller
{
    public function Index(){
        return view('admin.pages.order.index');
    }

    public function Details($id){



        $customertotour = CustomerToTours::findorfail($id);

        $customer = $customertotour->customers;


        $tour = $customertotour->create_tours;

        $hotel = CreateHotel::all();
        $allicons = CreateIcon::all();

        $linkedcity = $tour->create_cities;
        $allPlaces = CreatePlace::all();
        $allcity = CreateCity::all();
        $itinaries = $tour->create_tour_to_itinaries;
        $itiicons = $tour->create_tour_to_itinary_to_inclusions;
        $tour_to_estimates = $customertotour->tour_to_estimates;

    //  return $tour_to_estimates;

        return view('admin.pages.order.details')->with([
            'hotels' => $hotel,
            'tour' => $tour,
            'linkedcity' => $linkedcity,
            'places' =>$allPlaces,
            'allcities' => $allcity,
            'itinaries' => $itinaries,
            'allicons' => $allicons,
            'itiicons' => $itiicons,
            'customer' => $customer,
            'customertotour' => $customertotour,
            'tour_to_estimates' => $tour_to_estimates,
        ]);

    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $time = time();
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString.$time;
    }

    public function CreateTourToOrder(Request $request){
        $createestimate = new TourToEstimate();

        $createestimate->secure_url = $this->generateRandomString(25);
        $createestimate->customer_id = $request->customer_id;
        $createestimate->create_tour_id = $request->create_tour_id;
        $createestimate->tour_title = $request->tour_title;
        $createestimate->customer_to_tours = $request->customer_to_tours;
        $createestimate->ip = $request->ip;
        $createestimate->adult = '0';
        $createestimate->children = '0';
        $createestimate->date = $request->date;
        $createestimate->save();


        $createadult = new TourToEstimateToItem();

        $createadult->tour_to_estimate_id = $createestimate->id;
        $createadult->customer_id = $request->customer_id;
        $createadult->create_tour_id = $request->create_tour_id;
        $createadult->item = 'Adult';
        $createadult->price = $request->adult_base_price;
        $createadult->tax_per = $request->adult_tax_per;
        $createadult->tax = $request->adult_tax;
        $createadult->qty = $request->adult_person;
        $createadult->total = $request->adult_total;

        $createadult->save();


        $createadult = new TourToEstimateToItem();

        $createadult->tour_to_estimate_id = $createestimate->id;
        $createadult->customer_id = $request->customer_id;
        $createadult->create_tour_id = $request->create_tour_id;
        $createadult->item = 'children';
        $createadult->price = $request->children_base_price;
        $createadult->tax_per = $request->children_tax_per;
        $createadult->tax = $request->children_tax;
        $createadult->qty = $request->children_person;
        $createadult->total = $request->children_total;

        $createadult->save();

        return back();





    }


    public function ViewEstimate($id){

        $est = TourToEstimate::find($id);

        $customer = $est->customers;

       // return $customer;

        //sum



        $items = $est->tour_to_estimate_to_items;



        $sum  = 0;
        foreach($items as $item)
        {
            $sum+= $item->total;
        }
       // echo $sum;

      //  return $sum;







        return view('admin.pages.order.estimate.estimate')->with([
            'est' => $est,
            'items' => $items,
            'customer' => $customer,
            'sum' => $sum,
        ]);
    }


    public function ViewPDF($id){

        $est = TourToEstimate::find($id);

        $customer = $est->customers;

       // return $customer;

        //sum



        $items = $est->tour_to_estimate_to_items;



        $sum  = 0;
        foreach($items as $item)
        {
            $sum+= $item->total;
        }
       // echo $sum;

      //  return $sum;

        $tour = $est->create_tour;

        $create_tour_to_itinaries = $tour->create_tour_to_itinaries;




        $pdf = PDF::loadView('admin.pages.order.estimate.estimate',[
            'est' => $est,
            'items' => $items,
            'customer' => $customer,
            'sum' => $sum,
            'tour' => $tour,
            'create_tour_to_itinaries' => $create_tour_to_itinaries,
        ])->setPaper('a3');

        return $pdf->download('invoice.pdf');





    }


    public function ViewTourPlan($id){

        $est = TourToEstimate::find($id);

        $tour = $est->create_tour;

        $create_tour_to_itinaries = $tour->create_tour_to_itinaries;

        $customer = $est->customers;

      //  return $tour;

        /*


                return view('admin.pages.order.estimate.tourplan')->with([
                    'tour' => $tour,
                    'est' => $est,
                    'customer' => $customer,
                    'create_tour_to_itinaries' => $create_tour_to_itinaries,
                ]);


        // */

                $pdf = PDF::loadView('admin.pages.order.estimate.tourplan',[
                    'tour' => $tour,
                    'est' => $est,
                    'customer' => $customer,
                    'create_tour_to_itinaries' => $create_tour_to_itinaries,

                ])->setPaper('a4');

                return $pdf->download('invoice.pdf');




    }





    public function DatatableApi(){




        $users = CustomerToTours::select([
            'id',
            'create_tour_id',
            'customer_id',
            'tour_title',
            'ip',
            'adult',
            'children',
            'date',
            'created_at'

        ]);


        return Datatables::of($users)


            ->addColumn('details', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-primary" href="'.route('admin.order.details',$user->id).'"></i> Details</a>';
            })

            ->addColumn('customer_name', function ($user) {
                $cus = Customer::find($user->customer_id);
                return $cus->fname. ' '. $cus->lname;
            })

            ->addColumn('customer_email', function ($user) {
                $cus = Customer::find($user->customer_id);
                return $cus->email;
            })


            ->addColumn('customer_phone', function ($user) {
                $cus = Customer::find($user->customer_id);
                return $cus->phone;
            })


            ->rawColumns(['customer_id','details'])

            ->make(true);







    }
}
