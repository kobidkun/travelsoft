<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Charts\Chart\LastMonthOrder;
use App\Model\Customer\Customer;
use App\Model\Customer\CustomerToTours;
use App\Model\Tour\CreateTour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $countorder = CreateTour::withCount(['CountOrders'])
          //  ->orderBy('votes_count')
            ->get();

        $data3 = CreateTour::withCount(['CountOrders'])
            //  ->orderBy('votes_count')
            ->get()




            ->map(function ($item) {

                // Return the number of persons with that age

                return ($item->count_orders_count) ;
            });


       // return $countorder;



        $data = collect([]); // Could also be an array

        for ($days_backwards = 30; $days_backwards >= 0; $days_backwards--) {
            // Could also be an array_push if using an array rather than a collection.
            $data->push(CustomerToTours::whereDate('created_at', today()->subDays($days_backwards))->count());
        }
        /*

                $data2 = CustomerToTours::groupBy('customer_id','create_tour_id')
                    ->get();

                return $data2;

                */



        $data2 = CustomerToTours::groupBy('tour_title')
            ->get()




            ->map(function ($item) {

                // Return the number of persons with that age
                $cust = Customer::find($item->customer_id);
                return ($item->tour_title) ;
            });

     //  return $data2;

        $chart = new LastMonthOrder;
       $chart->labels(['2 days ago', 'Yesterday', 'Today']);
       $chart->dataset('Orders', 'line', $data);
       $chart->dataset('Orders', 'line', $data3);

        $chart2 = new LastMonthOrder;
        $chart2->labels($data2->keys());
        $chart2->dataset('Tour Packages', 'bar', $data);
        $chart2->dataset('Tour Packages', 'bar', $data2);
        $chart2->dataset('Tour Packages', 'bar', $data3);


        return view('admin.pages.blank')->with(
            [
                'chart' => $chart,
                'chart2' => $chart2

            ]

        );
    }
}
