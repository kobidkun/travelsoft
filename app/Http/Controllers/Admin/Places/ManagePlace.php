<?php

namespace App\Http\Controllers\Admin\Places;

use App\Model\City\CreateCity;
use App\Model\Place\CreatePlace;
use App\Model\Place\CreatePlaceToImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;


class ManagePlace extends Controller
{
    public function Create(){

        $city = CreateCity::all();

        return view('admin.pages.place.create')->with([
            'cities' => $city
        ]);

    }

    public function Index(){

        return view('admin.pages.place.index');

    }

    public function Save(Request $request){


        if ( $request->sm_img  === null ){
            return 'no Small image ';
        } else if ( $request->bg_img  === null ){
            return 'no HD image ';
        }


        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/place/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/place/hd', $imglocname);


        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/place/sm/'.$imglocsmname;
        $imglocsm = $request->sm_img->storeAs('public/images/place/sm',$imglocsmname);




        $a = new CreatePlace();

        $a->name = $request->name;
        $a->description = $request->description;
        $a->lat = $request->lat;
        $a->long = $request->long;
        $a->create_city_id = $request->create_city_id;
        $a->google_name = $request->google_name;
        $a->google_details = $request->google_details;
        $a->sm_img = $imglocsmnamepath;
        $a->bg_img = $imglocnamepath;
        $a->save();

        return back();

    }



    public function Details($id){

        $a = CreatePlace::find($id);

        $allcity = CreateCity::all();

        $b = $a->create_place_to_images;
        $linkedcity = $a->create_cities;

        //dd($b);

        return view('admin.pages.place.details')->with([
            'city' => $a,
            'images' => $b,
            'linkedcity' =>$linkedcity,
            'allcity' => $allcity
        ]);

    }

    public function Edit($id){

    }

    public function Update($id,Request $request){

        $a =  CreatePlace::findorfail($id);

        $a->name = $request->name;
        $a->description = $request->description;
        $a->lat = $request->lat;
        $a->long = $request->long;
        $a->google_name = $request->google_name;
        $a->google_details = $request->google_details;

        $a->save();

        return back();

    }

    public function UpdatesmImg($id,Request $request){





        $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
        $imglocsmnamepath = '/images/place/sm/'.$imglocsmname;
        $imglocsm = $request->sm_img->storeAs('public/images/place/sm',$imglocsmname);


        $a =  CreatePlace::findorfail($id);

        $a->sm_img = $imglocsmnamepath;


        $a->save();

        return back();

    }

    public function UpdateBGImg($id,Request $request){

        $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
        $imglocnamepath = '/images/place/hd/'.$imglocname;

        $imgloc = $request->bg_img->storeAs('public/images/place/hd', $imglocname);


        $a =  CreatePlace::findorfail($id);

        $a->bg_img = $imglocnamepath;


        $a->save();

        return back();

    }

    public function Delete($id){

    }

    public function UploadGallery($id, Request $request){
        if ( $request->file  === null ){
            return 'no Small image ';
        }

        // bcrypt()



        $imglocname =  time().'-'.$request->file->getClientOriginalName();
        $imglocnamepath = '/images/place/gallery/'.$imglocname;

        $imgloc = $request->file->storeAs('public/images/place/gallery', $imglocname);


        $a = new CreatePlaceToImage();

        $a->create_place_id = $id;
        $a->name = $imglocnamepath;
        $a->location = $imglocnamepath;
        $a->type = 'gallery';

        $a->save();



        return response([
            'message' => 'Image saved Successfully'
        ], 200);
    }

    public function DeleteGallery($id){
        $a = CreatePlaceToImage::find($id);

        $a->delete();
        return back();
    }

    public function DatatableApi(){



        $users = CreatePlace::select([
            'id',
            'name',
            'google_name',
            'is_featured',
        ]);


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-primary" href="'.route('admin.place.details',$user->id).'"></i> Details</a>';
            })

            ->make(true);






    }
}
