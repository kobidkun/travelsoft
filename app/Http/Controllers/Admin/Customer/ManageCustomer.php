<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Http\Requests\Admin\Customer\CreateCustomer;
use App\Model\Customer\Customer;
use App\Model\Customer\CustomerToTours;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;


class ManageCustomer extends Controller
{
    public function create(){




        return view('admin.pages.customer.create')->with([

        ]);

    }

    public function Index(){




        return view('admin.pages.customer.allcustomer')->with([

        ]);

    }

    public function Details($id){

        $customer = Customer::findorfail($id);

        $orders = $customer->customer_to_tours->last();

     // $b =  $orders->orderBy('created_at', 'desc');

     // return $orders;






        return view('admin.pages.customer.customerdetails')->with([

            'customer' => $customer,
            'lastorders' => $orders

        ]);

    }

    public function Save(CreateCustomer $request){

        $a = new Customer();

        $a->fname = $request->fname;
        $a->lname = $request->lname;
        $a->email = $request->email;
        $a->phone = $request->phone;
        $a->state = $request->state;
        $a->city = $request->city;


        $a->save();

        return back();







    }


    public function Delete($id){

        $a = Customer::findorfail($id);

        $a->delete();

        return back();





    }

    public function DatatableApi(){




        $users = Customer::select([
            'id',
            'fname',
            'lname',
            'email',
            'phone',
            'state',
            'city',

        ]);


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-danger" href="'.route('admin.customer.delete',$user->id).'"></i> Delete</a>';
            })

            ->addColumn('details', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-primary" href="'.route('admin.details.customer',$user->id).'"></i> Details</a>';
            })


            ->rawColumns(['action','details'])

            ->make(true);






    }


    public function DatatableApiOrders($id){


      //  $customerstotours = Customer::find($id);


        $users = CustomerToTours::where('customer_id', $id)->select([
            'id',
            'create_tour_id',
            'customer_id',
            'tour_title',
            'ip',
            'adult',
            'children',
            'date',
            'created_at'

        ]);


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-danger" href="'.route('admin.customer.delete',$user->id).'"></i> Delete</a>';
            })

            ->addColumn('details', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-primary" href="'.route('admin.order.details',$user->id).'"></i> Details</a>';
            })


            ->rawColumns(['action','details'])

            ->make(true);






    }

}
