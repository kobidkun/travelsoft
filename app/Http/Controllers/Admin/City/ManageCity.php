<?php

namespace App\Http\Controllers\Admin\City;

use App\Model\City\CreateCity;
use App\Model\City\CreateCityToImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ManageCity extends Controller
{
   public function Create(){

       return view('admin.pages.city.create');

   }

   public function Index(){

       return view('admin.pages.city.index');

   }

   public function Save(Request $request){


       if ( $request->sm_img  === null ){
           return 'no Small image ';
       } else if ( $request->bg_img  === null ){
           return 'no HD image ';
       }


       $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
       $imglocnamepath = '/images/city/hd/'.$imglocname;

       $imgloc = $request->bg_img->storeAs('public/images/city/hd', $imglocname);


       $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
       $imglocsmnamepath = '/images/city/sm/'.$imglocsmname;
       $imglocsm = $request->sm_img->storeAs('public/images/city/sm',$imglocsmname);




       $a = new CreateCity();

       $a->name = $request->name;
       $a->description = $request->description;
       $a->lat = $request->lat;
       $a->long = $request->long;
       $a->google_name = $request->google_name;
       $a->google_details = $request->google_details;
       $a->sm_img = $imglocsmnamepath;
       $a->bg_img = $imglocnamepath;
       $a->save();

       return back();

   }



   public function Details($id){

       $a = CreateCity::find($id);

       $b = $a->create_city_to_images;
       $places = $a->create_places;
       $hotels = $a->create_hotels;

      // dd($hotels);

       return view('admin.pages.city.details')->with([
           'city' => $a,
           'images' => $b,
           'places' => $places,
           'hotels' => $hotels
       ]);

   }

   public function Edit($id){

   }

   public function Update($id,Request $request){

       $a =  CreateCity::findorfail($id);

       $a->name = $request->name;
       $a->description = $request->description;
       $a->lat = $request->lat;
       $a->long = $request->long;
       $a->google_name = $request->google_name;
       $a->google_details = $request->google_details;

       $a->save();

       return back();

   }

   public function UpdatesmImg($id,Request $request){





       $imglocsmname =  time().'-'.$request->sm_img->getClientOriginalName();
       $imglocsmnamepath = '/images/city/sm/'.$imglocsmname;
       $imglocsm = $request->sm_img->storeAs('public/images/city/sm',$imglocsmname);


       $a =  CreateCity::findorfail($id);

       $a->sm_img = $imglocsmnamepath;


       $a->save();

       return back();

   }

   public function UpdateBGImg($id,Request $request){

       $imglocname =  time().'-'.$request->bg_img->getClientOriginalName();
       $imglocnamepath = '/images/city/hd/'.$imglocname;

       $imgloc = $request->bg_img->storeAs('public/images/city/hd', $imglocname);


       $a =  CreateCity::findorfail($id);

       $a->bg_img = $imglocnamepath;


       $a->save();

       return back();

   }

   public function Delete($id){

   }

   public function UploadGallery($id, Request $request){
       if ( $request->file  === null ){
           return 'no Small image ';
       }

       // bcrypt()



       $imglocname =  time().'-'.$request->file->getClientOriginalName();
       $imglocnamepath = '/images/city/gallery/'.$imglocname;

       $imgloc = $request->file->storeAs('public/images/city/gallery', $imglocname);


       $a = new CreateCityToImage();

       $a->create_city_id = $id;
       $a->name = $imglocnamepath;
       $a->location = $imglocnamepath;
       $a->type = 'gallery';

       $a->save();



       return response([
           'message' => 'Image saved Successfully'
       ], 200);
   }

   public function DeleteGallery($id){
       $a = CreateCityToImage::find($id);

       $a->delete();
       return back();
   }

    public function DatatableApi(){



        $users = CreateCity::select([
            'id',
            'name',
            'google_name',
            'is_featured',
        ]);


        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                return '<a class="btn waves-effect waves-light btn-rounded btn-primary" href="'.route('admin.city.details',$user->id).'"></i> Details</a>';
            })

            ->make(true);






    }
}
