<?php

namespace App\Http\Controllers\Front;

use App\Mail\Notification\Customer\OrderEmailNotificationToCustomer;
use App\Master\CreateSlider;
use App\Model\Cab\CreateCab;
use App\Model\City\CreateCity;
use App\Model\Customer\Customer;
use App\Model\Customer\CustomerToTours;
use App\Model\Hotel\CreateHotel;
use App\Model\Master\CreateIcon;
use App\Model\Master\CreateTag;
use App\Model\Master\CreateTheme;
use App\Model\Tour\CreateTour;
use App\Model\Tour\CreateTourToIcon;
use App\Model\Tour\CreateTourToTag;
use App\Model\Tour\CreateTourToTheme;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


use Illuminate\Support\Facades\Mail;
use SEOMeta;
use OpenGraph;
use Twitter;

use SEO;

class ManagePageController extends Controller
{
    public function HomePage(){




        $sliders = CreateSlider::all();


        $tours = CreateTour::paginate(9);

        $cities = CreateCity::paginate(6);

        $counttours = CreateTour::all()->count();



       // SEO::addMeta('article:section', 'travel', 'property');
       // SEO::addKeyword(['travel', 'darjeeling', 'sikkim']);


        return view('frontend.page.home')->with([
            'tours' => $tours,
            'cities' => $cities,
            'sliders' => $sliders,
            'counttours' => $counttours
        ]);


    }

    public function TourDetails($id){




        $findtour = CreateTour::findorfail($id);

        $gallery = $findtour->create_tour_to_images;
        $inclusion = $findtour->create_tour_to_inclusions;
        $exclusion = $findtour->create_tour_to_exclusions;
        $itinaries = $findtour->create_tour_to_itinaries;
        $icons = $findtour->create_tour_to_icons;
        $tags = $findtour->create_tour_to_tags;


        //return $itinaries;

      //  dd($itinaries);

        $explodekeyword = explode(' ',$findtour->title);

        //return $explodekeyword;



        SEOMeta::setTitle($findtour->title);
        SEOMeta::setDescription($findtour->description);
        SEOMeta::addMeta('article:published_time', $findtour->created_at->toW3CString(), 'property');
        SEOMeta::addMeta('article:section', $findtour->create_cities->name, 'property');
        SEOMeta::addKeyword($explodekeyword);

        OpenGraph::setDescription($findtour->description);
        OpenGraph::setTitle($findtour->title);
        OpenGraph::setUrl(route('front.details',$findtour->id));
        OpenGraph::addProperty('type', 'article');
        OpenGraph::addProperty('locale', 'en-in');


        OpenGraph::addImage(asset('/storage/'.$findtour->img_bg));
      //  OpenGraph::addImage($post->images->list('url'));
      //  OpenGraph::addImage(['url' => 'http://image.url.com/cover.jpg', 'size' => 300]);
       // OpenGraph::addImage('http://image.url.com/cover.jpg', ['height' => 300, 'width' => 300]);

        // Namespace URI: http://ogp.me/ns/article#
        // article
        OpenGraph::setTitle('Article')
            ->setDescription('Some Article')
            ->setType('article')
            ->setArticle([
                'published_time' => $findtour->created_at,
                'modified_time' => $findtour->updated_at,
                'expiration_time' => 'datetime'
            ]);



        return view('frontend.page.tourdetails')->with([
            'tour' => $findtour,
            'galleries' => $gallery,
            'inclusions' => $inclusion,
            'exclusions' => $exclusion,
            'itinaries' => $itinaries,
            'icons' => $icons,
            'tags' => $tags,
        ]);
    }

    public function GetTourbyCity($city){
        $getCity = CreateCity::where('name', $city)->first();

        $tours = CreateTour::where('create_city_id', $getCity->id)->paginate(12);

      //  return $tours;

        $returntourids  = $tours->map(function ($item, $key)  {

            return $item->id;
        });


       $alltags = CreateTourToTag::whereIn('create_tour_id', $returntourids)->get();



        $unique_tag = $alltags->unique('tag');






      //  return $alltags;



        return view('frontend.page.gridbycity')->with([
            'tours' => $tours,
            'theme' => $getCity,
            'alltags' => $unique_tag,
            'city' => $getCity
        ]);



    }


    public function GetTourbyTheme($theme){





        $getCity = CreateTheme::where('theme', $theme)->first();

     //   return $getCity;

        $tourstotheme = CreateTourToTheme::where('create_theme_id', $getCity->id)->get();

        $returntourtothemeids  = $tourstotheme->map(function ($item, $key)  {

            return $item->create_tour_id;
        });

       // return $tourstotheme;

        $tours = CreateTour::whereIn('id', $returntourtothemeids)->paginate(12);

      //  return $tours;



        $returntourids  = $tours->map(function ($item, $key)  {

            return $item->id;
        });


       $alltags = CreateTourToTag::whereIn('create_tour_id', $returntourids)->get();

      //  return $tours;

        $unique_tag = $alltags->unique('tag');

        $cities = CreateCity::all();



        SEOMeta::setTitle($getCity->theme);
        SEOMeta::setDescription($getCity->theme);
        SEOMeta::addMeta('article:published_time', $getCity->created_at->toW3CString(), 'property');
        SEOMeta::addMeta('article:section', $getCity->theme, 'property');
        SEOMeta::addKeyword($getCity);

        OpenGraph::setDescription($getCity->theme);








      //  return $alltags;



        return view('frontend.page.bytheme')->with([
            'tours' => $tours,
            'cities' => $cities,
            'alltags' => $unique_tag
        ]);



    }


    public function GetTourbyTag($tag){
        $getCity = CreateTag::where('tag', $tag)->first();

     //   return $getCity;

        $tourstotheme = CreateTourToTag::where('create_tag_id', $getCity->id)->get();

        $returntourtothemeids  = $tourstotheme->map(function ($item, $key)  {

            return $item->create_tour_id;
        });

       // return $tourstotheme;

        $tours = CreateTour::whereIn('id', $returntourtothemeids)->paginate(12);

      //  return $tours;



        $returntourids  = $tours->map(function ($item, $key)  {

            return $item->id;
        });


       $alltags = CreateTourToTag::whereIn('create_tour_id', $returntourids)->get();

        $unique_tag = $alltags->unique('tag');

      //  return $unique_applied_exams;

        $cities = CreateCity::all();









      //  return $alltags;



        return view('frontend.page.bytheme')->with([
            'tours' => $tours,
            'cities' => $cities,
            'alltags' => $unique_tag
        ]);



    }


    public function GetAllTour(){
      //  $getCity = CreateCity::where('name', $city)->first();

        $allcities = CreateCity::all();

        $tours = CreateTour::paginate(12);

      //  return $tours;

        $returntourids  = $tours->map(function ($item, $key)  {

            return $item->id;
        });


       $tags = CreateTourToTag::
       //distinct('create_tag_id')->
         //  pluck('create_tag_id')//->
           whereIn('create_tour_id', $returntourids)
           ->get();

       // $alltags = collect($tags)->unique('create_tag_id');


        $tags = CreateTourToTag::
        //distinct('create_tag_id')->
        //  pluck('create_tag_id')//->
        whereIn('create_tour_id', $returntourids)
            ->get();

        $alltags = collect($tags)->unique('create_tag_id');






      //  return $alltags;



        return view('frontend.page.alltours')->with([
            'tours' => $tours,
            'alltags' => $alltags,
            'cities' => $allcities
        ]);



    }

    public function SortTour($city,$place,$theme,$duration){

    }



    public function TourMapDetails($id){

        $a = CreateTour::find($id);

        $allcity = CreateCity::all();

        $allthemes = CreateTheme::all();

        $b = $a->create_tour_to_images;
        $linkedcity = $a->create_cities;
        $itinaey = $a->create_tour_to_itinaries;



        $hotel = $a->create_hotels;

        $mappoints = $a->create_tour_to_places;
        $tourInclusion = $a->create_tour_to_inclusions;
        $tourExclusion = $a->create_tour_to_exclusions;
        $touricons = $a->create_tour_to_icons;
        $tourtags = $a->create_tour_to_tags;
        $tourtocities = $a->create_tour_to_cities;
        $tourtothemes = $a->create_tour_to_themes;

        //   dd($tourtocity);




        //

        $collection = collect($itinaey);

        $multiplied = $collection->map(function ($item, $key) {
            return $item->create_hotel_id;
        });

        $multiplied->all();

        $models = CreateHotel::findMany($multiplied);






        $allTags = CreateTag::all();
        $allIcons = CreateIcon::all();





        // return response()->json($models);

        return view('frontend.page.tourmap')->with([
            'city' => $a,
            'images' => $b,
            'linkedcity' =>$linkedcity,
            'allcity' => $allcity,
            'itineraries' => $itinaey,
            'places' => $mappoints,
            'hotels' => $models,
            'tourinclusions' => $tourInclusion,
            'tourexclusions' => $tourExclusion,
            'alltags' => $allTags,
            'allicons' => $allIcons,
            'touricons' => $touricons,
            'tourtags' => $tourtags,
            'tourtocities' => $tourtocities,
            'allthemes' => $allthemes,
            'tourtothemes' => $tourtothemes,

        ]);

    }


    public function SaveLeadForm(Request $request){
        $email = $request->email;

        if (Customer::where('email', $email)->exists() ){

            $customerfind = Customer::where('email', $email)->first();
            $findtourtitle = CreateTour::find($request->create_tour_id);

            $b = new CustomerToTours();

            $b->customer_id = $customerfind->id;
            $b->create_tour_id = $request->create_tour_id;
            $b->tour_title = $findtourtitle->title;
            $b->ip = $request->ip();
            $b->adult = $request->adult;
            $b->children = $request->children;
            $b->date = $request->date;

            $b->save();


           // $getTourformail = CreateTour::find($request->create_tour_id);


            $data = CustomerToTours::find($b->id)->first();

          //  $data = $b->with('create_tours')->get();





           // return $data2;

            Mail::to($request->email)->send(new OrderEmailNotificationToCustomer($data));

            return back();


        } else {


            $a = new Customer();

            $a->fname = $request->fname;
            $a->lname = $request->lname;
            $a->phone = $request->phone;
            $a->password = $request->password;
            $a->email = $request->email;
            $a->save();

            $findtourtitle = CreateTour::find($request->create_tour_id);

            $b = new CustomerToTours();

            $b->customer_id = $a->id;
            $b->create_tour_id = $request->create_tour_id;
            $b->tour_title = $findtourtitle->title;
            $b->ip = $request->ip();
            $b->adult = $request->adult;
            $b->children = $request->children;
            $b->date = $request->date;

            $b->save();

            Mail::to($request->email)->send(new OrderEmailNotificationToCustomer($b));


            return back();




        }




    }

    public function About(){
        return view('frontend.page.about');
    }

    public function Contact(){
        return view('frontend.page.contact');
    }


    public function Terms(){
        return view('frontend.page.legeal.terms');
    }

    public function Privacy(){
        return view('frontend.page.legeal.privacy');
    }


    public function AllCars(){

        $cars = CreateCab::all();

      //  return $cars;
        return view('frontend.page.car.allcar')->with(['cars' => $cars]);
    }


    public function CarDetails($id){

        $cars = CreateCab::findorfail($id);

        return view('frontend.page.car.cardetails')->with(['car' => $cars]);
    }


}
