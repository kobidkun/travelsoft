<?php

namespace App\Model\Hotel\Room;

use Illuminate\Database\Eloquent\Model;

class CreateRoom extends Model
{
    public function create_cities()
    {
        return $this->hasOne('App\Model\City\CreateCity','id','create_city_id');
    }

    public function create_hotel()
    {
        return $this->hasOne('App\Model\Hotel\CreateHotel','id','create_hotel_id');
    }

    public function create_room_to_images()
    {
        return $this->hasMany('App\Model\Hotel\Room\CreateRoomToImage','id','create_room_id');
    }

    public function create_places()
    {
        return $this->hasMany('App\Model\Place\CreatePlace','create_city_id','create_city_id');
    }
}
