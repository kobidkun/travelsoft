<?php

namespace App\Model\Hotel;

use Illuminate\Database\Eloquent\Model;

class CreateHotel extends Model
{
    public function create_hotel_to_images()
    {
        return $this->hasMany('App\Model\Hotel\CreateHotelToImages','create_hotel_id','id');
    }

    public function create_cities()
    {
        return $this->hasOne('App\Model\City\CreateCity','id','create_city_id');
    }

    public function create_places()
    {
        return $this->hasMany('App\Model\Place\CreatePlace','create_city_id','create_city_id');
    }

    public function create_rooms()
    {
        return $this->hasMany('App\Model\Hotel\Room\CreateRoom','create_hotel_id','id');
    }

    public function create_tour_to_itinaries()
    {
        return $this->belongsTo('App\Model\Tour\Itinary\CreateTourToItinary','create_hotel_id','id');
    }
}
