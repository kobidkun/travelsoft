<?php

namespace App\Model\Customer;

use Illuminate\Database\Eloquent\Model;

class CustomerToTours extends Model
{
    public  function create_tours(){

       return $this->hasOne('App\Model\Tour\CreateTour','id','create_tour_id');

    }

    public  function customers(){

       return $this->hasOne('App\Model\Customer\Customer','id','customer_id');

    }

    public  function tour_to_estimates(){

       return $this->hasOne('App\Model\Customer\Tour\TourToEstimate','customer_to_tours','id');

    }
}
