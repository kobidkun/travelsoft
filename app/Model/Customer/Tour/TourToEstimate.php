<?php

namespace App\Model\Customer\Tour;

use Illuminate\Database\Eloquent\Model;

class TourToEstimate extends Model
{
    public  function customers(){

        return  $this->hasOne('App\Model\Customer\Customer','id','customer_id');

    }

    public  function tour_to_estimate_to_items(){

        return  $this->hasMany('App\Model\Customer\Tour\TourToEstimateToItem','tour_to_estimate_id','id');

    }

    public  function create_tour(){

        return  $this->hasOne('App\Model\Tour\CreateTour','id','create_tour_id')
            ->with('create_tour_to_itinaries','create_tour_to_images');

    }
}
