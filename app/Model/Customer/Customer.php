<?php

namespace App\Model\Customer;


use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{

    public  function customer_to_tours(){

      return  $this->hasMany('App\Model\Customer\CustomerToTours','customer_id','id');

    }


    protected $guard = 'admin';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


}
