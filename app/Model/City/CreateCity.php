<?php

namespace App\Model\City;

use Illuminate\Database\Eloquent\Model;

class CreateCity extends Model
{
    public function create_city_to_images()
    {
        return $this->hasMany('App\Model\City\CreateCityToImage','create_city_id','id');
    }

    public function create_places()
    {
        return $this->hasMany('App\Model\Place\CreatePlace','create_city_id','id');
    }

    public function create_hotels()
    {
        return $this->hasMany('App\Model\Hotel\CreateHotel','create_city_id','id')->with('create_rooms');
    }

    public function TourPackages(){

        return $this->hasMany('App\Model\Tour\CreateTour','create_city_id','id');

    }
}
