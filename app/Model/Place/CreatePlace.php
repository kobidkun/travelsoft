<?php

namespace App\Model\Place;

use Illuminate\Database\Eloquent\Model;

class CreatePlace extends Model
{
    public function create_place_to_images()
    {
        return $this->hasMany('App\Model\Place\CreatePlaceToImage','create_place_id','id');
    }

    public function create_cities()
    {
        return $this->hasOne('App\Model\City\CreateCity','id','create_city_id');
    }
}
