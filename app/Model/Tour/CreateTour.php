<?php

namespace App\Model\Tour;

use Illuminate\Database\Eloquent\Model;

class CreateTour extends Model
{
    public function create_cities()
    {
        return $this->hasOne('App\Model\City\CreateCity','id','create_city_id');
    }

    public function create_tour_to_images()
    {
        return $this->hasMany('App\Model\Tour\CreateTourToImage','create_tour_id','id');
    }

    public function create_tour_to_inclusions()
    {
        return $this->hasMany('App\Model\Tour\CreateTourToInclusion','create_tour_id','id');
    }

    public function create_tour_to_exclusions()
    {
        return $this->hasMany('App\Model\Tour\CreateTourToExclusion','create_tour_id','id');
    }

    public function create_tour_to_ratings()
    {
        return $this->hasMany('App\Model\City\CreateCityToImage','create_tour_id','id');
    }

    public function create_tour_to_itinaries()
    {
        return $this->hasMany('App\Model\Tour\Itinary\CreateTourToItinary','create_tour_id','id')
            ->with(['create_tour_to_itinary_to_cities','create_tour_to_itinary_to_places','create_tour_itinary_to_images'])
            ->orderBy('day');
    }



    public function create_tour_to_places()
    {
        return $this->hasMany('App\Model\Tour\Itinary\CreateTourToItinaryToPlace','create_tour_id','id')
            ->with(['create_places','create_hotels']);
    }

    public function create_tour_to_itinary_to_inclusions()
    {
        return $this->hasMany('App\Model\Tour\Itinary\CreateTourToItinaryToInclusion','create_tour_id','id')->take(6);
    }

    public function create_tour_to_icons()
    {
        return $this->hasMany('App\Model\Tour\CreateTourToIcon','create_tour_id','id');
    }

    public function create_tour_to_tags()
    {
        return $this->hasMany('App\Model\Tour\CreateTourToTag','create_tour_id','id');
    }

    public function create_tour_to_cities()
    {
        return $this->hasMany('App\Model\Tour\CreateTourToCity','create_tour_id','id');
    }

    public function create_tour_to_themes()
    {
        return $this->hasMany('App\Model\Tour\CreateTourToTheme','create_tour_id','id');
    }

    public function CountOrders(){

        return $this->hasMany('App\Model\Customer\CustomerToTours','create_tour_id','id');

    }







}
