<?php

namespace App\Model\Tour\Itinary;

use Illuminate\Database\Eloquent\Model;

class CreateTourToItinaryToPlace extends Model
{
    public function create_places()
    {
        return $this->hasOne('App\Model\Place\CreatePlace','id','create_place_id');
    }

    public function create_hotels()
    {
        return $this->hasMany('App\Model\Hotel\CreateHotel','id','create_hotel_id')
            ;
    }
}
