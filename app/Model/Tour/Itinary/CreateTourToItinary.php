<?php

namespace App\Model\Tour\Itinary;

use Illuminate\Database\Eloquent\Model;

class CreateTourToItinary extends Model
{
    public function create_tour()
    {
        return $this->belongsTo('App\Model\Tour\CreateTour','id','create_tour_id');
    }

    public function create_tour_to_itinary_to_cities()
    {
        return $this->hasOne('App\Model\City\CreateCity','id','create_city_id');
    }

    public function create_tour_to_itinary_to_inclusions()
    {
        return $this->hasMany('App\Model\Tour\Itinary\CreateTourToItinaryToInclusion','create_tour_to_itinary_id','id');
    }

    public function create_tour_to_itinary_to_places()
    {
        return $this->hasMany('App\Model\Tour\Itinary\CreateTourToItinaryToPlace','create_tour_to_itinary_id','id');
    }

    public function create_tour_itinary_to_images()
    {
        return $this->hasMany('App\Model\Tour\Itinary\CreateTourItinaryToImage','create_tour_to_itinary_id','id');
    }
}
