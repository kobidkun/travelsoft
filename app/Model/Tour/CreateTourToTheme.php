<?php

namespace App\Model\Tour;

use Illuminate\Database\Eloquent\Model;

class CreateTourToTheme extends Model
{
    public function create_tours()
    {
        return $this->hasMany('App\Model\Tour\CreateTour','id','create_tour_id');
    }
}
