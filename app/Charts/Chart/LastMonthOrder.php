<?php

namespace App\Charts\Chart;

use ConsoleTVs\Charts\Classes\Highcharts\Chart;

class LastMonthOrder extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
}
