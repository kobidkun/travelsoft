
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title> {{env('SITENAME')}} Admin Panel</title>
    <meta name="description" content="Admintres is a Dashboard & Admin Site Responsive Template by hencework." />
    <meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Admintres Admin, Admintresadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
    <meta name="author" content="hencework"/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('/images/logo/fabicon.png')}}">
    <link rel="icon" href="{{asset('/images/logo/fabicon.png')}}" type="image/x-icon">

    <!-- Data table CSS -->
    <link href="{{asset('admin/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css"/>

    <!-- Custom CSS -->
    <link href="{{asset('admin/dist/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('frontend/css/vendors.css')}}" rel="stylesheet" type="text/css"/>

    @yield('css')
</head>

<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->
<div class="wrapper theme-2-active navbar-top-light">





@include('admin.component.menu')

@include('admin.component.leftsidebar')

@include('admin.component.rightsidebar')






<!-- Main Content -->
    <div class="page-wrapper">
        <div class="container">








        @yield('content')










            <!-- Footer -->
            <footer class="footer pl-30 pr-30">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>2018 &copy; </p>
                        </div>
                        <div class="col-sm-6 text-right">
                            <p>Follow Us</p>
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- /Footer -->
        </div>
    </div>
    <!-- /Main Content -->


</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="{{asset('admin/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{asset('admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Data table JavaScript -->
<script src="{{asset('admin/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/dist/js/dataTables-data.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{asset('admin/dist/js/jquery.slimscroll.js')}}"></script>

<!-- Owl JavaScript -->
<script src="{{asset('admin/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js')}}"></script>

<!-- Switchery JavaScript -->
<script src="{{asset('admin/vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{asset('admin/dist/js/dropdown-bootstrap-extended.js')}}"></script>

@yield('script')


<!-- Init JavaScript -->
<script src="{{asset('admin/dist/js/init.js')}}"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script>
    (function(e,t,n,i,s,a,c){e[n]=e[n]||function(){(e[n].q=e[n].q||[]).push(arguments)}
    ;a=t.createElement(i);c=t.getElementsByTagName(i)[0];a.async=true;a.src=s
    ;c.parentNode.insertBefore(a,c)
    })(window,document,"galite","script","https://cdn.jsdelivr.net/npm/ga-lite@2/dist/ga-lite.min.js");

    galite('create', 'UA-133545673-1', 'auto');
    galite('send', 'pageview');
</script>


</body>

</html>
