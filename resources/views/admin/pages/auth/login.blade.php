<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title> {{env('SITENAME')}} Admin Login</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="tecions"/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('/images/logo/fabicon.png')}}">
    <link rel="icon" href="{{asset('/images/logo/fabicon.png')}}" type="image/x-icon">

    <!-- vector map CSS -->
    <link href="{{asset('admin/vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>



    <!-- Custom CSS -->
    <link href="{{asset('admin/dist/css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
<!--Preloader-->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!--/Preloader-->

<div class="wrapper  pa-0">
    <header class="sp-header">
        <div class="sp-logo-wrap pull-left">
            <a href="{{route('admin.login')}}">
                <img class="brand-img mr-10" src="{{asset('/images/logo/logo_sticky.png')}}" alt="brand"/>
                <span class="brand-text"><img  src="{{asset('/images/logo/logo_sticky.png')}}" alt="brand"/></span>
            </a>
        </div>

        <div class="clearfix"></div>
    </header>

    <!-- Main Content -->
    <div class="page-wrapper pa-0 ma-0 error-bg-img">
        <div class="container">
            <!-- Row -->
            <div class="table-struct full-width full-height">
                <div class="table-cell vertical-align-middle auth-form-wrap">
                    <div class="auth-form  ml-auto mr-auto no-float card-view pt-30 pb-30">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="mb-30">
                                    <h3 class="text-center txt-dark mb-10">Sign in to {{env('SITENAME')}}</h3>

                                </div>
                                <div class="form-wrap">




                                    <form type="POST" action="{{route('admin.login.submit')}}">

                                        @if ($errors->has('email'))
                                        <div class="alert alert-danger  alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                                &times;</button>
                                            <p class="pull-left">
                                                {{ $errors->first('email') }}</p>
                                            <div class="clearfix"></div>
                                        </div>

                                        @endif

                                            @if ($errors->has('password'))


                                                <div class="alert alert-danger  alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    <i class="zmdi zmdi-check pr-15 pull-left"></i><p class="pull-left">
                                                        {{ $errors->first('password') }}</p>
                                                    <div class="clearfix"></div>
                                                </div>



                                            @endif

                                        @csrf
                                        <div class="form-group">
                                            <label class="control-label mb-10" for="exampleInputEmail_2">{{ __('E-Mail Address') }}</label>
                                            <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   value="{{ old('email') }}" required autofocus
                                                   placeholder="Enter email">

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group">

                                            <div class="clearfix"></div>
                                            <input name="password" type="password" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   required=""  placeholder="Enter pwd">


                                        </div>


                                        <div class="form-group text-center">
                                            <button formmethod="post" formaction="{{route('admin.login.submit')}}" type="submit" class="btn btn-orange btn-rounded">sign in</button>



                                        </div>
                                    </form>





                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>

    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="{{asset('admin/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{asset('admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{asset('admin/dist/js/jquery.slimscroll.js')}}"></script>

<!-- Init JavaScript -->
<script src="{{asset('admin/dist/js/init.js')}}"></script>
</body>
</html>
