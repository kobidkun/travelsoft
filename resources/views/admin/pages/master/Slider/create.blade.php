@extends('admin.index')


@section('content')




    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Theme Package</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">Admin</a></li>
                <li><a href="#"><span>Master</span></a></li>
                <li class="active"><span>Theme Master</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Theme</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">
                                    <form   method="POST"
                                            enctype="multipart/form-data"
                                            action="{{route('admin.save.master.slider')}}"
                                    >
                                        @csrf
                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font">
                                                <i class="zmdi zmdi-pin mr-10"></i>Theme's Info</h6>
                                            <hr class="light-grey-hr"/>
                                            <div class="row">



                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label  class="control-label mb-10">Title
                                                        </label>
                                                        <input name="title" type="text"  class="form-control"
                                                               placeholder="Title">




                                                    </div>
                                                </div>



                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label  class="control-label mb-10">Description
                                                        </label>
                                                        <input name="desc" type="text"  class="form-control"
                                                               placeholder="Description">




                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label  class="control-label mb-10">url
                                                        </label>
                                                        <input name="url" type="text"  class="form-control"
                                                               placeholder="Title">




                                                    </div>
                                                </div>


                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label  class="control-label mb-10">Image (1800x700px)
                                                        </label>
                                                        <input name="bg_img" type="file"  class="form-control"
                                                               placeholder="Title">




                                                    </div>
                                                </div>
                                                <!--/span-->

                                                <!--/span-->
                                            </div>




                                        </div>
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                         </div>
                                    </form>
                                </div>
                            </div>


                            <div style="height: 250px"></div>


                            <div class="col-sm-12 col-xs-12">



                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table  class="table table-hover display  pb-30" >

                                            <thead>
                                            <tr>

                                                <th>Title</th>
                                                <th>Desc</th>
                                                <th>Img</th>
                                                <th>Delete</th>


                                            </tr>
                                            </thead>

                                            <tbody>

                                           @foreach($sliders as $slider)
                                            <tr>
                                                <th scope="row">{{$slider->title}}</th>
                                                <td>{{$slider->desc}}</td>
                                                <td><img src="{{asset('/storage/'.$slider->imgurl)}}" width="350px" alt=""></td>

                                                <td>
                                                    <a class="btn btn-danger" href="{{route('admin.slider.master.delete',$slider->id)}}">Delete</a>
                                                </td>
                                            </tr>

                                               @endforeach

                                            </tbody>




                                        </table>
                                    </div>
                                </div>


                            </div>





















                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







@stop


@section('css')



@stop


@section('script')







    <script src="{{asset('/admin/dist/js/imagekit.min.js')}}"></script>



    <script>
        // Sepecify imagekitid
        imagekit.config.imagekitid = 'tecions';
        $("img").imagekit();
    </script>





@stop
