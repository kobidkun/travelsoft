@extends('admin.index')


@section('content')




    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Tag Package</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">Admin</a></li>
                <li><a href="#"><span>Master</span></a></li>
                <li class="active"><span>Tag Master</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Tag</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">
                                    <form   method="POST"
                                            enctype="multipart/form-data"
                                            action="{{route('admin.save.master.tag')}}"
                                    >
                                        @csrf
                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font">
                                                <i class="zmdi zmdi-pin mr-10"></i>Tag's Info</h6>
                                            <hr class="light-grey-hr"/>
                                            <div class="row">



                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label  class="control-label mb-10">Title
                                                        </label>
                                                        <input name="tag" type="text"  class="form-control"
                                                               placeholder="Title">




                                                    </div>
                                                </div>
                                                <!--/span-->


                                                <!--/span-->
                                            </div>




                                        </div>
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                          </div>
                                    </form>
                                </div>
                            </div>


                            <div style="height: 250px"></div>


                            <div class="col-sm-12 col-xs-12">



                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="users-table" class="table table-hover display  pb-30" >

                                            <thead>
                                            <tr>

                                                <th>Name</th>

                                                <th>Delete</th>


                                            </tr>
                                            </thead>




                                        </table>
                                    </div>
                                </div>


                            </div>





















                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







@stop


@section('css')



@stop


@section('script')

    <link href="{{asset('admin/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css')}}">
    <script src="{{asset('admin/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/dataTables-data.js')}}"></script>



    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.master.tag.datatables') !!}',
                columns: [
                    { data: 'tag', name: 'tag' },

                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>


@stop
