@extends('admin.index')


@section('content')
    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Dashboard</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="index.html">Dashboard</a></li>
                <li><a href="#"><span>speciality pages</span></a></li>
                <li class="active"><span>blank page</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->



            <div class="row">



                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="panel panel-default border-panel card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-dark"> statistics</h6>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <ul class="flex-stat flex-stat-2 mt-20">
                                            <li>
                                                <span class="block"><span class="initial">$ </span><span class="txt-dark weight-300 counter-anim data-rep">7,115,008</span></span>
                                                <span class="block">Bitcoin Price</span>

                                            </li>
                                            <li>
                                                <span class="block"><span class="initial">$ </span><span class="txt-dark weight-300 counter-anim data-rep">5,426.21</span></span>
                                                <span class="block">Since Last Month (USD)</span>

                                            </li>
                                            <li>
													<span class="block">
														<i class="zmdi zmdi-caret-up pr-10 font-24 txt-success"></i><span class="txt-dark weight-300 data-rep"><span class="counter-anim">89</span>%</span>
													</span>

                                                <span class="block">Since Last Month (%)</span>

                                            </li>
                                        </ul>



                                        {!! $chart->container() !!}


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <div class="panel panel-default border-panel card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-dark"> statistics</h6>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">



                                        {!! $chart2->container() !!}


                                    </div>
                                </div>
                            </div>
                        </div>







                    </div>
                </div>




            </div>
            <!-- /Row -->








@stop


@section('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>

    {!! $chart->script() !!}
    {!! $chart2->script() !!}

@stop
