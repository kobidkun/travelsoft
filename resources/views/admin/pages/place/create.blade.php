@extends('admin.index')


@section('content')




    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Create Place Of Interest</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">Admin</a></li>
                <li><a href="#"><span>Place</span></a></li>
                <li class="active"><span>Create Place Of Interest</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Place Of Interest</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">
                                    <form   method="POST"
                                            enctype="multipart/form-data"
                                            action="{{route('admin.save.place')}}"
                                    >
                                        @csrf
                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font">
                                                <i class="zmdi zmdi-pin mr-10"></i>Place's Info</h6>
                                            <hr class="light-grey-hr"/>
                                            <div class="row">



                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label  class="control-label mb-10">Search Places.
                                                             Relax We will assist you. 😊</label>
                                                        <input name="google_name" type="text"  class="form-control"
                                                               id="autocomplete"          placeholder="Place's Name">
                                                        <input type="hidden"  class="form-control" required
                                                               id="google_details"   name="google_details"        placeholder="Place's Name">
                                                        <span class="help-block"> This will help you to find latitude & longitude </span>

                                                        <input type="hidden"  class="form-control" required
                                                               id="name" value=""  name="name" >


                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Short Info</label>
                                                        <input type="text" name="description" class="form-control"
                                                             required  placeholder="Short Info">

                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Select City</label>
                                                        <select class="form-control select2" id="cityname" name="create_city_id">

                                                            @foreach($cities as $city)

                                                                <option value="{{$city->id}}">{{$city->name}}</option>


                                                                @endforeach

                                                        </select>





                                                    </div>
                                                </div>





                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Latitude</label>
                                                        <input  id="lat" name="lat" required readonly type="text" class="form-control" placeholder="Latitude">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Longitude</label>
                                                        <input name="long"  id="long" required readonly  type="text" class="form-control" placeholder="Longitude">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->

                                            <hr class="light-grey-hr"/>

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Large Image</label>
                                                        <input name="bg_img" type="file"  class="form-control"
                                                            required   placeholder="Siliguri">

                                                    </div>
                                                </div>





                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Small Image</label>
                                                        <input name="sm_img"  required  type="file" class="form-control">
                                                    </div>
                                                </div>

                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->



                                        </div>
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                            <button type="button" class="btn btn-default">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







@stop


@section('css')
    <link href="{{asset('admin/vendors/bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
@stop


@section('script')


    <script src="{{asset('admin/vendors/bower_components/select2/dist/js/select2.full.min.js')}}"></script>




    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('GMAPS')}}&libraries=places"></script>
    <script>
        function initialize() {
            var input = document.getElementById('autocomplete');
            var autocomplete = new google.maps.places.Autocomplete(input);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();

                document.getElementById('cityname').value = place.vicinity;
                document.getElementById('name').value = place.name;
                document.getElementById('lat').value = place.geometry.location.lat();
                document.getElementById('long').value = place.geometry.location.lng();
                document.getElementById('google_details').value = JSON.stringify(place);
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>




@stop
