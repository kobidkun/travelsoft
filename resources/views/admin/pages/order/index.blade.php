@extends('admin.index')


@section('content')




    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">CUSTOMER</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">Admin</a></li>
                <li><a href="#"><span>Master</span></a></li>
                <li class="active"><span>Customer</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->




















    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Customer</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">




                        <div class="row">






                            <div class="col-sm-12 col-xs-12">



                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="users-table" class="table table-hover display  pb-30" >

                                            <thead>
                                            <tr>

                                                <th> Name</th>
                                                <th> Email</th>
                                                <th> Phone</th>
                                                <th>Package Name</th>
                                                <th>IP</th>
                                                <th>Adult</th>
                                                <th>Children</th>
                                                <th>Booking Date</th>
                                                <th>Created At</th>
                                                <th>Action</th>




                                            </tr>
                                            </thead>




                                        </table>
                                    </div>
                                </div>


                            </div>


















                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







@stop


@section('css')



@stop


@section('script')

    <link href="{{asset('admin/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css')}}">
    <script src="{{asset('admin/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/dataTables-data.js')}}"></script>



    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.datatable.order') !!}',
                columns: [
                    { data: 'customer_name', name: 'customer_name' },
                    { data: 'customer_email', name: 'customer_email' },
                    { data: 'customer_phone', name: 'customer_phone' },
                    { data: 'tour_title', name: 'tour_title' },
                    { data: 'ip', name: 'ip' },
                    { data: 'adult', name: 'adult' },
                    { data: 'children', name: 'children' },
                    { data: 'date', name: 'date' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'details', name: 'details' },

                ]
            });
        });
    </script>

@endsection
