@extends('admin.index')


@section('content')




    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Order Summery</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">Admin</a></li>
                <li><a href="#"><span>Orders</span></a></li>
                <li class="active"><span> Order info</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Details</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">




                                    <div style="height: 100px"></div>


                                    <div class="profile-box">

                                        <div class="profile-info text-center mb-15">
                                            <div class="profile-img-wrap">
                                                <img class="inline-block mb-10"
                                                     src="{{asset('/storage/'.$tour->img_sm)}}" alt="user"/>
                                                <div class="fileupload btn btn-default">


                                                </div>
                                            </div>

                                        </div>

                                        <div class="social-info">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <span class="counts block head-font"><span style="color: #0e88b1;font-weight: 500" class="counter-anim">{{$tour->title}}</span></span>
                                                    <span class="counts-text block"> Title</span>
                                                </div>

                                                <div class="col-xs-12 text-center">
                                                    <span class="counts block head-font"><span class="counter-anim">{{$customer->fname}} {{$customer->lname}}</span></span>
                                                    <span class="counts-text block">Name</span>
                                                </div>

                                                <div class="col-xs-6 text-center">
                                                    <span class="counts block head-font"><span class="counter-anim">{{$customer->phone}}</span></span>
                                                    <span class="counts-text block">Phone</span>
                                                </div>

                                                <div class="col-xs-6 text-center">
                                                    <span class="counts block head-font"><span class="counter-anim">{{$customer->email}}</span></span>
                                                    <span class="counts-text block">Email</span>
                                                </div>






                                            </div>

                                        </div>

                                        <div class="social-info">

                                            <div class="row">


                                                <div class="col-xs-6 text-center">
                                                    <span class="counts block head-font"><span class="counter-anim">{{$customertotour->adult}}</span></span>
                                                    <span class="counts-text block">Adult</span>
                                                </div>

                                                <div class="col-xs-6 text-center">
                                                    <span class="counts block head-font"><span class="counter-anim">{{$customertotour->children}}</span></span>
                                                    <span class="counts-text block">Children</span>
                                                </div>

                                                <div class="col-xs-12 text-center">
                                                    <span class="counts block head-font"><span class="counter-anim">{{$customertotour->date}}</span></span>
                                                    <span class="counts-text block">Date Of Journey</span>
                                                </div>






                                            </div>

                                        </div>



                                    </div>




                                </div>
                            </div>
                        </div>
                    </div>
                </div>












                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">

                            </div>
                        </div>
                    </div>
                </div>
























            </div>
        </div>
    </div>















    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Price Info</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">

                                @if ( $tour_to_estimates == null  )

                                <form   method="POST"
                                        enctype="multipart/form-data"
                                        action="{{route('admin.order.estimate.save')}}"
                                >
                                    @csrf
                                    <div class="form-body">


                                        <input type="hidden" name="customer_id"  value="{{$customer->id}}"  >
                                        <input type="hidden" name="create_tour_id"  value="{{$customertotour->create_tour_id}}"  >
                                        <input type="hidden" name="tour_title"  value="{{$customertotour->tour_title}}"  >
                                        <input type="hidden" name="ip"  value="{{$customertotour->ip}}"  >
                                        <input type="hidden" name="date"  value="{{$customertotour->date}}"  >
                                        <input type="hidden" name="customer_to_tours"  value="{{$customertotour->id}}"  >




                                        <div class="row">

                                            <h6 style="padding: 20px"  class="panel-title txt-dark">Adult Price Info</h6>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 ">Base  Price</label>
                                                    <input name="adult_base_price" required   type="text"
                                                      value="{{$tour->price}}"     class="form-control adult-base-price"
                                                           placeholder="Price">
                                                </div>
                                            </div>


                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 ">Tax %</label>
                                                    <select class="form-control adult-taxper" name="adult_tax_per" id="">
                                                        <option value="{{$tour->tax_per}}">{{$tour->tax_per}}</option>
                                                        <option value="0">0</option>
                                                        <option value="5">5</option>
                                                        <option value="12">12</option>
                                                        <option value="18">18</option>
                                                        <option value="28">28</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label mb-10">Persons</label>
                                                    <input class="adult-person" id="adult"
                                                           type="text" value="{{$customertotour->adult}}"
                                                           name="adult_person"
                                                           data-bts-button-down-class="btn btn-default"
                                                           data-bts-button-up-class="btn btn-default">
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 ">Tax AMT</label>
                                                    <input name="adult_tax"   required readonly  type="text"
                                                           value="{{$tour->tax * $customertotour->adult}}"     class="form-control adult-taxamt"
                                                           placeholder="Tax AMT">
                                                </div>
                                            </div>




                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 ">Total</label>
                                                    <input name="adult_total"   required readonly  type="text"
                                                           value="{{$tour->total * $customertotour->adult }}"        class="form-control adult-total" placeholder="Total">
                                                </div>
                                            </div>

                                        </div>



                                        <div class="row">

                                            <h6 style="padding: 20px" class="panel-title txt-dark">Children Price Info</h6>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 ">Base  Price</label>
                                                    <input name="children_base_price" required   type="text"
                                                           value="{{$tour->price}}"     class="form-control children-base-price" placeholder="Price">
                                                </div>
                                            </div>


                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 ">Tax %</label>
                                                    <select class="form-control children-taxper" name="children_tax_per" id="">
                                                        <option value="{{$tour->tax_per}}">{{$tour->tax_per}}</option>
                                                        <option value="0">0</option>
                                                        <option value="5">5</option>
                                                        <option value="12">12</option>
                                                        <option value="18">18</option>
                                                        <option value="28">28</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label mb-10">Children</label>
                                                    <input class="children-person" id="adult" type="text" value="{{$customertotour->children}}" name="children_person"
                                                           data-bts-button-down-class="btn btn-default"
                                                           data-bts-button-up-class="btn btn-default">
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 ">Tax AMT</label>
                                                    <input name="children_tax"   required readonly  type="text"
                                                           value="{{$tour->tax * $customertotour->children }}"       class="form-control children-taxamt" placeholder="Tax AMT">
                                                </div>
                                            </div>




                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label mb-10 ">Total</label>
                                                    <input name="children_total"   required readonly  type="text"
                                                           value="{{$tour->total *  $customertotour->children}}"     class="form-control children-total" placeholder="Total">
                                                </div>
                                            </div>

                                        </div>



                                        <hr class="light-grey-hr"/>







                                    </div>
                                    <div class="form-actions mt-10">
                                        <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                        <button type="button" class="btn btn-default">Cancel</button>





                                    </div>
                                </form>

                                    @else
                                    <a target="_blank" href="{{route('admin.order.estimate.view',$tour_to_estimates->id)}}" type="button" class="btn btn-orange">View Estimate</a>
                                    <a target="_blank" href="{{route('admin.order.estimate.pfd.download',$tour_to_estimates->id)}}" type="button" class="btn btn-info">Download Estimate</a>
                                    <a target="_blank" href="{{route('admin.order.tour.plan.pfd.download',$tour_to_estimates->id)}}" type="button" class="btn btn-primary">Download Tour Plan</a>


                                @endif









                            </div>
                        </div>
                    </div>
                </div>












                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">

                            </div>
                        </div>
                    </div>
                </div>
























            </div>
        </div>
    </div>






















    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark"> Itinerary</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>


                <style>
                    .itinarydescimage{
                        width: 200px;
                    }
                </style>




                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                @foreach($itinaries as $itinarie)

                                    <div class="panel panel-success card-view">
                                        <div class="panel-heading">
                                            <div class="pull-left">
                                                <h6 class="panel-title txt-light">
                                                    {{$itinarie->title }} |
                                                    Day: {{$itinarie->day }}

                                                </h6>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div  class="panel-wrapper collapse in">
                                            <div  class="panel-body">



                                                <div class="col-md-12">
                                                    @foreach($itinarie->create_tour_to_itinary_to_inclusions as $inclusion)
                                                        <i style="size: 50px; padding: 15px; color: #0b97c4" class="{{$inclusion->text}}"></i>

                                                    @endforeach
                                                </div>


                                                <p>

                                                    {{$itinarie->description}}

                                                </p>


                                                <div class="col-md-3">

                                                    <a  data-fancybox="gallery" href="{{asset('/storage/'.$itinarie->img)}}"><img class="itinarydescimage"
                                                                                                                                  src="{{asset('/storage/'.$itinarie->img)}}"></a>




                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
























            </div>
        </div>
    </div>






@stop


@section('css')
    <link href="{{asset('admin/vendors/bower_components/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/vendors/bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin//vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet" type="text/css"/>
@stop


@section('script')

    <script src="{{asset('admin/vendors/bower_components/multiselect/js/jquery.multi-select.js')}}"></script>
    <script src="{{asset('/admin/dist/js/kqksrcmulsel.js')}}"></script>

    <script src="{{asset('admin/vendors/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('admin/vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js')}}"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>


    <script>

        $('.searchable').multiSelect({
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Type \"A Place Name\"'>",
            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Type \"A Place Name\"'>",
            afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';


                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                        if (e.which == 40){
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }
        });



    </script>



    <script>
        /*Form advanced Init*/
        $(document).ready(function() {
            "use strict";




            $("input[name='adult_person']").TouchSpin({
                min: 0,
                max: 100,
                step: 1,
                decimals: 0,
                boostat: 5,
                maxboostedstep: 10,
            });

            $("input[name='children_person']").TouchSpin({
                min: 0,
                max: 100,
                step: 1,
                decimals: 0,
                boostat: 5,
                maxboostedstep: 10,
            });

        });
    </script>


    <script>


        $(document).ready(function(){

            //adult

            $(".adult-base-price").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".adult-taxper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();

            }));

                $(".adult-person").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();

                }));




            function onPriceupdate() {
                var BASEPRICE = $(".adult-base-price").val();
                var TAXPERCENTAGE = $(".adult-taxper option:selected").val();
                var PERSON = $(".adult-person").val();
                var DISCOUNT = 0;


                //cal gst
                //
                //
                //

                var TocalculatePrice = BASEPRICE * PERSON;


                var TOTALTAX = TocalculatePrice * (TAXPERCENTAGE / 100);
                var TOTAL = TocalculatePrice + TOTALTAX;


                $(".adult-taxamt").val(TOTALTAX.toFixed(2));
                // $(".product-tax-amt").val(TOTALTAX.toFixed(2));
                $(".adult-total").val((Number(TocalculatePrice) + Number(TOTALTAX)).toFixed(2));


            }


        });
    </script>
    <script>


        $(document).ready(function(){

            //adult

            $(".children-base-price").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".children-taxper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();

            }));

                $(".children-person").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();

                }));




            function onPriceupdate() {
                var BASEPRICE = $(".children-base-price").val();
                var TAXPERCENTAGE = $(".children-taxper option:selected").val();
                var PERSON = $(".children-person").val();
                var DISCOUNT = 0;


                //cal gst
                //
                //
                //

                var TocalculatePrice = BASEPRICE * PERSON;


                var TOTALTAX = TocalculatePrice * (TAXPERCENTAGE / 100);
                var TOTAL = TocalculatePrice + TOTALTAX;


                $(".children-taxamt").val(TOTALTAX.toFixed(2));
                // $(".product-tax-amt").val(TOTALTAX.toFixed(2));
                $(".children-total").val((Number(TocalculatePrice) + Number(TOTALTAX)).toFixed(2));


            }


        });
    </script>


@stop
