@extends('admin.index')


@section('content')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">



            <!-- Row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view  pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body  pa-0">
                                        <div class="profile-box">
                                            <div class="profile-cover-pic">
                                                <div class="fileupload btn btn-default">
                                                    <span class="btn-text">edit</span>

                                                    <form   method="POST"
                                                            enctype="multipart/form-data"
                                                            action="{{route('admin.cab.update.bg.image',$city->id)}}"
                                                            class="bgimgform"
                                                    >
                                                        @csrf

                                                    <input class="upload bgimgforminput " name="bg_img" type="file">

                                                    </form>


                                                </div>
                                                <div
                                                    style="background-image: url('{{asset('/storage/'.$city->bg_img)}}')"
                                                    class="profile-image-overlay"></div>
                                            </div>
                                            <div class="profile-info text-center mb-15">
                                                <div class="profile-img-wrap">
                                                    <img class="inline-block mb-10"
                                                         src="{{asset('/storage/'.$city->sm_img)}}" alt="user"/>
                                                    <div class="fileupload btn btn-default">
                                                        <span class="btn-text">edit</span>


                                                        <form   method="POST"
                                                                enctype="multipart/form-data"
                                                                action="{{route('admin.cab.update.sm.image',$city->id)}}"
                                                                class="smimgform"
                                                        >
                                                            @csrf

                                                            <input class="upload smimgforminput"  name="sm_img" type="file">

                                                        </form>


                                                    </div>
                                                </div>
                                                <h5 class="block mt-10 weight-500 capitalize-font txt-dark">{{$city->name}}</h5>
                                                <p style="padding: 10px" class="block">{{$city->description}}</p>
                                            </div>


                                            <div class="social-info">
                                                <div class="row">
                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">

                                                                @if ($city->type === 'Sedan')
                                                                    <img src="{{asset('/admin/img/carmake/sed.png')}}" alt="">
                                                                    @elseif($city->type === 'SUV')

                                                                    <img src="{{asset('/admin/img/carmake/suv.png')}}" alt="">

                                                                    @elseif($city->type === 'HatchBack')
                                                                    <img src="{{asset('/admin/img/carmake/hb.png')}}" alt="">
                                                                    @else


                                                                @endif

                                                                <br>

                                                               ( {{$city->type}})
                                                            </span></span>
                                                        <span class="counts-text block"> Type</span>



                                                    </div>

                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font">
                                                            <span class="counter-anim">

                                                                <br>

                                                                 @for ($i = 0; $i < $city->capacity; $i++)



                                                                    <i style="font-size: 45px; color: green; padding-top: 40px; margin-bottom: 20px"
                                                                       class="icon-user-following"></i>
                                                                @endfor
                                                                <br>
                                                               <div style="height: 25px;"></div>
                                                                ({{$city->capacity}} Person)

                                                            </span></span>





                                                        <span class="counts-text block"> Capacity</span>
                                                    </div>




                                                </div>





                                              {{--mod 1 --}}



















                                            </div>





                                            <div class="social-info">
                                                <div class="row">
                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">{{$city->segment}}</span></span>
                                                        <span class="counts-text block">Segment</span>
                                                    </div>


                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">
                                                                <i class="fa fa-inr"></i>
                                                                {{$city->price}}</span></span>
                                                        <span class="counts-text block">Starting Price</span>
                                                    </div>






                                                </div>






                                              {{--mod 1 --}}



                                                <style>
                                                    .pac-container {
                                                        z-index: 10000 !important;
                                                    }
                                                </style>

                                                <button class="btn btn-orange btn-block  btn-anim mt-15" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i><span class="btn-text">edit profile</span></button>
                                                <div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h5 class="modal-title" id="myModalLabel">Edit Profile</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <!-- Row -->
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="">
                                                                            <div class="panel-wrapper collapse in">
                                                                                <div class="panel-body pa-0">
                                                                                    <div class="col-sm-12 col-xs-12">
                                                                                        <div class="form-wrap">




                                                                                            <form   method="POST"
                                                                                                    enctype="multipart/form-data"
                                                                                                    action="{{route('admin.cab.update',$city->id)}}"
                                                                                            >
                                                                                                @csrf





                                                                                                <div class="form-body">
                                                                                                    <h6 class="txt-dark capitalize-font">
                                                                                                        <i class="zmdi zmdi-pin mr-10"></i>Car's Info</h6>
                                                                                                    <hr class="light-grey-hr"/>
                                                                                                    <div class="row">



                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label  class="control-label mb-10">Car's Name</label>
                                                                                                                <input name="name" type="text"  class="form-control"
                                                                                                                       value="{{$city->name}}"        placeholder="Place's Name">

                                                                                                            </div>




                                                                                                        </div>
                                                                                                        <!--/span-->
                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10">Short Info</label>
                                                                                                                <input type="text"  value="{{$city->description}}"   name="description" class="form-control"
                                                                                                                       required  placeholder="Short Info">

                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!--/span-->
                                                                                                    </div>
                                                                                                    <!-- /Row -->
                                                                                                    <div class="row">

                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10">Type</label>

                                                                                                                <select class="form-control" name="type" id="">
                                                                                                                    <option value="{{$city->type}}">{{$city->type}}</option>
                                                                                                                    <option value="Sedan">Sedan</option>
                                                                                                                    <option value="SUV">SUV</option>
                                                                                                                    <option value="HatchBack">HatchBack</option>
                                                                                                                    <option value="Others">Others</option>
                                                                                                                </select>




                                                                                                            </div>
                                                                                                        </div>





                                                                                                        <!--/span-->
                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">

                                                                                                                <label class="control-label mb-10">Capacity</label>
                                                                                                                <select class="form-control" name="capacity" id="">
                                                                                                                    <option value="{{$city->capacity}}">{{$city->capacity}}</option>
                                                                                                                    <option value="5">5</option>
                                                                                                                    <option value="7">7</option>
                                                                                                                    <option value="8">8</option>
                                                                                                                    <option value="10">10</option>
                                                                                                                    <option value="10+">10+</option>

                                                                                                                </select>

                                                                                                            </div>
                                                                                                        </div>


                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10">Segment</label>

                                                                                                                <select class="form-control" name="segment" id="">
                                                                                                                    <option value="{{$city->segment}}">{{$city->segment}}</option>
                                                                                                                    <option value="Basic">Basic</option>
                                                                                                                    <option value="Premium">Premium</option>
                                                                                                                    <option value="Luxury">Luxury</option>

                                                                                                                </select>

                                                                                                            </div>
                                                                                                        </div>


                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10 ">Base  Price</label>
                                                                                                                <input name="price" required   type="text"
                                                                                                                       value="{{$city->price}}"      class="form-control baseprice" placeholder="Price">
                                                                                                            </div>
                                                                                                        </div>


                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10 ">Tax %</label>
                                                                                                                <select class="form-control taxper" name="tax_per" id="">
                                                                                                                    <option value="{{$city->tax_per}}">{{$city->tax_per}}</option>
                                                                                                                    <option value="0">0</option>
                                                                                                                    <option value="5">5</option>
                                                                                                                    <option value="12">12</option>
                                                                                                                    <option value="18">18</option>
                                                                                                                    <option value="28">28</option>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10 ">Tax AMT</label>
                                                                                                                <input name="tax"   required readonly  type="text"
                                                                                                                       value="{{$city->tax}}"     class="form-control taxamt" placeholder="Tax AMT">
                                                                                                            </div>
                                                                                                        </div>


                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10 ">Cess %</label>
                                                                                                                <input name="cess_per"   required   type="text"
                                                                                                                       value="{{$city->cess_per}}"       class="form-control cessper" placeholder="Cess %">
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10 ">Cess</label>
                                                                                                                <input name="cess"   required readonly  type="text"
                                                                                                                       value="{{$city->cess}}"       class="form-control cessamt" placeholder="Cess ">
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10 ">Total</label>
                                                                                                                <input name="total"   required readonly  type="text"
                                                                                                                       value="{{$city->total}}"      class="form-control total" placeholder="Total">
                                                                                                            </div>
                                                                                                        </div>





                                                                                                        <!--/span-->
                                                                                                    </div>
                                                                                                    <!-- /Row -->

                                                                                                    <hr class="light-grey-hr"/>


                                                                                                    <!-- /Row -->



                                                                                                </div>
                                                                                                <div class="form-actions mt-10">
                                                                                                    <button type="submit" class="btn btn-success  mr-10"> Update</button>
                                                                                                    <button type="button" class="btn btn-default">Cancel</button>
                                                                                                </div>






                                                                                            </form>





                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">

                                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>















                                            </div>





                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>








            </div>
            <!-- /Row -->









@stop


@section('script')


    <script>
        $('.bgimgforminput').change(function() {
            // select the form and submit
            $('.bgimgform').submit();
        });

        $('.smimgforminput').change(function() {
            // select the form and submit
            $('.smimgform').submit();
        });
    </script>



    <script>


        $(document).ready(function(){


            $(".baseprice").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".taxper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".cessper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))




















            function onPriceupdate() {
                var BASEPRICE =  $(".baseprice").val();
                var TAXPERCENTAGE =  $(".taxper option:selected").val();
                var CESSPERCENTAGE =  $(".cessper").val();
                var DISCOUNT =  0;




                //cal gst
                //
                //
                //

                var TocalculatePrice =  BASEPRICE;


                //   var  CGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2;
                //  var   SGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2
                //  var   IGST =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALTAX =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALCESS =  TocalculatePrice * (CESSPERCENTAGE/100);
                var   TOTAL = TocalculatePrice + TOTALTAX;


                //   $(".product-cgst").val(CGST.toFixed(2));
                //   $(".product-sgst").val(SGST.toFixed(2));
                //  $(".product-igst").val(IGST.toFixed(2));
                $(".taxamt").val(TOTALTAX.toFixed(2));
                $(".cessamt").val(TOTALCESS.toFixed(2));
                // $(".product-tax-amt").val(TOTALTAX.toFixed(2));
                $(".total").val((Number(TocalculatePrice) + Number(TOTALTAX)+ Number(TOTALCESS)).toFixed(2));




            }

        });
    </script>





    {{-- open street maps view--}}

@stop
