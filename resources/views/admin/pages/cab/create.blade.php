@extends('admin.index')


@section('content')




    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Create Car</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">Admin</a></li>
                <li><a href="#"><span>Car</span></a></li>
                <li class="active"><span>Create Car</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Car</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">
                                    <form   method="POST"
                                            enctype="multipart/form-data"
                                            action="{{route('admin.save.cab')}}"
                                    >
                                        @csrf
                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font">
                                                <i class="zmdi zmdi-pin mr-10"></i>Car's Info</h6>
                                            <hr class="light-grey-hr"/>
                                            <div class="row">



                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label  class="control-label mb-10">Car's Name</label>
                                                        <input name="name" type="text"  class="form-control"
                                                               id="autocomplete"          placeholder="Place's Name">

                                                    </div>




                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Short Info</label>
                                                        <input type="text" name="description" class="form-control"
                                                             required  placeholder="Short Info">

                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Type</label>

                                                        <select class="form-control" name="type" id="">
                                                            <option value="Sedan">Sedan</option>
                                                            <option value="SUV">SUV</option>
                                                            <option value="HatchBack">HatchBack</option>
                                                            <option value="Others">Others</option>
                                                        </select>




                                                    </div>
                                                </div>





                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">

                                                        <label class="control-label mb-10">Capacity</label>
                                                        <select class="form-control" name="capacity" id="">
                                                            <option value="5">5</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="10">10</option>
                                                            <option value="10+">10+</option>

                                                        </select>

                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Segment</label>

                                                        <select class="form-control" name="segment" id="">
                                                            <option value="Basic">Basic</option>
                                                            <option value="Premium">Premium</option>
                                                            <option value="Luxury">Luxury</option>

                                                        </select>

                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Base  Price</label>
                                                        <input name="price" required   type="text"
                                                               class="form-control baseprice" placeholder="Price">
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Tax %</label>
                                                        <select class="form-control taxper" name="tax_per" id="">
                                                            <option value="0">0</option>
                                                            <option value="5">5</option>
                                                            <option value="12">12</option>
                                                            <option value="18">18</option>
                                                            <option value="28">28</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Tax AMT</label>
                                                        <input name="tax"   required readonly  type="text"
                                                               class="form-control taxamt" placeholder="Tax AMT">
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Cess %</label>
                                                        <input name="cess_per"   required   type="text"
                                                               class="form-control cessper" placeholder="Cess %">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Cess</label>
                                                        <input name="cess"   required readonly  type="text"
                                                               class="form-control cessamt" placeholder="Cess ">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Total</label>
                                                        <input name="total"   required readonly  type="text"
                                                               class="form-control total" placeholder="Total">
                                                    </div>
                                                </div>





                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->

                                            <hr class="light-grey-hr"/>

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Large Image</label>
                                                        <input name="bg_img" type="file"  class="form-control"
                                                            required   placeholder="Siliguri">

                                                    </div>
                                                </div>





                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Small Image</label>
                                                        <input name="sm_img"  required  type="file" class="form-control">
                                                    </div>
                                                </div>

                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->



                                        </div>
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                            <button type="button" class="btn btn-default">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







@stop


@section('css')

@stop


@section('script')

    <script>


        $(document).ready(function(){


        $(".baseprice").on("change paste keyup keydown keypress",(function () {
            //  var LISTINGPRICE = ;
            onPriceupdate();
        }))

        $(".taxper").on("change paste keyup keydown keypress",(function () {
            //  var LISTINGPRICE = ;
            onPriceupdate();
        }))

        $(".cessper").on("change paste keyup keydown keypress",(function () {
            //  var LISTINGPRICE = ;
            onPriceupdate();
        }))




















        function onPriceupdate() {
            var BASEPRICE =  $(".baseprice").val();
            var TAXPERCENTAGE =  $(".taxper option:selected").val();
            var CESSPERCENTAGE =  $(".cessper").val();
            var DISCOUNT =  0;




            //cal gst
            //
            //
            //

            var TocalculatePrice =  BASEPRICE;


         //   var  CGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2;
          //  var   SGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2
          //  var   IGST =  TocalculatePrice * (TAXPERCENTAGE/100);
            var   TOTALTAX =  TocalculatePrice * (TAXPERCENTAGE/100);
            var   TOTALCESS =  TocalculatePrice * (CESSPERCENTAGE/100);
            var   TOTAL = TocalculatePrice + TOTALTAX;


         //   $(".product-cgst").val(CGST.toFixed(2));
         //   $(".product-sgst").val(SGST.toFixed(2));
          //  $(".product-igst").val(IGST.toFixed(2));
            $(".taxamt").val(TOTALTAX.toFixed(2));
            $(".cessamt").val(TOTALCESS.toFixed(2));
           // $(".product-tax-amt").val(TOTALTAX.toFixed(2));
            $(".total").val((Number(TocalculatePrice) + Number(TOTALTAX)+ Number(TOTALCESS)).toFixed(2));




        }

        });
    </script>





@stop
