@extends('admin.index')


@section('content')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">



            <!-- Row -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view  pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body  pa-0">
                                        <div class="profile-box">
                                            <div class="profile-cover-pic">
                                                <div class="fileupload btn btn-default">
                                                    <span class="btn-text">edit</span>

                                                    <form   method="POST"
                                                            enctype="multipart/form-data"
                                                            action="{{route('admin.room.update.bg.image',$city->id)}}"
                                                            class="bgimgform"
                                                    >
                                                        @csrf

                                                    <input class="upload bgimgforminput " name="bg_img" type="file">

                                                    </form>


                                                </div>
                                                <div
                                                    style="background-image: url('{{asset('/storage/'.$city->img_bg)}}')"
                                                    class="profile-image-overlay"></div>
                                            </div>
                                            <div class="profile-info text-center mb-15">
                                                <div class="profile-img-wrap">
                                                    <img class="inline-block mb-10"
                                                         src="{{asset('/storage/'.$city->img_sm)}}" alt="user"/>
                                                    <div class="fileupload btn btn-default">
                                                        <span class="btn-text">edit</span>


                                                        <form   method="POST"
                                                                enctype="multipart/form-data"
                                                                action="{{route('admin.room.update.sm.image',$city->id)}}"
                                                                class="smimgform"
                                                        >
                                                            @csrf

                                                            <input class="upload smimgforminput"  name="sm_img" type="file">

                                                        </form>


                                                    </div>
                                                </div>
                                                <h5 class="block mt-10 weight-500 capitalize-font txt-dark">{{$city->name}}</h5>
                                                <p style="padding: 10px" class="block">{{$hotel->name}}</p>
                                            </div>
                                            <div class="social-info">
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">{{$hotel->address}}</span></span>
                                                        <span class="counts-text block"> Address</span>
                                                    </div>

                                                    <div class="col-xs-12 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">{{$linkedcity->name}}</span></span>
                                                        <span class="counts-text block">City</span>
                                                    </div>


                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">{{$hotel->phone}}</span></span>
                                                        <span class="counts-text block">Phone</span>
                                                    </div>

                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">
                                                              <i class="fa fa-inr"></i>  {{$city->price}} / Day </span></span>
                                                        <span class="counts-text block">On {{$city->room_capacity}} Person Sharing Basis</span>
                                                    </div>










                                                </div>







                                            </div>














                                            <div class="social-info">
                                                <div class="row">
                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">
                                                            @for ($i = 0; $i < $city->room_capacity; $i++)



                                                                    <i style="font-size: 30px; color: #69abff; "
                                                                       class="fa fa-user"></i>
                                                                @endfor
                                                            </span></span>
                                                        <span class="counts-text block">Persons</span>
                                                    </div>


                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">

                                                                @for ($i = 0; $i < $hotel->star; $i++)



                                                                    <i style="font-size: 30px; color: goldenrod; "
                                                                       class="fa fa-star"></i>
                                                                @endfor

                                                            </span></span>
                                                        <span class="counts-text block">Star</span>
                                                    </div>





                                                </div>



                                                <button class="btn btn-orange btn-block  btn-anim mt-15" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i><span class="btn-text">edit profile</span></button>

                                                <button class="btn btn-orange btn-block  btn-anim mt-15" data-toggle="modal" data-target="#myModaltwo">
                                                    <i class="fa fa-photo"></i><span class="btn-text">Upload Gallery</span></button>


                                                {{--mod 1 --}}


                                                <style>
                                                    .pac-container {
                                                        z-index: 10000 !important;
                                                    }
                                                </style>





                                            </div>







                                            {{--modal--}}

                                            <div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h5 class="modal-title" id="myModalLabel">Edit Hotel</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- Row -->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="">
                                                                        <div class="panel-wrapper collapse in">
                                                                            <div class="panel-body pa-0">
                                                                                <div class="col-sm-12 col-xs-12">
                                                                                    <div class="form-wrap">




                                                                                        <form   method="POST"
                                                                                                enctype="multipart/form-data"
                                                                                                action="{{route('admin.room.update',$city->id)}}"
                                                                                        >
                                                                                            @csrf

                                                                                            <div class="form-body">
                                                                                                <h6 class="txt-dark capitalize-font">
                                                                                                    <i class="zmdi zmdi-pin mr-10"></i>Room's Info</h6>
                                                                                                <hr class="light-grey-hr"/>
                                                                                                <div class="row">



                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label  class="control-label mb-10">Room Name</label>
                                                                                                            <input name="name" value="{{$city->name}}" type="text"  class="form-control"
                                                                                                                   placeholder="Room Name">
                                                                                                            <input type="hidden"  class="form-control" required
                                                                                                                   value="{{$linkedcity->id}}"  name="create_city_id"
                                                                                                            >


                                                                                                            <input type="hidden"  class="form-control" required
                                                                                                                   value="{{$hotel->id}}"  name="create_hotel_id" >


                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <!--/span-->
                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10">Short Info</label>
                                                                                                            <input type="text" name="description" class="form-control"
                                                                                                                   required value="{{$city->description}}"  placeholder="Short Info">

                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10">Room Capacity</label>
                                                                                                            <input type="text" value="{{$city->room_capacity}}" name="room_capacity" class="form-control"
                                                                                                                   required  placeholder="Room Capacity">

                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <!--/span-->
                                                                                                </div>
                                                                                                <!-- /Row -->

                                                                                                <!-- /Row -->

                                                                                                <hr class="light-grey-hr"/>


                                                                                                <div class="row">

                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Base  Price</label>
                                                                                                            <input name="price" required value="{{$city->price}}"   type="text"
                                                                                                                   class="form-control baseprice" placeholder="Price">
                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Tax %</label>
                                                                                                            <select class="form-control taxper" name="tax_per" id="">
                                                                                                                <option value="{{$city->tax_per}}">{{$city->tax_per}}</option>
                                                                                                                <option value="0">0</option>
                                                                                                                <option value="5">5</option>
                                                                                                                <option value="12">12</option>
                                                                                                                <option value="18">18</option>
                                                                                                                <option value="28">28</option>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Tax AMT</label>
                                                                                                            <input name="tax" value="{{$city->tax}}"   required readonly  type="text"
                                                                                                                   class="form-control taxamt" placeholder="Tax AMT">
                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Cess %</label>
                                                                                                            <input name="cess_per" value="{{$city->cess_per}}"  required   type="text"
                                                                                                                   class="form-control cessper" placeholder="Cess %">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Cess</label>
                                                                                                            <input name="cess" value="{{$city->cess}}"   required readonly  type="text"
                                                                                                                   class="form-control cessamt" placeholder="Cess ">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Total</label>
                                                                                                            <input name="total" value="{{$city->total}}"   required readonly  type="text"
                                                                                                                   class="form-control total" placeholder="Total">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>






                                                                                            </div>










                                                                                            <div class="form-actions mt-10">
                                                                                                <button type="submit" class="btn btn-success  mr-10"> Update</button>

                                                                                            </div>
                                                                                        </form>





                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">

                                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>



                                            {{--mod 1 end--}}



                                            {{--mod 1 --}}


                                            <div id="myModaltwo" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabeltwo" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h5 class="modal-title" id="myModalLabeltwo">Edit Profile</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- Row -->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="">
                                                                        <div class="panel-wrapper collapse in">
                                                                            <div class="panel-body pa-0">
                                                                                <div class="col-sm-12 col-xs-12">
                                                                                    <div class="form-wrap">



                                                                                        <form method="post" action="{{route('admin.room.add.gallery',$city->id)}}"
                                                                                              enctype="multipart/form-data"
                                                                                              class="dropzone" id="dropzone">
                                                                                            @csrf
                                                                                        </form>









                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">

                                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>



                                            {{--mod 1 end--}}

                                            {{--modal--}}





















                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>




                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div  class="panel-body pb-0">
                                        <div class="col-md-12 pb-20">




                                            <div style="height: 670px" id="map"></div>





                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



            </div>
            <!-- /Row -->


    <div class="row">
        <div class="col-sm-12">
            
            
            
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div  class="panel-body pb-0">
                                    <div class="col-md-12 pb-20">


                                        <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0"
                                                marginwidth="0" src="https://maps.google.com/maps?q={{urlencode($hotel->google_name)}}&amp;output=embed"></iframe>


                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            
            
            
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div  class="panel-body pb-0">
                                    <div class="col-md-12 pb-20">
                                        <div class="gallery-wrap">
                                            <div class="portfolio-wrap project-gallery">
                                                <ul id="portfolio_1" class="portf auto-construct  project-gallery" data-col="4">


                                                    @foreach($images as $image)

                                                      <div class="col-md-6">
                                                          <li  class="item"   data-src="{{asset('/storage/'.$image->location)}}" data-sub-html="{{$city->name}}" >
                                                              <a href="">
                                                                  <img style="height: 300px !important;" class="img-responsive" src="{{asset('/storage/'.$image->location)}}"  alt="Image description" />
                                                                  <span class="hover-cap">{{$city->name}}</span>
                                                              </a>
                                                          </li>

                                                          <a class="btn btn-danger" href="{{route('admin.room.delete.gallery',$image->id)}}">Delete</a>

                                                      </div>
                                                    @endforeach



                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            
            
            
            
            
        </div>
    </div>






@stop


@section('script')



    <script>
        $('.bgimgforminput').change(function() {
            // select the form and submit
            $('.bgimgform').submit();
        });

        $('.smimgforminput').change(function() {
            // select the form and submit
            $('.smimgform').submit();
        });
    </script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>


    <script type="text/javascript">
        Dropzone.options.dropzone =
            {
                maxFilesize: 12,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time+file.name;
                },
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                timeout: 5000,
                success: function(file, response)
                {
                    console.log(response);
                },
                error: function(file, response)
                {
                    return false;
                }
            };
    </script>


    {{-- open street maps view--}}

    <style>
        .leaflet-bottom {
            visibility: hidden;
        }
    </style>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
          integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
            integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
            crossorigin=""></script>




    <script>


        $(document).ready(function(){


            $(".baseprice").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".taxper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".cessper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))




















            function onPriceupdate() {
                var BASEPRICE =  $(".baseprice").val();
                var TAXPERCENTAGE =  $(".taxper option:selected").val();
                var CESSPERCENTAGE =  $(".cessper").val();
                var DISCOUNT =  0;




                //cal gst
                //
                //
                //

                var TocalculatePrice =  BASEPRICE;


                //   var  CGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2;
                //  var   SGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2
                //  var   IGST =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALTAX =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALCESS =  TocalculatePrice * (CESSPERCENTAGE/100);
                var   TOTAL = TocalculatePrice + TOTALTAX;


                //   $(".product-cgst").val(CGST.toFixed(2));
                //   $(".product-sgst").val(SGST.toFixed(2));
                //  $(".product-igst").val(IGST.toFixed(2));
                $(".taxamt").val(TOTALTAX.toFixed(2));
                $(".cessamt").val(TOTALCESS.toFixed(2));
                // $(".product-tax-amt").val(TOTALTAX.toFixed(2));
                $(".total").val((Number(TocalculatePrice) + Number(TOTALTAX)+ Number(TOTALCESS)).toFixed(2));




            }

        });
    </script>


    <script>

        function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2-lat1);  // deg2rad below
            var dLon = deg2rad(lon2-lon1);
            var a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                Math.sin(dLon/2) * Math.sin(dLon/2)
            ;
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c; // Distance in km
            return d;
        }

        function deg2rad(deg) {
            return deg * (Math.PI/180)
        }



        var map = L.map('map').setView([{{$hotel->lat}}, {{$hotel->long}}], 12);


        var HotelIcon = L.icon({
            iconUrl: '{{asset('/admin/img/icon/hotel.png')}}',
            iconSize: [45, 45],
        });

        var PlaceIcon = L.icon({
            iconUrl: '{{asset('/admin/img/icon/placeholder.png')}}',
            iconSize: [45, 45],
        });



        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);



        @foreach($linkedplaces as $place)

        L.marker([{{$place->lat}}, {{$place->long}}],{icon: PlaceIcon}).addTo(map)
            .bindPopup(' <h6>{{$place->name}} </h6> '
                +'<p style=" color: green;"> Distance from Hotel is :  <i class="fa fa-road"></i> ' + getDistanceFromLatLonInKm('{{$hotel->lat}}','{{$hotel->long}}','{{$place->lat}}','{{$place->long}}').toFixed(2)+ ' KM</p>'
                +'<br> {{$place->description}}')
            .openPopup();

            @endforeach

            L.circle([{{$hotel->lat}}, {{$hotel->long}}], {
                color: '#69abff',
                fillColor: '#69abff',
                fillOpacity: 0.3,
                radius: 5000
            }).addTo(map);

            L.marker([{{$hotel->lat}}, {{$hotel->long}}],{icon: HotelIcon}).addTo(map)
                .bindPopup(' <h6>{{$hotel->name}} </h6> '

                    +' @for ($i = 0; $i < $hotel->star; $i++)' +


                    '  <i style="color: yellow" class="fa fa-star"></i> @endfor'

                    +'{{$hotel->star}} Star'
                    +'<p >Available Rooms at <span style="color:#69abff;">{{$hotel->name}}</span></p>'
                    +'@foreach($avlroms as $avlrom) {{$avlrom->name}} | {{$avlrom->room_capacity}} Persons | <i class="fa fa-inr"></i> {{$avlrom->price}} <br>  @endforeach'

                )
                .openPopup();


    </script>



    {{-- open street maps view--}}

@stop
