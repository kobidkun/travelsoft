@extends('admin.index')


@section('content')




    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">CUSTOMER</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">Admin</a></li>
                <li><a href="#"><span>Master</span></a></li>
                <li class="active"><span>Customer</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Customer</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">

                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif


                                    <form   method="POST"
                                            enctype="multipart/form-data"
                                            action="{{route('admin.save.customer')}}"
                                    >
                                        @csrf
                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font">
                                                <i class="zmdi zmdi-pin mr-10"></i>Customer's Info</h6>
                                            <hr class="light-grey-hr"/>


                                            <div class="row">



                                                <div class="col-md-6">
                                                    <div class="form-group">

                                                        <label  class="control-label mb-10">First Name
                                                             </label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="icon-user"></i></div>
                                                        <input name="fname" type="text"  class="form-control"
                                                                        placeholder="First Name">
                                                        </div>




                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Last Name</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="icon-user"></i></div>
                                                        <input type="text" name="lname" class="form-control"
                                                             required  placeholder="Last Name">
                                                        </div>

                                                    </div>
                                                </div>

                                                <!--/span-->
                                            </div>



                                            <div class="row">



                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label  class="control-label mb-10">Mobile
                                                             </label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="icon-phone-1"></i></div>
                                                        <input name="phone" type="text"  class="form-control"
                                                                        placeholder="Mobile">
                                                        </div>




                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Email</label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="icon-email"></i></div>
                                                        <input type="text" name="email" class="form-control"
                                                             required  placeholder="Email">
                                                        </div>

                                                    </div>
                                                </div>

                                                <!--/span-->
                                            </div>



                                            <div class="row">



                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label  class="control-label mb-10">City
                                                             </label>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="icon-building"></i></div>
                                                        <input name="city" type="text"  class="form-control"
                                                                        placeholder="City">
                                                        </div>




                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">

                                                        <label class="control-label mb-10">State</label>

                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="icon-globe"></i></div>
                                                        <input type="text" name="state" class="form-control"
                                                             required  placeholder="State">

                                                        </div>

                                                    </div>
                                                </div>

                                                <!--/span-->
                                            </div>




                                        </div>
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>


                            <div style="height: 250px"></div>



                            <div class="col-sm-12 col-xs-12">



                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="users-table" class="table table-hover display  pb-30" >

                                            <thead>
                                            <tr>

                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>State</th>
                                                <th>City</th>
                                                <th>Delete</th>


                                            </tr>
                                            </thead>




                                        </table>
                                    </div>
                                </div>


                            </div>


















                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







@stop


@section('css')



@stop


@section('script')

    <link href="{{asset('admin/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css')}}">
    <script src="{{asset('admin/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/dataTables-data.js')}}"></script>



    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.customer.datatables') !!}',
                columns: [
                    { data: 'fname', name: 'fname' },
                    { data: 'lname', name: 'lname' },
                    { data: 'email', name: 'email' },
                    { data: 'phone', name: 'phone' },
                    { data: 'state', name: 'state' },
                    { data: 'city', name: 'city' },



                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>
