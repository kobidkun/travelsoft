@extends('admin.index')


@section('content')




    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">CUSTOMER</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">Admin</a></li>
                <li><a href="#"><span>Master</span></a></li>
                <li class="active"><span>Customer</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->






                        <div class="row">







                                        <!-- Title -->

                                        <!-- /Title -->

                                        <!-- Row -->
                                        <div class="row">


                                            <div class="col-sm-4">
                                                <div class="panel panel-default card-view colorgradnam">
                                                    <div class="panel-wrapper collapse in">
                                                        <div class="panel-body pt-0">
                                                            <div class="row">


                                                                <div class="col-md-12 colorgradnam">
                                                                    <div class="product-detail-wrap">

                                                                        <h4 class="mb-5 weight-500">{{$customer->fname}}  {{$customer->lname}}  </h4>


                                                                        <p class="mb-25">
                                                                      <i class="icon-email"></i>      {{$customer->email}}
                                                                        </p>
                                                                        <p class="mb-25">
                                                                            <i class="icon-phone"></i>      {{$customer->phone}}
                                                                        </p>
                                                                        <p class="mb-25">
                                                                            <i class="icon-pin"></i>       {{$customer->city}}
                                                                        </p>
                                                                        <p class="mb-25">
                                                                            <i class="icon-globe"></i>      {{$customer->state}}
                                                                        </p>





                                                                    </div>
                                                                </div>





                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <style>
                                                .colorgradorder{
                                                    background: #7F00FF;  /* fallback for old browsers */
                                                    background: -webkit-linear-gradient(to right, #E100FF, #7F00FF);  /* Chrome 10-25, Safari 5.1-6 */
                                                    background: linear-gradient(to right, #E100FF, #7F00FF); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

                                                }

                                                .colorgradthree{
                                                    background: #E0EAFC;  /* fallback for old browsers */
                                                    background: -webkit-linear-gradient(to bottom, #CFDEF3, #E0EAFC);  /* Chrome 10-25, Safari 5.1-6 */
                                                    background: linear-gradient(to bottom, #CFDEF3, #E0EAFC); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

                                                }

                                                .col8765{
                                                    color: white;

                                                }
                                            </style>

                                            <style>
                                                .colorgradnam{
                                                    background: #fceabb;  /* fallback for old browsers */
                                                    background: -webkit-linear-gradient(to bottom, #f8b500, #fceabb);  /* Chrome 10-25, Safari 5.1-6 */
                                                    background: linear-gradient(to bottom, #f8b500, #fceabb); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

                                                }

                                                .col8765{
                                                    color: white;

                                                }
                                            </style>

                                            <div class="col-sm-4 ">
                                                <div class="panel panel-default card-view colorgradthree">
                                                    <div class="panel-wrapper collapse in">
                                                        <div class="panel-body pt-0">
                                                            <div class="row ">


                                                                <div class="col-md-12 colorgradthree">

                                                                 @if ($lastorders != null)


                                                                    <div class="product-detail-wrap">



                                                                        <h4 class="mb-5  weight-500"> Latest Orders </h4>


                                                                        <p class="mb-25 ">
                                                                            <i class="icon-location-2"></i>    {{$lastorders->tour_title}}
                                                                        </p>

                                                                        <div class="col-md-6">
                                                                            <p class="mb-25 ">
                                                                                <i class="icon-child"></i>       {{$lastorders->children}} children
                                                                            </p>

                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <p class="mb-25 ">
                                                                                <i class="icon-user"></i>      {{$lastorders->adult}} Person(s)
                                                                            </p>

                                                                        </div>

                                                                        <p class="mb-25 ">
                                                                            <i class="icon-calendar"></i>      {{$lastorders->date}}
                                                                        </p>

                                                                        <p class="mb-25 ">
                                                                            <i class="icon-calendar"></i>      {{\Carbon\Carbon::parse($lastorders->created_at)->toDayDateTimeString()}}
                                                                        </p>





                                                                    </div>
                                                                    @else

                                                                        <div class="product-detail-wrap">



                                                                            <h4 class="mb-5  weight-500"> No  Orders </h4>


                                                                            <p class="mb-25 ">


                                                                                 </p>





                                                                        </div>

                                                                    @endif

                                                                </div>





                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-4 ">
                                                <div class="panel panel-default card-view colorgradorder">
                                                    <div class="panel-wrapper collapse in">
                                                        <div class="panel-body pt-0">
                                                            <div class="row ">


                                                                <div class="col-md-12 colorgradorder">
                                                                    <div class="product-detail-wrap">

                                                                        <h4 class="mb-5 col8765 weight-500"> Latest Order </h4>


                                                                        <p class="mb-25 col8765">
                                                                      <i class="icon-email"></i>      {{$customer->email}}
                                                                        </p>
                                                                        <p class="mb-25 col8765">
                                                                            <i class="icon-phone"></i>      {{$customer->phone}}
                                                                        </p>
                                                                        <p class="mb-25 col8765">
                                                                            <i class="icon-pin"></i>       {{$customer->city}}
                                                                        </p>
                                                                        <p class="mb-25 col8765">
                                                                            <i class="icon-globe"></i>      {{$customer->state}}
                                                                        </p>





                                                                    </div>
                                                                </div>





                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>







                                        </div>
                                        <!-- Row -->


                            </div>













    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Customer</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">




                        <div class="row">






                            <div class="col-sm-12 col-xs-12">



                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="users-table" class="table table-hover display  pb-30" >

                                            <thead>
                                            <tr>

                                                <th>Package Name</th>
                                                <th>IP</th>
                                                <th>Adult</th>
                                                <th>Children</th>
                                                <th>Booking Date</th>
                                                <th>Created At</th>
                                                <th>Action</th>
                                                <th>Delete</th>



                                            </tr>
                                            </thead>




                                        </table>
                                    </div>
                                </div>


                            </div>


















                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







@stop


@section('css')



@stop


@section('script')

    <link href="{{asset('admin/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css')}}">
    <script src="{{asset('admin/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/dataTables-data.js')}}"></script>



    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.customer.to.order.datatables',$customer->id) !!}',
                columns: [
                    { data: 'tour_title', name: 'tour_title' },
                    { data: 'ip', name: 'ip' },
                    { data: 'adult', name: 'adult' },
                    { data: 'children', name: 'children' },
                    { data: 'date', name: 'date' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'details', name: 'details' },
                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>

    @endsection
