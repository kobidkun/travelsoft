@extends('admin.index')


@section('content')




    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">CUSTOMER</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">Admin</a></li>
                <li><a href="#"><span>Master</span></a></li>
                <li class="active"><span>Customer</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Customer</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">






                            <div class="col-sm-12 col-xs-12">



                                <div class="table-wrap">
                                    <div class="table-responsive">
                                        <table id="users-table" class="table table-hover display  pb-30" >

                                            <thead>
                                            <tr>

                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>State</th>
                                                <th>City</th>
                                                <th>Details</th>
                                                <th>Delete</th>


                                            </tr>
                                            </thead>




                                        </table>
                                    </div>
                                </div>


                            </div>


















                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







@stop


@section('css')



@stop


@section('script')

    <link href="{{asset('admin/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css')}}">
    <script src="{{asset('admin/vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/dataTables-data.js')}}"></script>



    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.customer.datatables') !!}',
                columns: [
                    { data: 'fname', name: 'fname' },
                    { data: 'lname', name: 'lname' },
                    { data: 'email', name: 'email' },
                    { data: 'phone', name: 'phone' },
                    { data: 'state', name: 'state' },
                    { data: 'city', name: 'city' },
                    { data: 'details', name: 'details' },
                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>

    @endsection
