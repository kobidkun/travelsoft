@extends('admin.index')


@section('content')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">



            <!-- Row -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view  pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body  pa-0">
                                        <div class="profile-box">
                                            <div class="profile-cover-pic">
                                                <div class="fileupload btn btn-default">
                                                    <span class="btn-text">edit</span>

                                                    <form   method="POST"
                                                            enctype="multipart/form-data"
                                                            action="{{route('admin.city.update.bg.image',$city->id)}}"
                                                            class="bgimgform"
                                                    >
                                                        @csrf

                                                    <input class="upload bgimgforminput " name="bg_img" type="file">

                                                    </form>


                                                </div>
                                                <div
                                                    style="background-image: url('{{asset('/storage/'.$city->bg_img)}}')"
                                                    class="profile-image-overlay"></div>
                                            </div>
                                            <div class="profile-info text-center mb-15">
                                                <div class="profile-img-wrap">
                                                    <img class="inline-block mb-10"
                                                         src="{{asset('/storage/'.$city->sm_img)}}" alt="user"/>
                                                    <div class="fileupload btn btn-default">
                                                        <span class="btn-text">edit</span>


                                                        <form   method="POST"
                                                                enctype="multipart/form-data"
                                                                action="{{route('admin.city.update.sm.image',$city->id)}}"
                                                                class="smimgform"
                                                        >
                                                            @csrf

                                                            <input class="upload smimgforminput"  name="sm_img" type="file">

                                                        </form>


                                                    </div>
                                                </div>
                                                <h5 class="block mt-10 weight-500 capitalize-font txt-dark">{{$city->name}}</h5>
                                                <p style="padding: 10px" class="block">{{$city->description}}</p>
                                            </div>
                                            <div class="social-info">
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">{{$city->google_name}}</span></span>
                                                        <span class="counts-text block">Google Name</span>
                                                    </div>
                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">{{$city->lat.round(2)}}</span></span>
                                                        <span class="counts-text block">Latitude</span>
                                                    </div>
                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">{{$city->long.round(3)}}</span></span>
                                                        <span class="counts-text block">Longitude</span>
                                                    </div>
                                                </div>




                                              {{--mod 1 --}}


                                                <style>
                                                    .pac-container {
                                                        z-index: 10000 !important;
                                                    }
                                                </style>

                                                <button class="btn btn-orange btn-block  btn-anim mt-15" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i><span class="btn-text">edit profile</span></button>
                                                <div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h5 class="modal-title" id="myModalLabel">Edit Profile</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <!-- Row -->
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="">
                                                                            <div class="panel-wrapper collapse in">
                                                                                <div class="panel-body pa-0">
                                                                                    <div class="col-sm-12 col-xs-12">
                                                                                        <div class="form-wrap">




                                                                                            <form   method="POST"
                                                                                                    enctype="multipart/form-data"
                                                                                                    action="{{route('admin.city.update',$city->id)}}"
                                                                                            >
                                                                                                @csrf
                                                                                                <div class="form-body">
                                                                                                    <h6 class="txt-dark capitalize-font">
                                                                                                        <i class="zmdi zmdi-local-activity mr-10"></i>City's Info</h6>
                                                                                                    <hr class="light-grey-hr"/>
                                                                                                    <div class="row">



                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label  class="control-label mb-10">Search form google</label>
                                                                                                                <input name="google_name" type="text"  class="form-control"
                                                                                                                       id="autocomplete"    value="{{$city->google_name}}"      placeholder="City Name">
                                                                                                                <input type="hidden"  class="form-control" required
                                                                                                                       id="google_details" value="{{$city->google_details}}"  name="google_details"        placeholder="City Name">
                                                                                                                <span class="help-block"> This will help you to find latitude & longitude </span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!--/span-->
                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10">Short Info</label>
                                                                                                                <input type="text" name="description" class="form-control"
                                                                                                                       value="{{$city->description}}"   required  placeholder="Short Info">

                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!--/span-->
                                                                                                    </div>
                                                                                                    <!-- /Row -->
                                                                                                    <div class="row">

                                                                                                        <div class="col-md-6">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10">City Name</label>
                                                                                                                <input type="text" id="cityname"
                                                                                                                       name="name"
                                                                                                                       class="form-control" value="{{$city->name}}"
                                                                                                                       required  placeholder="Siliguri">

                                                                                                            </div>
                                                                                                        </div>





                                                                                                        <!--/span-->
                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10">Latitude</label>
                                                                                                                <input  id="lat" name="lat" value="{{$city->lat}}" required readonly type="text" class="form-control" placeholder="Latitude">
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="col-md-3">
                                                                                                            <div class="form-group">
                                                                                                                <label class="control-label mb-10">Longitude</label>
                                                                                                                <input name="long"  id="long" value="{{$city->long}}" required readonly  type="text" class="form-control" placeholder="Longitude">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!--/span-->
                                                                                                    </div>
                                                                                                    <!-- /Row -->



                                                                                                </div>
                                                                                                <div class="form-actions mt-10">
                                                                                                    <button type="submit" class="btn btn-success  mr-10"> Update</button>

                                                                                                </div>
                                                                                            </form>





                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">

                                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>



                                                {{--mod 1 end--}}



                                                {{--mod 1 --}}


                                                <button class="btn btn-orange btn-block  btn-anim mt-15" data-toggle="modal" data-target="#myModaltwo">
                                                    <i class="fa fa-photo"></i><span class="btn-text">Upload Gallery</span></button>
                                                <div id="myModaltwo" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabeltwo" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h5 class="modal-title" id="myModalLabeltwo">Edit Profile</h5>
                                                            </div>
                                                            <div class="modal-body">
                                                                <!-- Row -->
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <div class="">
                                                                            <div class="panel-wrapper collapse in">
                                                                                <div class="panel-body pa-0">
                                                                                    <div class="col-sm-12 col-xs-12">
                                                                                        <div class="form-wrap">



                                                                                            <form method="post" action="{{route('admin.city.add.gallery',$city->id)}}"
                                                                                                  enctype="multipart/form-data"
                                                                                                  class="dropzone" id="dropzone">
                                                                                                @csrf
                                                                                            </form>









                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">

                                                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                            </div>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>



                                                {{--mod 1 end--}}












                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>




                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div  class="panel-body pb-0">
                                        <div class="col-md-12 pb-20">




                                            <div style="height: 670px" id="map"></div>





                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



            </div>
            <!-- /Row -->


    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div  class="panel-body pb-0">
                                    <div class="col-md-12 pb-20">
                                        <div class="gallery-wrap">
                                            <div class="portfolio-wrap project-gallery">
                                                <ul id="portfolio_1" class="portf auto-construct  project-gallery" data-col="4">


                                                    @foreach($images as $image)

                                                        <li  class="item"   data-src="{{asset('/storage/'.$image->location)}}" data-sub-html="{{$city->name}}" >
                                                            <a href="">
                                                                <img class="img-responsive" src="{{asset('/storage/'.$image->location)}}"  alt="Image description" />
                                                                <span class="hover-cap">{{$city->name}}</span>
                                                            </a>
                                                        </li>

                                                        <a class="btn btn-danger" href="{{route('admin.city.delete.gallery',$image->id)}}">Delete</a>

                                                    @endforeach



                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{env('GMAPS')}}&libraries=places"></script>
            <script>
                function initialize() {
                    var input = document.getElementById('autocomplete');
                    var autocomplete = new google.maps.places.Autocomplete(input);
                    google.maps.event.addListener(autocomplete, 'place_changed', function () {
                        var place = autocomplete.getPlace();

                        document.getElementById('cityname').value = place.name;
                        document.getElementById('lat').value = place.geometry.location.lat();
                        document.getElementById('long').value = place.geometry.location.lng();
                        document.getElementById('google_details').value = JSON.stringify(place);
                    });
                }
                google.maps.event.addDomListener(window, 'load', initialize);
            </script>



@stop


@section('script')


    <script>
        $('.bgimgforminput').change(function() {
            // select the form and submit
            $('.bgimgform').submit();
        });

        $('.smimgforminput').change(function() {
            // select the form and submit
            $('.smimgform').submit();
        });
    </script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>


    <script type="text/javascript">
        Dropzone.options.dropzone =
            {
                maxFilesize: 12,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time+file.name;
                },
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                timeout: 5000,
                success: function(file, response)
                {
                    console.log(response);
                },
                error: function(file, response)
                {
                    return false;
                }
            };
    </script>


    {{-- open street maps view--}}

    <style>
        .leaflet-bottom {
            visibility: hidden;
        }
    </style>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
          integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
            integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
            crossorigin=""></script>


    <script>

        function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2-lat1);  // deg2rad below
            var dLon = deg2rad(lon2-lon1);
            var a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                Math.sin(dLon/2) * Math.sin(dLon/2)
            ;
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c; // Distance in km
            return d;
        }

        function deg2rad(deg) {
            return deg * (Math.PI/180)
        }

        var map = L.map('map').setView([{{$city->lat}}, {{$city->long}}], 12);



        var HotelIcon = L.icon({
            iconUrl: '{{asset('/admin/img/icon/hotel.png')}}',
            iconSize: [45, 45],
        });

        var PlaceIcon = L.icon({
            iconUrl: '{{asset('/admin/img/icon/placeholder.png')}}',
            iconSize: [45, 45],
        });




        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);



        @foreach($hotels as $hotel)




        L.marker([{{$hotel->lat}}, {{$hotel->long}}],{icon: HotelIcon}).addTo(map)
            .bindPopup(' <h6>{{$hotel->name}} </h6> ' +

                +' @for ($i = 0; $i < $hotel->star; $i++)' +


                '  <i style="color: yellow" class="fa fa-star"></i> @endfor'

                +'{{$hotel->star}} Star'
                +'<br>  <span style="color:#69abff;">Available Rooms</span><br>' +

                '@foreach($hotel->create_rooms as $room)  {{$room->name}} | {{$room->room_capacity}} Persons | <i class="fa fa-inr"></i> {{$room->price}}  <br> @endforeach'+


                '<br><span style="color:#69abff;">Distance From Popular Place</span><br> '+

        '@foreach($places->take(3) as $place)'+
               getDistanceFromLatLonInKm( '{{$place->lat}}' ,'{{$place->long}}' ,'{{$hotel->lat}}' ,'{{$hotel->long}}' ).toFixed(2) +' KM From {{$place->name}} <br>'+
               ' @endforeach'+
                '<br> <a href="{{route('admin.hotel.details',$hotel->id)}}" class="btn btn-primary  btn-block btn-rounded btn-icon left-icon"> <i class="fa fa-pin"></i> <span style="color:white;"> Details</span></a>'
            )
            .openPopup();


        @endforeach

        @foreach($places as $place)

        L.marker([{{$place->lat}}, {{$place->long}}],{icon: PlaceIcon}).addTo(map)
            .bindPopup(' <h6>{{$place->google_name}} </h6> ' +
                '<img style="display: block;' +
                '  margin-left: auto;' +
                '  margin-right: auto;" height="100px" src="{{asset('/storage/'.$place->sm_img)}}">'+
                '<br> <a href="{{route('admin.place.details',$place->id)}}" class="btn btn-primary  btn-block btn-rounded btn-icon left-icon"> <i class="fa fa-pin"></i> <span style="color:white;"> Details</span></a>'
            )
            .openPopup();

        @endforeach








         L.circle([{{$city->lat}}, {{$city->long}}], {
            color: '#69abff',
            fillColor: '#69abff',
            fillOpacity: 0.3,
            radius: 5000
        }).addTo(map);
    </script>



    {{-- open street maps view--}}

@stop
