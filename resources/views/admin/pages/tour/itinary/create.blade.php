@extends('admin.index')


@section('content')




    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Create Itinerary</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">Admin</a></li>
                <li><a href="#"><span>Place</span></a></li>
                <li class="active"><span>Create Itinerary</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Itinerary</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">




                                    <div style="height: 100px"></div>


                                    <div class="profile-box">

                                        <div class="profile-info text-center mb-15">
                                            <div class="profile-img-wrap">
                                                <img class="inline-block mb-10"
                                                     src="{{asset('/storage/'.$tour->img_sm)}}" alt="user"/>
                                                <div class="fileupload btn btn-default">


                                                </div>
                                            </div>

                                        </div>

                                    <div class="social-info">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <span class="counts block head-font"><span style="color: #0e88b1;font-weight: 500" class="counter-anim">{{$tour->title}}</span></span>
                                                <span class="counts-text block"> Title</span>
                                            </div>

                                            <div class="col-xs-12 text-center">
                                                <span class="counts block head-font"><span class="counter-anim">{{$linkedcity->name}}</span></span>
                                                <span class="counts-text block">City</span>
                                            </div>




                                        </div>

                                    </div>
                                    </div>



                                    <form   method="POST"
                                            enctype="multipart/form-data"
                                            action="{{route('admin.save.itinerary')}}"
                                    >
                                        @csrf
                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font">
                                                <i class="zmdi zmdi-pin mr-10"></i>Hotel's Info</h6>
                                            <hr class="light-grey-hr"/>
                                            <div class="row">



                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label  class="control-label mb-10">Title</label>
                                                        <input name="title" type="text"  class="form-control"
                                                                        placeholder="Title">



                                                        <input type="hidden"  class="form-control" required
                                                                value="{{$tour->id}}"  name="create_tour_id" >





                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Short Info</label>
                                                        <textarea rows="5" type="text" name="description" class="form-control"
                                                                  required  placeholder="Short Info"></textarea>

                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Day</label>
                                                        <input type="text"  name="day" class="form-control"
                                                             required  placeholder="Day">

                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">City</label>
                                                        <select class="form-control"  name="create_city_id" >
                                                            <option value="{{$linkedcity->id}}"   selected>{{$linkedcity->name}}</option>

                                                            @foreach($allcities as $allcity)
                                                            <option value="{{$allcity->id}}"   selected>{{$allcity->name}}</option>

                                                                @endforeach
                                                        </select>


                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Hotel</label>
                                                        <select class="form-control"  name="create_hotel_id" >


                                                            @foreach($hotels as $allcity)
                                                                <option value="{{$allcity->id}}"   selected>{{$allcity->name}}</option>

                                                            @endforeach
                                                        </select>


                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="panel-wrapper collapse in">
                                                        <div class="panel-body">

                                                            <div class="row mt-40">
                                                                <div class="col-sm-12">
                                                                    <h5 class="box-title"></h5>
                                                                    <label class="control-label mb-10">Add All Places </label>

                                                                    <select multiple class="searchable" name="placeid[]">
                                                                        @foreach($places as $place)
                                                                        <option value="{{$place->id}}">{{$place->name}} | {{$place->create_cities->name}} </option>
                                                                       @endforeach

                                                                    </select>
                                                                </div>
                                                            </div>



                                                        </div>
                                                    </div>
                                                </div>


                                                 <div class="col-sm-6">

                                                                                            <label class="control-label mb-10">Add Tour Icon </label>

                                                                                            <select multiple class="searchable" name="activities[]">
                                                                                                @foreach($allicons as $allicon)
                                                                                                    <option value="{{$allicon->id}}">{{$allicon->title}}  </option>
                                                                                                @endforeach


                                                                                            </select>
                                                                                        </div>
                                                                                        <!--/span-->








                                                <!--/span-->
                                            </div>








                                            <hr class="light-grey-hr"/>



                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Large Image</label>
                                                        <input name="bg_img" type="file"  class="form-control"
                                                            required   placeholder="Siliguri">

                                                    </div>
                                                </div>





                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Small Image</label>
                                                        <input name="sm_img"  required  type="file" class="form-control">
                                                    </div>
                                                </div>

                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->



                                        </div>
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                            <button type="button" class="btn btn-default">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>












                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">

                            </div>
                        </div>
                    </div>
                </div>
























            </div>
        </div>
    </div>






















    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Created Itinerary</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>


                <style>
                    .itinarydescimage{
                        width: 200px;
                    }
                </style>




                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                @foreach($itinaries as $itinarie)

                                    <div class="panel panel-success card-view">
                                        <div class="panel-heading">
                                            <div class="pull-left">
                                                <h6 class="panel-title txt-light">
                                                    {{$itinarie->title }} |
                                                    Day: {{$itinarie->day }}

                                                </h6>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div  class="panel-wrapper collapse in">
                                            <div  class="panel-body">



                                                <div class="col-md-12">
                                                    @foreach($itinarie->create_tour_to_itinary_to_inclusions as $inclusion)
                                                        <i style="size: 50px; padding: 15px; color: #0b97c4" class="{{$inclusion->text}}"></i>

                                                    @endforeach
                                                </div>


                                                <p>

                                                    {{$itinarie->description}}

                                                </p>


                                                <div class="col-md-3">

                                                    <a  data-fancybox="gallery" href="{{asset('/storage/'.$itinarie->img)}}"><img class="itinarydescimage"
                                                                                                      src="{{asset('/storage/'.$itinarie->img)}}"></a>




                                                </div>

                                                <a class="btn btn-danger" href="{{route('admin.itinerary.delete',$itinarie->id)}}">Delete</a>

                                            </div>
                                        </div>
                                    </div>


                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
























            </div>
        </div>
    </div>






@stop


@section('css')
    <link href="{{asset('admin/vendors/bower_components/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/vendors/bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
@stop


@section('script')

    <script src="{{asset('admin/vendors/bower_components/multiselect/js/jquery.multi-select.js')}}"></script>
    <script src="{{asset('/admin/dist/js/kqksrcmulsel.js')}}"></script>

    <script src="{{asset('admin/vendors/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>


    <script>

        $('.searchable').multiSelect({
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Type \"A Place Name\"'>",
            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Type \"A Place Name\"'>",
            afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';


                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                        if (e.which == 40){
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }
        });



    </script>





    <script>


        $(document).ready(function(){


            $(".baseprice").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".taxper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".cessper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))




















            function onPriceupdate() {
                var BASEPRICE =  $(".baseprice").val();
                var TAXPERCENTAGE =  $(".taxper option:selected").val();
                var CESSPERCENTAGE =  $(".cessper").val();
                var DISCOUNT =  0;




                //cal gst
                //
                //
                //

                var TocalculatePrice =  BASEPRICE;


                //   var  CGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2;
                //  var   SGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2
                //  var   IGST =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALTAX =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALCESS =  TocalculatePrice * (CESSPERCENTAGE/100);
                var   TOTAL = TocalculatePrice + TOTALTAX;


                //   $(".product-cgst").val(CGST.toFixed(2));
                //   $(".product-sgst").val(SGST.toFixed(2));
                //  $(".product-igst").val(IGST.toFixed(2));
                $(".taxamt").val(TOTALTAX.toFixed(2));
                $(".cessamt").val(TOTALCESS.toFixed(2));
                // $(".product-tax-amt").val(TOTALTAX.toFixed(2));
                $(".total").val((Number(TocalculatePrice) + Number(TOTALTAX)+ Number(TOTALCESS)).toFixed(2));




            }

        });
    </script>


@stop
