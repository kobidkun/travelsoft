@extends('admin.index')


@section('content')
    <link rel="stylesheet" href="{{asset('/admin/dist/css/dropzone.css')}}">



            <!-- Row -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view  pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body  pa-0">
                                        <div class="profile-box">
                                            <div class="profile-cover-pic">
                                                <div class="fileupload btn btn-default">
                                                    <span class="btn-text">edit</span>

                                                    <form   method="POST"
                                                            enctype="multipart/form-data"
                                                            action="{{route('admin.tour.update.bg.image',$city->id)}}"
                                                            class="bgimgform"
                                                    >
                                                        @csrf

                                                    <input class="upload bgimgforminput " name="bg_img" type="file">

                                                    </form>


                                                </div>
                                                <div
                                                    style="background-image: url('{{asset('/storage/'.$city->img_bg)}}')"
                                                    class="profile-image-overlay"></div>
                                            </div>
                                            <div class="profile-info text-center mb-15">
                                                <div class="profile-img-wrap">
                                                    <img class="inline-block mb-10"
                                                         src="{{asset('/storage/'.$city->img_sm)}}" alt="user"/>
                                                    <div class="fileupload btn btn-default">
                                                        <span class="btn-text">edit</span>


                                                        <form   method="POST"
                                                                enctype="multipart/form-data"
                                                                action="{{route('admin.tour.update.sm.image',$city->id)}}"
                                                                class="smimgform"
                                                        >
                                                            @csrf

                                                            <input class="upload smimgforminput"  name="sm_img" type="file">

                                                        </form>


                                                    </div>
                                                </div>
                                                <h5 class="block mt-10 weight-500 capitalize-font txt-dark">{{$city->title}}</h5>
                                                <p style="padding: 10px" class="block">{{$city->description}}</p>
                                            </div>


                                            <div class="social-info">
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">{{$city->google_name}}</span></span>
                                                        <span class="counts-text block"> Name</span>
                                                    </div>

                                                    <div class="col-xs-12 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">

                                                                {{$linkedcity->title}}</span></span>
                                                        <span class="counts-text block"></span>
                                                    </div>


                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font">
                                                            <span class="counter-anim">
                                                            <i style="color: yellow" class="fa fa-sun-o"></i>
                                                                {{$city->days}}</span></span>
                                                        <span class="counts-text block">Days</span>

                                                    </div>
                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">
                                                                 <i style="color: grey" class="fa fa-moon-o"></i>
                                                                {{$city->night}}</span></span>
                                                        <span class="counts-text block">Night</span>
                                                    </div>
                                                </div>







                                            </div>














                                            <div class="social-info">
                                                <div class="row">
                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">
                                                                <i class="fa fa-inr"></i>
                                                                {{$city->price}}</span></span>
                                                        <span class="counts-text block"> Price</span>
                                                    </div>

                                                    <div class="col-xs-6 text-center">
                                                        <span class="counts block head-font"><span class="counter-anim">


                                                                {{$linkedcity->name}}

                                                            </span></span>
                                                        <span class="counts-text block">Primary City</span>
                                                    </div>





                                                </div>



                                                <button class="btn btn-orange btn-block  btn-anim mt-15" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i><span class="btn-text">edit profile</span></button>

                                                <button class="btn btn-orange btn-block  btn-anim mt-15" data-toggle="modal" data-target="#myModaltwo">
                                                    <i class="fa fa-photo"></i><span class="btn-text">Upload Gallery</span></button>


                                                {{--mod 1 --}}


                                                <style>
                                                    .pac-container {
                                                        z-index: 10000 !important;
                                                    }
                                                </style>





                                            </div>







                                            {{--modal--}}

                                            <div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h5 class="modal-title" id="myModalLabel">Edit Tour</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- Row -->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="">
                                                                        <div class="panel-wrapper collapse in">
                                                                            <div class="panel-body pa-0">
                                                                                <div class="col-sm-12 col-xs-12">
                                                                                    <div class="form-wrap">




                                                                                        <form   method="POST"
                                                                                                enctype="multipart/form-data"
                                                                                                action="{{route('admin.tour.update',$city->id)}}"
                                                                                        >
                                                                                            @csrf

                                                                                            <div class="form-body">
                                                                                                <h6 class="txt-dark capitalize-font">
                                                                                                    <i class="zmdi zmdi-pin mr-10"></i>Tour's Info</h6>
                                                                                                <hr class="light-grey-hr"/>
                                                                                                <div class="row">



                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label  class="control-label mb-10">Title
                                                                                                            </label>
                                                                                                            <input name="title" type="text" value="{{$city->title}}"  class="form-control"
                                                                                                                   placeholder="Title">




                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <!--/span-->
                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10">Short Info</label>
                                                                                                            <input type="text" value="{{$city->description}}"  name="description" class="form-control"
                                                                                                                   required  placeholder="Short Info">

                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10">Select City</label>





                                                                                                                <select class="form-control select2"  name="create_city_id">

                                                                                                                    <option value="{{$linkedcity->id}}">{{$linkedcity->name}}</option>

                                                                                                                    @foreach($allcity as $allcit)

                                                                                                                        <option value="{{$allcit->id}}">{{$allcit->name}}</option>

                                                                                                                    @endforeach



                                                                                                                </select>









                                                                                                        </div>
                                                                                                    </div>





                                                                                                    <div class="col-md-3">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10">Days</label>
                                                                                                            <input name="days" value="{{$city->days}}"  class="form-control" type="number">





                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col-md-3">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10">Night</label>
                                                                                                            <input name="night" value="{{$city->night}}"  class="form-control" type="number">





                                                                                                        </div>
                                                                                                    </div>








                                                                                                    <!--/span-->
                                                                                                </div>
                                                                                                <!-- /Row -->

                                                                                                <!-- /Row -->

                                                                                                <hr class="light-grey-hr"/>


                                                                                                <div class="row">

                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Base  Price</label>
                                                                                                            <input name="price" required   type="text"
                                                                                                                   class="form-control baseprice" value="{{$city->price}}"  placeholder="Price">
                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Tax %</label>
                                                                                                            <select class="form-control taxper" name="tax_per" id="">
                                                                                                                <option value="{{$city->tax_per}}" >{{$city->tax_per}}</option>
                                                                                                                <option value="0">0</option>
                                                                                                                <option value="5">5</option>
                                                                                                                <option value="12">12</option>
                                                                                                                <option value="18">18</option>
                                                                                                                <option value="28">28</option>
                                                                                                            </select>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Tax AMT</label>
                                                                                                            <input name="tax"   required readonly  type="text"
                                                                                                                   class="form-control taxamt" value="{{$city->tax}}"  placeholder="Tax AMT">
                                                                                                        </div>
                                                                                                    </div>


                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Cess %</label>
                                                                                                            <input name="cess_per" value="{{$city->cess_per}}"   required   type="text"
                                                                                                                   class="form-control cessper" placeholder="Cess %">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Cess</label>
                                                                                                            <input name="cess"   required readonly  type="text"
                                                                                                                   class="form-control cessamt" value="{{$city->cess}}"  placeholder="Cess ">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="col-md-6">
                                                                                                        <div class="form-group">
                                                                                                            <label class="control-label mb-10 ">Total</label>
                                                                                                            <input name="total"   required readonly  type="text"
                                                                                                                   class="form-control total" value="{{$city->total}}"  placeholder="Total">
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>






                                                                                            </div>










                                                                                            <div class="form-actions mt-10">
                                                                                                <button type="submit" class="btn btn-success  mr-10"> Update</button>

                                                                                            </div>
                                                                                        </form>





                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">

                                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>



                                            {{--mod 1 end--}}



                                            {{--mod 1 --}}


                                            <div id="myModaltwo" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabeltwo" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                            <h5 class="modal-title" id="myModalLabeltwo">Edit Profile</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                            <!-- Row -->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="">
                                                                        <div class="panel-wrapper collapse in">
                                                                            <div class="panel-body pa-0">
                                                                                <div class="col-sm-12 col-xs-12">
                                                                                    <div class="form-wrap">



                                                                                        <form method="post" action="{{route('admin.tour.add.gallery',$city->id)}}"
                                                                                              enctype="multipart/form-data"
                                                                                              class="dropzone" id="dropzone">
                                                                                            @csrf
                                                                                        </form>









                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">

                                                            <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>



                                            {{--mod 1 end--}}

                                            {{--modal--}}





















                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>




                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default card-view pa-0">
                                <div class="panel-wrapper collapse in">
                                    <div  class="panel-body pb-0">
                                        <div class="col-md-12 pb-20">




                                            <div style="height: 670px" id="map"></div>





                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



            </div>
            <!-- /Row -->



    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Created Itinerary</h6>
                    </div>

                    <style>

                        .right-block-content{
                            position: absolute;
                            left: 85%;
                            top: 0;

                        }
                    </style>

                    <div align="center" class="right-block-content">
                        <a href="{{route('admin.create.itinerary',$city->id)}}"  class="btn btn-info btn-rounded btn-block btn-anim"><i class="fa fa-file-image-o"></i><span class="btn-text">Create Itinerary</span></a>
                    </div>
                    <div class="clearfix"></div>
                </div>


                <style>
                    .itinarydescimage{
                        width: 200px;
                    }
                </style>




                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                @foreach($itineraries as $itinarie)

                                    <div class="panel panel-success card-view">
                                        <div class="panel-heading">
                                            <div class="pull-left">
                                                <h6 class="panel-title txt-light">
                                                    {{$itinarie->title }} |
                                                    Day: {{$itinarie->day }}

                                                </h6>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div  class="panel-wrapper collapse in">
                                            <div  class="panel-body">



                                                <div class="col-md-12">
                                                    @foreach($itinarie->create_tour_to_itinary_to_inclusions as $inclusion)
                                                        <i style="size: 50px; padding: 15px; color: #0b97c4" class="{{$inclusion->text}}"></i>

                                                    @endforeach
                                                </div>


                                                <p>

                                                    {{$itinarie->description}}

                                                </p>


                                                <div class="col-md-3">

                                                    <a  data-fancybox="gallery" href="{{asset('/storage/'.$itinarie->img)}}"><img class="itinarydescimage"
                                                                                                                                  src="{{asset('/storage/'.$itinarie->img)}}"></a>




                                                </div>

                                                <a class="btn btn-danger" href="{{route('admin.itinerary.delete',$itinarie->id)}}">Delete</a>

                                            </div>
                                        </div>
                                    </div>


                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
























            </div>
        </div>
    </div>











































    <div class="row">
        <div class="col-sm-12">




            <div class="row">
            {{--tour tags--}}

            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div  class="panel-body pb-0">
                                    <div class="col-md-12 pb-20" >



                                        <form   method="POST"
                                                enctype="multipart/form-data"
                                                action="{{route('admin.save.tour.tag',$city->id)}}"
                                        >
                                            @csrf

                                            <div class="form-body">
                                                <h6 class="txt-dark capitalize-font">
                                                    <i class="zmdi zmdi-pin mr-10"></i>Tour Tag</h6>
                                                <hr class="light-grey-hr"/>
                                                <div class="row">



                                                    <div class="col-sm-12">
                                                        <h5 class="box-title"></h5>
                                                        <label class="control-label mb-10">Add Tour Tag </label>

                                                        <select multiple class="searchable" name="tagid[]">
                                                            @foreach($alltags as $place)
                                                                <option value="{{$place->id}}">{{$place->tag}}  </option>
                                                            @endforeach





                                                        </select>
                                                    </div>
                                                    <!--/span-->



                                                    <!--/span-->
                                                </div>
                                                <!-- /Row -->







                                            </div>










                                            <div class="form-actions mt-10">
                                                <button type="submit" class="btn btn-success  mr-10"> Create</button>

                                            </div>
                                        </form>

                                        <div style="height: 50px"></div>

                                        <div class="col-md-12">
                                            @foreach($tourtags as $touricon)

                                                {{$touricon->tag}}   <a href="{{route('admin.delete.tour.tag',$touricon->id)}}" style="color: red"><i class="icon-trash"></i></a>
                                                <br>

                                            @endforeach
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

{{--icon--}}
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div  class="panel-body pb-0">
                                    <div class="col-md-12 pb-20" >



                                        <form   method="POST"
                                                enctype="multipart/form-data"
                                                action="{{route('admin.save.tour.icon',$city->id)}}"
                                        >
                                            @csrf

                                            <div class="form-body">
                                                <h6 class="txt-dark capitalize-font">
                                                    <i class="zmdi zmdi-pin mr-10"></i>Tour Icon</h6>
                                                <hr class="light-grey-hr"/>
                                                <div class="row">



                                                    <div class="col-sm-12">
                                                        <h5 class="box-title"></h5>
                                                        <label class="control-label mb-10">Add Tour Icon </label>

                                                        <select multiple class="searchable" name="iconid[]">
                                                            @foreach($allicons as $place)
                                                                <option value="{{$place->id}}">{{$place->title}}  </option>
                                                            @endforeach


                                                        </select>
                                                    </div>
                                                    <!--/span-->




                                                    <!--/span-->
                                                </div>
                                                <!-- /Row -->







                                            </div>










                                            <div class="form-actions mt-10">
                                                <button type="submit" class="btn btn-success  mr-10"> Create</button>

                                            </div>
                                        </form>

                                        <div style="height: 50px"></div>


                                        <div class="col-md-12">
                                            @foreach($touricons as $touricon)

                                               <p>
                                                   <i style="size: 50px!important;" class="{{$touricon->icon}}"></i>
                                                   <a href="{{route('admin.delete.tour.icon',$touricon->id)}}" style="color: red"><i class="icon-trash"></i></a>

                                               </p>

                                            @endforeach
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>



            </div>



            <div class="row">

                 {{--tour theme--}}

            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div  class="panel-body pb-0">
                                    <div class="col-md-12 pb-20" >



                                        <form   method="POST"
                                                enctype="multipart/form-data"
                                                action="{{route('admin.save.tour.theme',$city->id)}}"
                                        >
                                            @csrf

                                            <div class="form-body">
                                                <h6 class="txt-dark capitalize-font">
                                                    <i class="zmdi zmdi-pin mr-10"></i>Tour Theme</h6>
                                                <hr class="light-grey-hr"/>
                                                <div class="row">



                                                    <div class="col-sm-12">
                                                        <h5 class="box-title"></h5>
                                                        <label class="control-label mb-10">Add Tour Theme </label>

                                                        <select multiple class="searchable" name="themeid[]">
                                                            @foreach($allthemes as $alltheme)
                                                                <option value="{{$alltheme->id}}">{{$alltheme->theme}}  </option>
                                                            @endforeach


                                                        </select>
                                                    </div>
                                                    <!--/span-->




                                                    <!--/span-->
                                                </div>
                                                <!-- /Row -->







                                            </div>










                                            <div class="form-actions mt-10">
                                                <button type="submit" class="btn btn-success  mr-10"> Create</button>

                                            </div>
                                        </form>

                                        <div style="height: 50px"></div>


                                        <div class="col-md-12">
                                            @foreach($tourtothemes as $tourtotheme)

                                               <p>
                                                   {{$tourtotheme->theme}}
                                                   <a href="{{route('admin.delete.tour.theme',$tourtotheme->id)}}" style="color: red"><i class="icon-trash"></i></a>

                                               </p>

                                            @endforeach
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


         {{--cities--}}


            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div  class="panel-body pb-0">
                                    <div class="col-md-12 pb-20" >



                                        <form   method="POST"
                                                enctype="multipart/form-data"
                                                action="{{route('admin.save.tour.to.city',$city->id)}}"
                                        >
                                            @csrf

                                            <div class="form-body">
                                                <h6 class="txt-dark capitalize-font">
                                                    <i class="zmdi zmdi-pin mr-10"></i>Add City</h6>
                                                <hr class="light-grey-hr"/>
                                                <div class="row">



                                                    <div class="col-sm-12">
                                                        <h5 class="box-title"></h5>
                                                        <label class="control-label mb-10">Add City </label>

                                                        <select multiple class="searchable" name="cityid[]">
                                                            @foreach($allcity as $cityadd)
                                                                <option value="{{$cityadd->id}}">{{$cityadd->name}}  </option>
                                                            @endforeach


                                                        </select>
                                                    </div>
                                                    <!--/span-->




                                                    <!--/span-->
                                                </div>
                                                <!-- /Row -->







                                            </div>










                                            <div class="form-actions mt-10">
                                                <button type="submit" class="btn btn-success  mr-10"> Create</button>

                                            </div>
                                        </form>

                                        <div style="height: 50px"></div>


                                        <div class="col-md-12">
                                            @foreach($tourtocities as $tourtocityy)

                                               <p>{{$tourtocityy->name}}
                                                   <a href="{{route('admin.delete.tour.tour.to.city',$tourtocityy->id)}}" style="color: red"><i class="icon-trash"></i></a>
                                               </p>

                                            @endforeach
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


               </div>




            <div class="row">


            {{--incluson--}}




            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div  class="panel-body pb-0">
                                    <div class="col-md-12 pb-20">



                                        <form   method="POST"
                                                enctype="multipart/form-data"
                                                action="{{route('admin.save.tour.inclusion',$city->id)}}"
                                        >
                                            @csrf

                                            <div class="form-body">
                                                <h6 class="txt-dark capitalize-font">
                                                    <i class="zmdi zmdi-pin mr-10"></i>Tour Inclusion</h6>
                                                <hr class="light-grey-hr"/>
                                                <div class="row">



                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label  class="control-label mb-10">Tour Inclusion.
                                                               </label>
                                                            </label>
                                                            <input name="text" value="" type="text"  class="form-control"
                                                                   placeholder="Tour Exclusion">

                                                            <input name="create_tour_id" value="{{$city->id}}" type="hidden"  class="form-control"
                                                                   placeholder="Tour Exclusion">



                                                        </div>
                                                    </div>
                                                    <!--/span-->



                                                    <!--/span-->
                                                </div>
                                                <!-- /Row -->







                                            </div>










                                            <div class="form-actions mt-10">
                                                <button type="submit" class="btn btn-success  mr-10"> Create</button>

                                            </div>
                                        </form>

                                        <div style="height: 50px"></div>

                                        @foreach($tourinclusions as $itinarie)

                                            <div class="panel panel-info card-view">
                                                <div class="panel-heading">
                                                    <div class="pull-left">
                                                        <h6 class="panel-title txt-light">
                                                            {{$itinarie->text }}


                                                        </h6>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <a class="btn btn-light" href="{{route('admin.delete.tour.inclusion',$itinarie->id)}}">Delete</a>
                                                </div>

                                            </div>


                                        @endforeach


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>



            {{--exclusions--}}



            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div  class="panel-body pb-0">
                                    <div class="col-md-12 pb-20" >



                                        <form   method="POST"
                                                enctype="multipart/form-data"
                                                action="{{route('admin.save.tour.exclusion')}}"
                                        >
                                            @csrf

                                            <div class="form-body">
                                                <h6 class="txt-dark capitalize-font">
                                                    <i class="zmdi zmdi-pin mr-10"></i>Tour Exclusion</h6>
                                                <hr class="light-grey-hr"/>
                                                <div class="row">



                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label  class="control-label mb-10">Tour Exclusion.
                                                               </label>
                                                            <input name="text" value="" type="text"  class="form-control"
                                                                        placeholder="Tour Exclusion">

                                                            <input name="create_tour_id" value="{{$city->id}}" type="hidden"  class="form-control"
                                                                           placeholder="Tour Exclusion">



                                                        </div>
                                                    </div>
                                                    <!--/span-->



                                                    <!--/span-->
                                                </div>
                                                <!-- /Row -->







                                            </div>










                                            <div class="form-actions mt-10">
                                                <button type="submit" class="btn btn-success  mr-10"> Create</button>

                                            </div>
                                        </form>

                                        <div style="height: 50px"></div>

                                        @foreach($tourexclusions as $itinarie)

                                            <div class="panel panel-danger card-view">
                                                <div class="panel-heading">
                                                    <div class="pull-left">
                                                        <h6 class="panel-title txt-light">
                                                            {{$itinarie->text }}


                                                        </h6>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <a class="btn btn-light" href="{{route('admin.delete.tour.exclusion',$itinarie->id)}}">Delete</a>

                                                </div>

                                            </div>


                                        @endforeach


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            </div>









{{--maps--}}



            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div  class="panel-body pb-0">
                                    <div class="col-md-12 pb-20">


                                        <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0"
                                                marginwidth="0" src="https://maps.google.com/maps?q={{urlencode($linkedcity->name)}}&amp;output=embed"></iframe>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

{{--gallery--}}

            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default card-view pa-0">
                            <div class="panel-wrapper collapse in">
                                <div  class="panel-body pb-0">
                                    <div class="col-md-12 pb-20">
                                        <div class="gallery-wrap">
                                            <div class="portfolio-wrap project-gallery">
                                                <ul id="portfolio_1" class="portf auto-construct  project-gallery" data-col="4">


                                                    @foreach($images as $image)

                                                       <div class="col-md-6">
                                                           <li  class="item"   data-src="{{asset('/storage/'.$image->location)}}" data-sub-html="{{$city->name}}" >
                                                               <a href="">


                                                                   <a  data-fancybox="gallery" href="{{asset('/storage/'.$image->location)}}"><img class="img-responsive" src="{{asset('/storage/'.$image->location)}}"  alt="Image description" /></a>
                                                                   <span class="hover-cap">{{$city->name}}</span>
                                                               </a>
                                                           </li>

                                                           <a class="btn btn-danger" href="{{route('admin.tour.delete.gallery',$image->id)}}">Delete</a>

                                                       </div>
                                                        
                                                    @endforeach



                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>









        </div>
    </div>





































@stop

@section('css')
    <link href="{{asset('admin/vendors/bower_components/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('admin/vendors/bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
@stop


@section('script')



    <script>
        $('.bgimgforminput').change(function() {
            // select the form and submit
            $('.bgimgform').submit();
        });

        $('.smimgforminput').change(function() {
            // select the form and submit
            $('.smimgform').submit();
        });
    </script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>


    <script type="text/javascript">
        Dropzone.options.dropzone =
            {
                maxFilesize: 12,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time+file.name;
                },
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                timeout: 5000,
                success: function(file, response)
                {
                    console.log(response);
                },
                error: function(file, response)
                {
                    return false;
                }
            };
    </script>


    {{-- open street maps view--}}

    <style>
        .leaflet-bottom {
            visibility: hidden;
        }
    </style>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
          integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
            integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
            crossorigin=""></script>


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>

    <script src="{{asset('admin/vendors/bower_components/multiselect/js/jquery.multi-select.js')}}"></script>
    <script src="{{asset('/admin/dist/js/kqksrcmulsel.js')}}"></script>

    <script>

        $('.searchable').multiSelect({
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Type \"A Place Name\"'>",
            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Type \"A Place Name\"'>",
            afterInit: function(ms){
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';


                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                        if (e.which == 40){
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function(){
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function(){
                this.qs1.cache();
                this.qs2.cache();
            }
        });



    </script>


    <script>




        $(document).ready(function(){


            $(".baseprice").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".taxper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".cessper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))




















            function onPriceupdate() {
                var BASEPRICE =  $(".baseprice").val();
                var TAXPERCENTAGE =  $(".taxper option:selected").val();
                var CESSPERCENTAGE =  $(".cessper").val();
                var DISCOUNT =  0;




                //cal gst
                //
                //
                //

                var TocalculatePrice =  BASEPRICE;


                //   var  CGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2;
                //  var   SGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2
                //  var   IGST =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALTAX =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALCESS =  TocalculatePrice * (CESSPERCENTAGE/100);
                var   TOTAL = TocalculatePrice + TOTALTAX;


                //   $(".product-cgst").val(CGST.toFixed(2));
                //   $(".product-sgst").val(SGST.toFixed(2));
                //  $(".product-igst").val(IGST.toFixed(2));
                $(".taxamt").val(TOTALTAX.toFixed(2));
                $(".cessamt").val(TOTALCESS.toFixed(2));
                // $(".product-tax-amt").val(TOTALTAX.toFixed(2));
                $(".total").val((Number(TocalculatePrice) + Number(TOTALTAX)+ Number(TOTALCESS)).toFixed(2));




            }

        });
    </script>


    <script>

        function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
            var R = 6371; // Radius of the earth in km
            var dLat = deg2rad(lat2-lat1);  // deg2rad below
            var dLon = deg2rad(lon2-lon1);
            var a =
                Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                Math.sin(dLon/2) * Math.sin(dLon/2)
            ;
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c; // Distance in km
            return d;
        }

        function deg2rad(deg) {
            return deg * (Math.PI/180)
        }







       var map = L.map('map').setView([{{$linkedcity->lat}}, {{$linkedcity->long}}], 10);


        var HotelIcon = L.icon({
            iconUrl: '{{asset('/admin/img/icon/hotel.png')}}',
            iconSize: [45, 45],
        });

        var PlaceIcon = L.icon({
            iconUrl: '{{asset('/admin/img/icon/placeholder.png')}}',
            iconSize: [45, 45],
        });


        //places


        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);


        @foreach($places as $place)

        L.marker([{{$place->create_places->lat}}, {{$place->create_places->long}}],{icon: PlaceIcon}).addTo(map)
            .bindPopup(' <h6>{{$place->create_places->name}} </h6> '
                +'<br> {{$place->create_places->description}}'
               )
            .openPopup();

        @endforeach


        @foreach($hotels as $hotel)

        L.marker([{{$hotel->lat}}, {{$hotel->long}}],{icon: HotelIcon}).addTo(map)
            .bindPopup(' <h6>{{$hotel->name}} </h6> '
               )
            .openPopup();

        @endforeach






    </script>



    {{-- open street maps view--}}

@stop
