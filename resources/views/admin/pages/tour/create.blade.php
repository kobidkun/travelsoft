@extends('admin.index')


@section('content')




    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Create Tour Package</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}">Admin</a></li>
                <li><a href="#"><span>Tour</span></a></li>
                <li class="active"><span>Tour Package</span></li>
            </ol>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-panel card-view">
                <div class="panel-heading">
                    <div class="pull-left">
                        <h6 class="panel-title txt-dark">Tour</h6>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">
                                    <form   method="POST"
                                            enctype="multipart/form-data"
                                            action="{{route('admin.save.tour')}}"
                                    >
                                        @csrf
                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font">
                                                <i class="zmdi zmdi-pin mr-10"></i>Tour's Info</h6>
                                            <hr class="light-grey-hr"/>
                                            <div class="row">



                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label  class="control-label mb-10">Title
                                                             </label>
                                                        <input name="title" type="text"  class="form-control"
                                                                        placeholder="Title">




                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Short Info</label>
                                                        <input type="text" name="description" class="form-control"
                                                             required  placeholder="Short Info">

                                                    </div>
                                                </div>

                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Select City</label>
                                                        <select class="form-control select2"  name="create_city_id">

                                                            @foreach($cities as $city)

                                                                <option value="{{$city->id}}">{{$city->name}}</option>

                                                                @endforeach



                                                        </select>





                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Days</label>
                                                        <input name="days" class="form-control" type="number">





                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Night</label>
                                                        <input name="night" class="form-control" type="number">





                                                    </div>
                                                </div>







                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->

                                            <hr class="light-grey-hr"/>


                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Base  Price</label>
                                                        <input name="price" required   type="text"
                                                               class="form-control baseprice" placeholder="Price">
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Tax %</label>
                                                        <select class="form-control taxper" name="tax_per" id="">
                                                            <option value="0">0</option>
                                                            <option value="5">5</option>
                                                            <option value="12">12</option>
                                                            <option value="18">18</option>
                                                            <option value="28">28</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Tax AMT</label>
                                                        <input name="tax"   required readonly  type="text"
                                                               class="form-control taxamt" placeholder="Tax AMT">
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Cess %</label>
                                                        <input name="cess_per"   required   type="text"
                                                               class="form-control cessper" placeholder="Cess %">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Cess</label>
                                                        <input name="cess"   required readonly  type="text"
                                                               class="form-control cessamt" placeholder="Cess ">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10 ">Total</label>
                                                        <input name="total"   required readonly  type="text"
                                                               class="form-control total" placeholder="Total">
                                                    </div>
                                                </div>

                                            </div>



                                            <hr class="light-grey-hr"/>



                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Large Image</label>
                                                        <input name="bg_img" type="file"  class="form-control"
                                                            required   placeholder="Siliguri">

                                                    </div>
                                                </div>





                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10">Small Image</label>
                                                        <input name="sm_img"  required  type="file" class="form-control">
                                                    </div>
                                                </div>

                                                <!--/span-->
                                            </div>
                                            <!-- /Row -->



                                        </div>
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                            <button type="button" class="btn btn-default">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







@stop


@section('css')
    <link href="{{asset('admin/vendors/bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
@stop


@section('script')


    <script src="{{asset('admin/vendors/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>


    <script type="text/javascript">
        Dropzone.options.dropzone =
            {
                maxFilesize: 12,
                renameFile: function(file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time+file.name;
                },
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                timeout: 5000,
                success: function(file, response)
                {
                    console.log(response);
                },
                error: function(file, response)
                {
                    return false;
                }
            };
    </script>


    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?type=restaurant&key={{env('GMAPS')}}&libraries=places"></script>
    <script>
        function initialize() {

            var options = {
               // types: ['(lodging)'],
            };

            var input = document.getElementById('autocomplete');
            var autocomplete = new google.maps.places.Autocomplete(input, options);

            google.maps.event.addListener(autocomplete, 'place_changed', function () {

                var place = autocomplete.getPlace();

                document.getElementById('cityname').value = place.vicinity;
                document.getElementById('name').value = place.name;
                document.getElementById('lat').value = place.geometry.location.lat();
                document.getElementById('long').value = place.geometry.location.lng();
                document.getElementById('google_details').value = JSON.stringify(place);
                document.getElementById('g_star').value = place.rating;
                document.getElementById('g_place_id').value = place.place_id;
                document.getElementById('g_url').value = place.url;
                document.getElementById('phone').value = place.formatted_phone_number;
                document.getElementById('address').value = place.formatted_address;
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>


    <script>


        $(document).ready(function(){


            $(".baseprice").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".taxper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))

            $(".cessper").on("change paste keyup keydown keypress",(function () {
                //  var LISTINGPRICE = ;
                onPriceupdate();
            }))




















            function onPriceupdate() {
                var BASEPRICE =  $(".baseprice").val();
                var TAXPERCENTAGE =  $(".taxper option:selected").val();
                var CESSPERCENTAGE =  $(".cessper").val();
                var DISCOUNT =  0;




                //cal gst
                //
                //
                //

                var TocalculatePrice =  BASEPRICE;


                //   var  CGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2;
                //  var   SGST = (TocalculatePrice * (TAXPERCENTAGE/100))/2
                //  var   IGST =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALTAX =  TocalculatePrice * (TAXPERCENTAGE/100);
                var   TOTALCESS =  TocalculatePrice * (CESSPERCENTAGE/100);
                var   TOTAL = TocalculatePrice + TOTALTAX;


                //   $(".product-cgst").val(CGST.toFixed(2));
                //   $(".product-sgst").val(SGST.toFixed(2));
                //  $(".product-igst").val(IGST.toFixed(2));
                $(".taxamt").val(TOTALTAX.toFixed(2));
                $(".cessamt").val(TOTALCESS.toFixed(2));
                // $(".product-tax-amt").val(TOTALTAX.toFixed(2));
                $(".total").val((Number(TocalculatePrice) + Number(TOTALTAX)+ Number(TOTALCESS)).toFixed(2));




            }

        });
    </script>


@stop
