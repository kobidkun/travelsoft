<!-- Left Sidebar Menu -->
<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <span>System</span>
            <hr/>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#customer">
                <div class="pull-left"><i class="ti-user mr-20"></i><span class="right-nav-text">
                        Customers
                    </span>
                </div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="customer" class="collapse collapse-level-1">
                <li>
                    <a href="{{route('admin.all.customer')}}">All Customers</a>
                </li>
                <li>
                    <a href="{{route('admin.create.customer')}}"><div class="pull-left"><span>
                                Create Customer
                            </span></div><div class="pull-right"><span class="label label-success">Hot</span></div><div class="clearfix"></div></a>
                </li>

            </ul>
        </li>

        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#order">
                <div class="pull-left"><i class="ti-shopping-cart mr-20"></i><span class="right-nav-text">
                        Orders
                    </span>
                </div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="order" class="collapse collapse-level-1">
                <li>
                    <a href="{{route('admin.all.order')}}">Orders</a>
                </li>


            </ul>
        </li>

        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#city"><div class="pull-left">
                    <i class="ti-location-arrow mr-20"></i><span class="right-nav-text">
                     City
                    </span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="city" class="collapse collapse-level-1">
                <li>
                    <a href="{{route('admin.create.city')}}">Create City</a>
                </li>
                <li>
                    <a href="{{route('admin.city.index')}}">All City</a>
                </li>

            </ul>
        </li>

        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#app_dr"><div class="pull-left">
                    <i class="ti-location-pin mr-20"></i><span class="right-nav-text">
                     Place
                    </span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="app_dr" class="collapse collapse-level-1">
                <li>
                    <a href="{{route('admin.create.place')}}">Create Place</a>
                </li>
                <li>
                    <a href="{{route('admin.place.index')}}">All Place</a>
                </li>

            </ul>
        </li>


        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#car"><div class="pull-left">
                    <i class="ti-car mr-20"></i><span class="right-nav-text">
                     Car
                    </span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="car" class="collapse collapse-level-1">
                <li>
                    <a href="{{route('admin.create.cab')}}">Create Car</a>
                </li>
                <li>
                    <a href="{{route('admin.cab.index')}}">All Car</a>
                </li>

            </ul>
        </li>


        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#hotel"><div class="pull-left">
                    <i class="ti-home mr-20"></i><span class="right-nav-text">
                     Hotel
                    </span></div><div class="pull-right"><i class="ti-angle-down"></i></div><div class="clearfix"></div></a>
            <ul id="hotel" class="collapse collapse-level-1">
                <li>
                    <a href="{{route('admin.create.hotel')}}">Create Hotel</a>
                </li>
                <li>
                    <a href="{{route('admin.hotel.index')}}">All Hotel</a>
                </li>


            </ul>
        </li>


        <li class="navigation-header mt-20">
            <span>Tour Management</span>
            <hr/>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr">
                <div class="pull-left"><i class="ti-pencil-alt  mr-20"></i><span class="right-nav-text">
                         Tour
                    </span></div><div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
            <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">
                <li>
                    <a href="{{route('admin.create.tour')}}">Create Tour</a>
                </li>
                <li>
                    <a href="{{route('admin.tour.index')}}">All Tours</a>
                </li>



            </ul>
        </li>





        <li class="navigation-header mt-20">
            <span>Tour Master</span>
            <hr/>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_master">
                <div class="pull-left"><i class="ti-pencil-alt  mr-20"></i><span class="right-nav-text">
                         Tour Master
                    </span></div><div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
            <ul id="ui_master" class="collapse collapse-level-1 two-col-list">
                <li>
                    <a href="{{route('admin.create.master.icon')}}">Create Icon</a>
                </li>
                <li>
                    <a href="{{route('admin.create.master.theme')}}">All Theme</a>
                </li>

                <li>
                    <a href="{{route('admin.create.master.tag')}}">All Tag</a>
                </li>


 <li>
                    <a href="{{route('admin.slider.master.create')}}">Sliders</a>
                </li>



            </ul>
        </li>



        <li class="navigation-header mt-20">
            <span>Admin</span>
            <hr/>
        </li>
        <li>
            <a class="active" href="javascript:void(0);" data-toggle="collapse" data-target="#pages_dr"><div class="pull-left"><i class="ti-shield mr-20"></i><span class="right-nav-text">Pages</span></div><div class="pull-right"><i class="ti-angle-down "></i></div><div class="clearfix"></div></a>
            <ul id="pages_dr" class="collapse collapse-level-1">
                <li>
                    <a class="active-page" href="blank.html">Create Sales Executive</a>
                </li>

                <li>
                    <a class="active-page" href="blank.html">All Sales Executive</a>
                </li>


            </ul>
        </li>

    </ul>
</div>
<!-- /Left Sidebar Menu -->
