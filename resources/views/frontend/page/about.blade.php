@extends('frontend.index')


@section('content')

    <section class="parallax-window" data-parallax="scroll" data-image-src="{{'/images/bg/bg.jpg'}}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>About</h1>
                <p></p>
            </div>
        </div>
    </section>


    <main>
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a>
                    </li>
                    <li><a href="#">About</a>
                    </li>

                </ul>
            </div>
        </div>
        <!-- End Position -->

        <div class="container margin_60">

            <div class="main_title">
                <h2>A   <span>FEW WORDS </span>ABOUT US</h2>
                <p>




                </p>
            </div>

            <div class="row">
                <div class="col-lg-6 wow fadeIn" data-wow-delay="0.1s">
                    <div class="feature">
                        <i class="icon_set_1_icon-30"></i>
                        <h3><span> SAFETY </span> ASSURED</h3>
                        <p>
                            Your safety is the first thing that we have taken into consideration while designing our packages and we will have our trained experts guiding you, throughout the itineraries.



                        </p>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeIn" data-wow-delay="0.2s">
                    <div class="feature">
                        <i class="icon_set_1_icon-41"></i>
                        <h3><span>+120</span> SPLENDID ITINERARIES
                        </h3>
                        <p>
                            Itineraries designed to keep you amazed every single moment of your precious vacation and will meet the expectations of each so you can take back home some memories worth cherishing

                        </p>
                    </div>
                </div>
            </div>
            <!-- End row -->
            <div class="row">
                <div class="col-lg-6 wow fadeIn" data-wow-delay="0.3s">
                    <div class="feature">
                        <i class="icon_set_1_icon-57"></i>
                        <h3><span>H24</span> Support</h3>
                        <p>
                            We have a dedicated team in our customer care sector that, with their pleasing personality and ever enthusiastic attitude will silently; steal your hearts by erasing all of your doubts and queries

                        </p>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeIn" data-wow-delay="0.4s">
                    <div class="feature">
                        <i class="icon_set_1_icon-61"></i>
                        <h3><span>COST EFFECTIVE </span> ACCOMMODATION</h3>
                        <p>
                            We offer you the luxury of choice from the ocean of best hotels. We have every kind of room with world class amenities and modern facilities and all at reasonable rates.

                        </p>
                    </div>
                </div>
            </div>
            <!-- End row -->


        </div>
        <!-- End container -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 nopadding features-intro-img">
                    <div class="features-bg">
                        <div class="features-img"></div>
                    </div>
                </div>
                <div class="col-lg-6 nopadding">
                    <div class="features-content">
                        <h3>"About Us"</h3>
                        <p>
                            Our trip for Travel Next has its finest detail around your taste and interests, and we listen to what you want and then we try to simply design an individual trip to match that is on the budget of the visitors. With the best commitment with that of the quality we have experts for each and every one of our destinations. We provide the accommodation with the proper budget estimations for the travellers with a holiday package, hotel booking, car rental and flight booking, with a design trip around you. We will also introduce you with the new places highlighting in an different light of the various places.


                        <p><a href="#" class=" btn_1 white">Read more</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- End container-fluid  -->


        <!-- End Container -->
    </main>
    <!-- End main -->
@stop



@section('footer')


@stop
