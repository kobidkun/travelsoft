@extends('frontend.index')
@section('css')
    <link href="{{asset('/frontend/css/timeline.css')}}" rel="stylesheet">
@stop


@section('content')

    <link href="{{asset('/frontend/css/timeline.css')}}" rel="stylesheet">


    <section class="parallax-window" data-parallax="scroll"

             data-image-src="{{asset('/storage/'.$car->bg_img)}}"

             data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1>{{$car->name}}</h1>

                        <span>Upto {{$car->capacity}} Person</span>

                     </div>
                    <div class="col-md-4">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End section -->

    <main>
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a>
                    </li>

                    </li>
                    <li>{{$car->name}}</li>
                </ul>
            </div>
        </div>
        <!-- End Position -->


        <!-- End Map -->

        <div class="container margin_60">
            <div class="row">
                <div class="col-lg-8" id="single_tour_desc">



                    <!-- Map button for tablets/mobiles -->
                    <div id="single_tour_feat">
                        <ul>


                                <li><i class="icon_set_1_icon-29"></i>Up to {{$car->capacity}} passengers</li>
                                <li><i class="icon_set_1_icon-6"></i>Hotel Pick up</li>
                                <li><i class="icon_set_2_icon-112"></i>{{$car->type}}</li>






                        </ul>
                    </div>

                    <div id="Img_carousel" class="slider-pro">
                        <div class="sp-slides">



                                <div class="sp-slide">
                                    <img alt="Image" class="sp-image" src="{{asset('frontend/css/images/blank.gif')}}"


                                         data-src="{{asset('/storage/'.$car->sm_img)}}"
                                         data-small="{{asset('/storage/'.$car->sm_img)}}"
                                         data-medium="{{asset('/storage/'.$car->sm_img)}}"
                                         data-large="{{asset('/storage/'.$car->sm_img)}}"
                                         data-retina="{{asset('/storage/'.$car->sm_img)}}">
                                </div>




                        </div>
                        <div class="sp-thumbnails">

                                <img alt="Image" class="sp-thumbnail" src="{{asset('/storage/'.$car->sm_img)}}">



                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-lg-3">
                            <h3>Description</h3>
                        </div>









                        <div class="col-lg-9">
                            <h4>{{$car->name}}</h4>





                            <p>

                                {{$car->description}}

                            </p>









                            <div class="row">


                            </div>
                            <!-- End row  -->
                        </div>


























                    </div>
                    <hr>

                    <hr>







                </div>
                <!--End  single_tour_desc-->

                <aside class="col-lg-4" id="sidebar">
                    <p class="d-none d-xl-block d-lg-block d-xl-none">



                    </p>
                    <div class="theiaStickySidebar">
                        <div class="box_style_1 expose" id="booking_box">
                            <h3 class="inner">- Booking -</h3>

                            <form action="{{route('front.collect.leads')}}"

                                  method="POST"
                                  enctype="multipart/form-data"
                            >

                                @csrf

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><i class="icon-person"></i>First Name</label>
                                        <input class=" form-control" name="fname" placeholder="Name" type="text">
                                        <input class=" " name="create_tour_id" value="{{$car->id}}" type="hidden">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><i class="icon-person"></i>Last Name</label>
                                        <input class=" form-control" name="lname" placeholder="Name" type="text">
                                        <input class=" " placeholder="Name" type="hidden">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><i class="fa fa-envelope"></i> Email</label>
                                        <input class=" form-control" placeholder="Email" name="email" type="email">
                                    </div>
                                </div>


                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><i class="icon-phone-1"></i> Phone</label>
                                        <input class=" form-control" placeholder="Phone" name="phone" type="text">
                                    </div>
                                </div>

                            </div>

                                <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><i class="icon-calendar-7"></i> Select a date</label>
                                        <input class="date-pick form-control" name="date" data-date-format="M d, D" type="date">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Adults</label>
                                        <div class="numbers-row">
                                            <input type="text" value="1" n id="adults" class="qty2 form-control"
                                                   name="adult">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Children</label>
                                        <div class="numbers-row">
                                            <input type="text" value="0" id="children" class="qty2 form-control" name="children">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <table class="table table_summary">
                                <tbody>


                                </tbody>
                            </table>
                            <button type="submit" class="btn_full">Customize & Get Quotes</button>

                            </form>
                        </div>
                        <!--/box_style_1 -->
                    </div>
                    <!--/sticky -->

                </aside>
            </div>
            <!--End row -->
        </div>
        <!--End container -->

        <div id="overlay"></div>
        <!-- Mask on input focus -->

    </main>
    <!-- End main -->
@stop



@section('footer')


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>



    <script src="{{asset('frontend/js/jquery.sliderPro.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function ($) {
            $('#Img_carousel').sliderPro({
                width: 960,
                height: 500,
                fade: true,
                arrows: true,
                buttons: false,
                fullScreen: false,
                smallSize: 500,
                startSlide: 0,
                mediumSize: 1000,
                largeSize: 3000,
                thumbnailArrows: true,
                autoplay: false
            });
        });
    </script>

    <!-- Fixed sidebar -->
    <script src="{{asset('/frontend/js/theia-sticky-sidebar.js')}}"></script>
    <script>
        jQuery('#sidebar').theiaStickySidebar({
            additionalMarginTop: 80
        });
    </script>
@stop
