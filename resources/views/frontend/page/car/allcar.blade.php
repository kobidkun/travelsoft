@extends('frontend.index')


@section('content')

    <section class="parallax-window" data-parallax="scroll" data-image-src="{{'/images/bg/carbg.jpg'}}"
             data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>All tours</h1>
                <p></p>
            </div>
        </div>
    </section>


    <!-- End section -->

    <main>
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a>
                    </li>
                    <li><a href="#">Cab Bookings</a>
                    </li>

                </ul>
            </div>
        </div>
        <!-- Position -->



        <div class="container margin_60">
            <div class="row">

                <!--End aside -->

                <div class="col-lg-12">

                    <div id="tools">
                        <div class="row">

                        </div>
                    </div>
                    <!--End tools -->

                    <div class="row">

                        @foreach($cars as $tour)

                        <div class="col-md-6 wow zoomIn" data-wow-delay="0.1s">
                            <div class="tour_container">
                                <div class="ribbon_3 popular"><span>Popular</span>
                                </div>
                                <div class="img_container">
                                    <a href="{{route('front.car.details',$tour->id)}}">
                                        <img src="{{asset('/storage/'.$tour->sm_img)}}"

                                             width="800" height="533" class="img-fluid" alt="Image">
                                        <div class="short_info">
                                            <i class="icon_set_1_icon-21"></i>{{$tour->type}}

                                            <span class="price"><sup>
                                                <i class=" icon_set_2_icon-105"></i> {{$tour->capacity}} Seater
                                            </sup> </span>
                                        </div>
                                    </a>
                                </div>
                                <div class="tour_title" style="height: 60px">
                                    <h3><strong>{{$tour->name}}  </strong>upto {{$tour->capacity}} Person(s) </h3>

                                    <!-- end rating -->

                                    <!-- End wish list-->
                                </div>
                            </div>
                            <!-- End box tour -->
                        </div>
                        <!-- End col-md-6 -->

                        @endforeach


                        <!-- End col-md-6 -->
                    </div>
                    <!-- End row -->



                    <hr>

                    <nav aria-label="Page navigation">


                        <ul class="pagination justify-content-center">



                          {{--  <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active"><span class="page-link">1<span class="sr-only">(current)</span></span>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>--}}



                        </ul>
                    </nav>
                    <!-- end pagination-->

                </div>
                <!-- End col lg 9 -->
            </div>
            <!-- End row -->
        </div>
        <!-- End container -->
    </main>
    <!-- End main -->
@stop



@section('footer')


@stop
