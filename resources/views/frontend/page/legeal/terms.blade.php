@extends('frontend.index')


@section('content')

    <section class="parallax-window" data-parallax="scroll" data-image-src="{{'/images/bg/bg.jpg'}}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>Ter,s & Conditions</h1>
                <p></p>
            </div>
        </div>
    </section>


    <main>
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a>
                    </li>

                    <li>Terms & Conditions</li>
                </ul>
            </div>
        </div>
        <!-- End Position -->

        <div class="container margin_60">
            <div class="row justify-content-center">

                <div class="col-lg-8">

                    <div class="row">
                        <div class="col-md-12">


                            <h1><strong>Cost Includes :</strong></h1>
                            <ol>
                                <li>Accommodation on twin Sharing Basis.</li>
                                <li>Meal Plan (Please refer Cost sheet)</li>
                                <li>Exclusive Non a/c vehicle for transfers &amp; sightseeing. Please brief to guest that vehicle will not be at disposal it will be available to guest as per itinerary only (point to point basis)</li>
                                <li>All permit fees &amp; hotel taxes (as per itinerary).</li>
                                <li>Rates are valid for INDIAN NATIONALS only</li>
                            </ol>
                            <h1><strong>Cost Excludes :</strong></h1>
                            <ol>
                                <li>Air Fare / Train fare.</li>
                                <li>Personal expenses such as laundry, telephone calls, tips &amp; gratuity, mineral water, soft &amp; hard drinks, rafting, rock climbing, paragliding, joy ride (Toy Train), porterage.</li>
                                <li>Additional sightseeing or extra usage of vehicle, other than mentioned in the itinerary.</li>
                                <li>Entrance Fees &amp; Guide charges.</li>
                                <li>Any cost arising due to natural calamities like, landslides, road blockage, political disturbances (strikes), etc (to be borne by the client, which is directly payable on the spot).</li>
                                <li>Any increase in taxes or fuel price, leading to increase in cost on surface transportation &amp; land arrangements, which may come into effect prior to departure.</li>
                                <li>Service Tax 3.09 %</li>
                                <li>Travel Insurance.</li>
                                <li>Anything which is not include in the inclusion.</li>
                            </ol>
                            <h1><strong>Child Policy :</strong></h1>
                            <ol>
                                <li>Child Below 5 yrs complimentary</li>
                                <li>Child 5 -10 yrs without extra bed as mention in cost column (CNB = Child no Bed)</li>
                                <li>Child 5 -10 yrs with extra bed as mention in cost column (CWB = Child with Bed)</li>
                                <li>Above 10 years / Extra adult with an extra bed sharing room are same &amp; charged as per (EPSR)</li>
                                <li>Hotels are very strict with the child policy. Please carry the age proof so that it can be produced if asked by hotel.</li>
                            </ol>
                            <h1><strong>Cancellation Policy :</strong></h1>
                            <ol>
                                <li>Minimum Cancellation is Rs. 1000 per Head.</li>
                                <li>25% Between 30-20 Days Before Tour Departure</li>
                                <li>50% Between 20-10 Days Before Tour Departure</li>
                                <li>75% Between 10-05 Days Before Tour Departure</li>
                                <li>100% on the Same Day &amp; No show</li>
                                <li>Cancellation charges as % of Total Tour Cost</li>
                            </ol>
                            <h1><strong>Important: We need following to process your booking</strong></h1>
                            <ol>
                                <li>Guest Name &amp; Contact Number</li>
                                <li>Naming List with gender &amp; age</li>
                                <li>Arrival / Departure details</li>
                            </ol>
                            <p>&nbsp;</p>
                            <h1><strong>About Vehicle</strong></h1>
                            <ol>
                                <li>We provide exclusive vehicles in our packages.</li>
                                <li>Non&nbsp; A/C &nbsp;Santro&nbsp; Wagnor / Alto for 1-3 Pax including child.</li>
                                <li>Non&nbsp;&nbsp; A/C Sumo / Max / Hard top Jeeps for 4-8 pax.</li>
                                <li>Innova / Scorpio / Xylo / Tavera for 6 pax including child.</li>
                                <li>Gangtok city tour is done only by Maruti van / Indica / Wagon R (4 people in a car).</li>
                                <li>If guest are booked for luxury vehicle, Innova / Scorpio / Xylo / Tavera will be provided as per availability.</li>
                                <li>Only 06 heads are allowed in 1 Innova / Scorpio / Xylo including child. In case the number of heads traveling increases, than the guest will have to take another vehicle.</li>
                                <li>In one Sumo / Max only 08 heads are allowed including child. If the number of heads traveling is more than 08, then guest will have to take another vehicle.</li>
                                <li>Incase the guest is opting for Innova / Scorpio inform the guest to travel light with minimum baggage of one piece per person. In case of excess baggage the guest will have to opt for an extra vehicle for carrying the excess baggage. The cost for this will be borne by guest directly.</li>
                            </ol>
                            <h1><strong>Important : About Hotel &ndash;</strong></h1>
                            <ol>
                                <li>Check-in 13:00 hrs. / Check-out time is 11:00 hrs.</li>
                                <li>The tourism infrastructure in Sikkim &amp; Darjeeling is not very developed and one should not expect or compare it with the standards of plains &amp; other developed destinations there is always scarcity of water and power in the region and so you would come across situation like water (hot) being supplied on time basis &ndash; morning &amp; evening.</li>
                                <li>Room Heater available with an extra cost bourne by the guest directly (Except at&nbsp;&nbsp; Super Luxury , Luxury&nbsp; , Heritage&nbsp;&nbsp; Premium&nbsp; &amp;&nbsp; Classic&nbsp;&nbsp; categories)</li>
                                <li>Category of Rooms in the packages normally remains to deluxe&nbsp;&nbsp; Room .</li>
                                <li>Hotels are very strict with the child policy. Please carry the age proof so that it can be produced if asked by hotel.</li>
                                <li>A valid photo ID proof for all guests staying at the hotel is mandatory.</li>
                                <li>For Extra adult in the room we will provide an extra bed (wherever possible), but most of the hotels only provide extra&nbsp; mattress or roll out bed. Most of the hotels have no provision of an extra bed.</li>
                                <li>Operator reserves the right to re-arrange itinerary to suit hotel availability without changing the total number of days in&nbsp; each destination and without compromising any services.</li>
                                <li>In the area (almost all) the hotels do not have a lift facility and in all the hotels the guest will have to climb stairs. In case&nbsp; if any guest is having problem of blood pressure, knee or other ailment for which they cannot climb the higher floors&nbsp; they will have to intimate us during the time of booking so that we can arrange rooms at the lowest floor of the hotel&nbsp; enabling the guest to climb the minimum number of stairs possible.</li>
                            </ol>
                            <h1><strong>About Transport&nbsp;</strong>-</h1>
                            <ol>
                                <li>If arrival of guest is by train at NJP Railway Station, please be informed that our executive will be there to receive the guest at the New Exit Gate (Near Escalator) or arrive at Bagdogra Airport our executive will be there with the PLA &ndash;&nbsp; CARD at Exit Gate on the Airport.</li>
                                <li>The guest should always keep cool with the drivers as they are not tourism educated and come from different remote villages.</li>
                                <li>As there is shortage of space for car parking in the entire Sikkim &amp; Darjeeling region &ndash; guest will have to wait at the Lobby in time for the vehicle to start their sightseeing / transfers.</li>
                                <li>We would appreciate, if the guest does not lend their ears to the drivers as most of the time they misguide the passengers. In such instances we would request them to contact our executive.</li>
                                <li>On arrival of guests we will provide mineral water, small kits, local tour co-coordinator number &amp; service voucher. Our representative will brief the total tour itinerary to the guest.</li>
                                <li>As most of the services are on point to point basis, it is obvious that the drivers would also keep changing. Keeping this in mind please ensure that you do not leave your belongings in the vehicle as there is very little possibility of getting them back. Always check your baggage before leave the vehicle.</li>
                                <li>In this sector the same vehicles will not be providing for the entire tour, it will be changed sector-wise. So we request you to inform the same to your guest to avoid any circumstance.</li>
                                <li>If any tourist spot do not complete which falls on closing day &amp; if they wants to do the same on next day then they have&nbsp; to pay the extra cost for the vehicle.</li>
                                <li>If guests want any changes in their sightseeing schedule they should be informed to our executive previous day before 16:00 hrs, after that no changes are allowed. If the guest inform after 16:00 hrs it will be consider as a cancellation with no refund or guest will pay extra cost for reschedule the sightseeing.</li>
                                <li>Please inform your guest to maintain the timing for the sightseeing / transfers which will advisable by our executive. Mainly in Gangtok vehicle will not allowed to park at the hotel instead of parking stand. Once our vehicle will leave the hotel area without picking up the guests &amp; come back at parking stand then guest must have to arrange their own to&nbsp; reach at the parking stand.</li>
                                <li>In Gangtok outside vehicle are not allowed to enter in the city. Only local taxi can roam around within Gangtok city. All&nbsp; outside vehicle will pick up &amp; drop them from Deorali taxi stand. We provide local taxi for pick up &amp; drop from hotel to taxi stand. For this reason guest have to wait sometime in the taxi stand.</li>
                                <li>In case the guest is opting for Innova / Scorpio inform the guest to travel with minimum baggage (one baggage per person). In case of excess baggage the guest will have to opt for an extra vehicle for carrying the excess baggage. The&nbsp; cost for this extra vehicle will be born by guest directly.</li>
                                <li>Operator reserves the right to re-arrange itinerary to suit hotel availability without changing the total number of days in&nbsp; each destination and without compromising any services.</li>
                                <li>Only 06 heads are allowed in 1 Innova / Scorpio / Xylo including child and 08 heads are allowed in 1 Sumo / Maxincluding child. In case the number of travelers are increases, than the guest will have to opt for the additional vehicle with an extra cost which is directly payable by the guest on spot.</li>
                            </ol>
                            <p>&nbsp;</p>
                            <h1><strong>Terms &amp; Conditions</strong></h1>
                            <ol>
                                <li>Every tourist should send a Xerox copy of their voter&rsquo;s identity card or passport and three passport size photographs at least ten days before the tour starts. Tourists should carry their id cards at all times during the tour.</li>
                                <li>Booking to be confirmed before 15 days with payment.</li>
                                <li>Cancellation charge of 50% if cancelled before 7 days, 75% if cancelled before 3 days and 100% if cancelled on the date of commencement of tour.</li>
                                <li>Company does not have any liability if the tour is cancelled or itinerary changed during the tour due to any unpredictable event.</li>
                                <li>Company does not cover insurance in case of any accidents or loss of belongings, etc.&nbsp;</li>
                                <li>Do not carry Indian Rs 500 or Rs 1000 currency notes.</li>
                                <li>Tobacco &amp; Tobacco products are banned in Bhutan.</li>
                                <li>Do not bring any plant or agricultural product out of Bhutan. It is illegal.</li>
                                <li>Credit cards are accepted in selected establishments in Bhutan.</li>
                            </ol>
                            <p><strong>NOTE :</strong></p>
                            <ul>
                                <ul>
                                    <li>* Permit require for Tsomgo Lake. It is mandatory that guest should carry 04 copies of pass &ndash; port size photograph, original photo ID proof with photo copy(identity card &ndash; Voter ID card/Pass-port/Driving License etc) for processing the permit.</li>
                                </ul>
                            </ul>
                            <p>&nbsp;</p>
                            <ul>
                                <ul>
                                    <li>* Permit also require for traveling to North Sikkim (Lachung &amp; Lachen).Requires 02 more copies of passport size photographs along with original photo ID proof.</li>
                                </ul>
                            </ul>
                            <p>&nbsp;</p>
                            <ul>
                                <ul>
                                    <li>* The above mentioned hotels will be confirmed as per the room availability. Otherwise we will confirm similar category.</li>
                                </ul>
                            </ul>
                            <p>&nbsp;</p>
                            <ul>
                                <li>* The rates quoted in the packages are on per person basis, valid for Indian nationals &amp; till 15th&nbsp;Sep 2014 (High seasons surcharge will be applicable from 20TH Dec &ndash; 04TH Jan).</li>
                            </ul>
                            <p>Thanking you&nbsp;and we assuring&nbsp;our&nbsp;best&nbsp;services&nbsp;at all time. For&nbsp;any further clarification, please&nbsp; feel free to&nbsp;contact&nbsp;us any time.</p>
                            <p>&nbsp;</p>
                            <p>I hope you will find above as pre your requirement. Incase of anything. Please feel free to get in touch. We will look forward to hear a positive reply from your side.</p>
                            <p>&nbsp;</p>


                        </div>
                    </div><!-- Edn row -->
        <!-- End container -->
        <div style="height: 350px"></div>



    </main>
    <!-- End main -->
@stop



@section('footer')


@stop
