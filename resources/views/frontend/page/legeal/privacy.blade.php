@extends('frontend.index')


@section('content')

    <section class="parallax-window" data-parallax="scroll" data-image-src="{{'/images/bg/bg.jpg'}}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>Ter,s & Conditions</h1>
                <p></p>
            </div>
        </div>
    </section>


    <main>
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a>
                    </li>

                    <li>Privacy Policy</li>
                </ul>
            </div>
        </div>
        <!-- End Position -->

        <div class="container margin_60">
            <div class="row justify-content-center">

                <div class="col-lg-8">

                    <div class="row">
                        <div class="col-md-12">

                            <h2>Disclaimer Policy</h2>
                            <p>Information on this website may be subjected to inaccuracies or typographical errors. This website doesn't constitute an offer to contract. Travel next makes no representations whatsoever about other websites that endorse the same nature of intellectual services we offer. We exercise no exclusive control whatsoever over the content of foreign websites. We do not endorse or accept any responsibility for any other website except ours.<span>&nbsp;</span><br /><br />Without limiting the foregoing, Travel next shall not be liable to you or your business for any incidental, consequential, special, or punitive damages/lost or imputed profits/royalties arising out of specific agreements, goods or services provided - whether for breach of warranty or any other obligation arising therefrom or otherwise, whether liability is asserted in contract or tort(including negligence and strict product liability) and irrespective of whether we have been advised of the possibility of any such loss or damage. Each party hereby waives any claims that these exclusions deprive such party of an adequate remedy. You acknowledge that third party product and service providers may advertise their products and services on Travel next making any representation or warranty regarding any third party products and services to you. However, you acknowledge and agree that at no time is Travel next making any representation or warranty regarding third party product &amp; services, nor will Travel next be liable to you or any third party over claims arising from or in connection with such third party products and services. You hereby disclaim and waive any rights and claims you may have against Travel next with respect to third party products and services, to the maximum extent permitted by the law.</p>
                        </div>
                        <div class="text-widget">
                            <h2>Privacy Policy</h2>
                            <p>We at Travel next, hold your Privacy to the highest esteem. We strictly discourage &amp; refrain from giving, leasing or selling or simply disclosing personal client information like contact forms, download requests, email lists or simple general or contact information.<span>&nbsp;</span><br /><br />Any information you submit to our official website will be held at the state of utmost confidentiality. For further queries, please email us.</p>
                        </div>
                        </div>
                    </div><!-- Edn row -->
        <!-- End container -->
        <div style="height: 350px"></div>



    </main>
    <!-- End main -->
@stop



@section('footer')


@stop
