@extends('frontend.index')


@section('content')

    <section class="parallax-window" data-parallax="scroll" data-image-src="{{'/images/bg/bg.jpg'}}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>Contact</h1>
                <p></p>
            </div>
        </div>
    </section>


    <main>
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a>
                    </li>

                    <li>Contact</li>
                </ul>
            </div>
        </div>
        <!-- End Position -->

        <div class="container margin_60">
            <div class="row">
                <div class="col-md-8">
                    <div class="form_title">
                        <h3><strong><i class="icon-pencil"></i></strong>Fill the form below</h3>
                        <p>

                        </p>
                    </div>
                    <div class="step">

                        <div id="message-contact"></div>
                        <form method="post" action="assets/contact.php" id="contactform">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control" id="name_contact" name="name_contact" placeholder="Enter Name">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" id="lastname_contact" name="lastname_contact" placeholder="Enter Last Name">
                                    </div>
                                </div>
                            </div>
                            <!-- End row -->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" id="email_contact" name="email_contact" class="form-control" placeholder="Enter Email">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="text" id="phone_contact" name="phone_contact" class="form-control" placeholder="Enter Phone number">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea rows="5" id="message_contact" name="message_contact" class="form-control" placeholder="Write your message" style="height:200px;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Human verification</label>
                                    <input type="text" id="verify_contact" class=" form-control add_bottom_30" placeholder="Are you human? 3 + 1 =">
                                    <input type="submit" value="Submit" class="btn_1" id="submit-contact">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End col-md-8 -->

                <div class="col-md-4">
                    <div class="box_style_1">
                        <span class="tape"></span>
                        <h4>Address <span><i class="icon-pin pull-right"></i></span></h4>
                        <p>
                            {{env('ADD1')}}, {{env('ADD2')}} , {{env('CITY')}}, {{env('PIN')}}
                        </p>
                        <hr>
                        <h4>Help center <span><i class="icon-help pull-right"></i></span></h4>

                        <ul id="contact-info">
                            <li>{{env('PHONE')}} / {{env('PHONE2')}}</li>
                            <li><a href="#">{{env('EMAIL')}}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="box_style_4">
                        <i class="icon_set_1_icon-57"></i>
                        <h4>Need <span>Help?</span></h4>
                        <a href="tel://004542344599" class="phone">{{env('PHONE')}}</a>
                        <small>Monday to Friday 9.00am - 7.30pm</small>
                    </div>
                </div>
                <!-- End col-md-4 -->


                <div class="col-md-12">
                    <div class="container">
                        <div class="row">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3897.826796697806!2d76.62976435090746!3d12.327442091238005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3baf7a85b061bdff%3A0x296b45ae45e824f8!2zVHJhdmVsIE5leHQgVG91cnMgJiBUcmF2ZWxzICjgsp_gs43gsrDgsr7gsrXgsrLgs40g4LKo4LOG4LKV4LON4LK44LON4LKfKSAtIEJlc3QgVHJhdmVsIEFnZW5jeSBpbiBNeXN1cnU!5e0!3m2!1sen!2sin!4v1548261836981"
                                    width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>

                        </div>

                    </div>
                </div>

            </div>
            <!-- End row -->
        </div>
        <!-- End container -->
        <div style="height: 350px"></div>



    </main>
    <!-- End main -->
@stop



@section('footer')


@stop
