
<link href="{{asset('frontend/css/vendors.css')}}" rel="stylesheet" type="text/css"/>

<title>Tour Map for {{$city->title}}</title>
<div style="height: 670px" id="map"></div>


<script src="{{asset('admin/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{asset('admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
      integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
      crossorigin=""/>
<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
        integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
        crossorigin=""></script>


<script>

    function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2-lat1);  // deg2rad below
        var dLon = deg2rad(lon2-lon1);
        var a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c; // Distance in km
        return d;
    }

    function deg2rad(deg) {
        return deg * (Math.PI/180)
    }







    var map = L.map('map').setView([{{$linkedcity->lat}}, {{$linkedcity->long}}], 10);


    var HotelIcon = L.icon({
        iconUrl: '{{asset('/admin/img/icon/hotel.png')}}',
        iconSize: [45, 45],
    });

    var PlaceIcon = L.icon({
        iconUrl: '{{asset('/admin/img/icon/placeholder.png')}}',
        iconSize: [45, 45],
    });


    //places


    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);


    @foreach($places as $place)

    L.marker([{{$place->create_places->lat}}, {{$place->create_places->long}}],{icon: PlaceIcon}).addTo(map)
        .bindPopup(' <h6>{{$place->create_places->name}} </h6> '
            +'<br> {{$place->create_places->description}}'+
    '<img style="display: block;' +
    '  margin-left: auto;' +
    '  margin-right: auto;" height="100px" src="{{asset('/storage/'.$place->create_places->sm_img)}}">'
    )
        .openPopup();

    @endforeach











</script>
