@extends('frontend.index')


@section('content')

    <section class="parallax-window" data-parallax="scroll" data-image-src="{{'/storage'.$city->bg_img}}" data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-1">
            <div class="animated fadeInDown">
                <h1>{{$city->name}} tours</h1>
                <p>{{$city->description}}</p>
            </div>
        </div>
    </section>


    <!-- End section -->

    <main>
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a>
                    </li>
                    <li><a href="#">City</a>
                    </li>
                    <li>{{$city->name}}</li>
                </ul>
            </div>
        </div>
        <!-- Position -->

        <div class="collapse" id="collapseMap">
            <div id="map" class="map"></div>
        </div>
        <!-- End Map -->

        <div class="container margin_60">
            <div class="row">
                <aside class="col-lg-3">
                    <p>
                        <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Hide map" data-text-original="View on map">View on map</a>
                    </p>

                    <div class="box_style_cat">

                        <div style="padding: 8px">
                            <a data-toggle="collapse" href="#collapseFilters" aria-expanded="false"
                               aria-controls="collapseFilters" id="filters_col_bt">
                                By Theme</a>
                        </div>


                        <ul id="cat_nav">

                            <li><a href="{{route('front.by.theme','Honeymoon')}}"><i class="icon_set_1_icon-82"></i>Honeymoon <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Family')}}"><i class="icon_set_1_icon-64"></i>Family <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Friends / Group')}}"><i class="icon_set_1_icon-20"></i>Friends / Group <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Solo')}}"><i class="icon_set_1_icon-70"></i>Solo <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Adventure')}}"><i class="icon_set_1_icon-30"></i>Adventure <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Nature')}}"><i class="icon_set_1_icon-37"></i>Nature <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Wildllife')}}"><i class="icon_set_1_icon-24"></i>Wildllife <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Religious')}}"><i class="icon_set_1_icon-2"></i>Religious <span></span></a>
                            </li>

                        </ul>
                    </div>

                    <div id="filters_col">

                        <div class="collapse show" id="collapseFilters">


                            <div class="filter_type">

                                <ul>

                                    @foreach($alltags as $alltag)

                                    <li>

                                        <a href="{{route('front.by.tag',$alltag->tag)}}" class="btn_1 outline">{{$alltag->tag}}</a>

                                    </li>

                                        @endforeach

                                </ul>
                            </div>
                        </div>
                        <!--End collapse -->
                    </div>
                    <!--End filters col-->
                    <div class="box_style_2">
                        <i class="icon_set_1_icon-57"></i>
                        <h4>Need <span>Help?</span></h4>
                        <a href="{{env('PHONE')}}" class="phone">{{env('PHONE')}}</a>
                        <small>24X7 365 Days</small>
                    </div>
                </aside>
                <!--End aside -->

                <div class="col-lg-9">


                    <!--End tools -->

                    <div class="row">

                        @foreach($tours as $tour)

                        <div class="col-md-6 wow zoomIn" data-wow-delay="0.1s">
                            <div class="tour_container">
                                <div class="ribbon_3 popular"><span>Popular</span>
                                </div>
                                <div class="img_container">
                                    <a href="{{route('front.details',$tour->id)}}">
                                        <img src="{{asset('/storage/'.$tour->img_sm)}}"


                                             width="800" height="533" class="img-fluid" alt="Image">
                                        <div class="short_info">
                                            <i class="icon_set_1_icon-37"></i>{{$tour->create_cities->name}}

                                            <span class="price"><sup>
                                                <i class=" icon_set_1_icon-53"></i> {{$tour->days}} Days
                                            </sup> </span>
                                        </div>
                                    </a>
                                </div>
                                <div class="tour_title" style="height: 60px">
                                    <h3><strong>{{$tour->title}} | {{$tour->days}}</strong> Days</h3>

                                    <!-- end rating -->

                                    <!-- End wish list-->
                                </div>
                            </div>
                            <!-- End box tour -->
                        </div>
                        <!-- End col-md-6 -->

                        @endforeach


                        <!-- End col-md-6 -->
                    </div>
                    <!-- End row -->



                    <hr>

                    <nav aria-label="Page navigation">


                        <ul class="pagination justify-content-center">

                            {{ $tours->links() }}

                          {{--  <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active"><span class="page-link">1<span class="sr-only">(current)</span></span>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>--}}



                        </ul>
                    </nav>
                    <!-- end pagination-->

                </div>
                <!-- End col lg 9 -->
            </div>
            <!-- End row -->
        </div>
        <!-- End container -->
    </main>
    <!-- End main -->
@stop



@section('footer')


@stop
