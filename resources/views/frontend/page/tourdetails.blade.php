@extends('frontend.index')
@section('css')
    <link href="{{asset('/frontend/css/timeline.css')}}" rel="stylesheet">
@stop


@section('content')

    <link href="{{asset('/frontend/css/timeline.css')}}" rel="stylesheet">


    <section class="parallax-window" data-parallax="scroll"

             data-image-src="{{asset('/storage/'.$tour->img_bg)}}"

             data-natural-width="1400" data-natural-height="470">
        <div class="parallax-content-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <h1>{{$tour->title}} - {{$tour->days}} Days</h1>
                        <span>{{$tour->create_cities->name}} </span>
                     </div>
                    <div class="col-md-4">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End section -->

    <main>
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#">Home</a>
                    </li>
                    <li><a href="#">{{$tour->create_cities->name}}</a>
                    </li>
                    <li>{{$tour->title}} - {{$tour->days}} Days</li>
                </ul>
            </div>
        </div>
        <!-- End Position -->


        <!-- End Map -->

        <div class="container margin_60">
            <div class="row">
                <div class="col-lg-8" id="single_tour_desc">


                    <p class="d-none d-md-block d-block d-lg-none"><a class=" "
                                                                      href="{{route('front.map.id',$tour->id)}}" target="_blank" aria-expanded="false"
                                                                      data-text-original="View on map">
                            View on map</a></p>
                    <!-- Map button for tablets/mobiles -->
                    <div id="single_tour_feat">
                        <ul>



                            @foreach($tour->create_tour_to_icons as $icons)

                                <li><i class="{{$icons->icon}}"></i></li>

                            @endforeach


                        </ul>
                    </div>

                    <div id="Img_carousel" class="slider-pro">
                        <div class="sp-slides">

                            @foreach($galleries as $gallery)

                            <div class="sp-slide">
                                <img alt="Image" class="sp-image" src="{{asset('frontend/css/images/blank.gif')}}"


                                     data-src="{{asset('/storage/'.$gallery->location)}}"
                                     data-small="{{asset('/storage/'.$gallery->location)}}"
                                     data-medium="{{asset('/storage/'.$gallery->location)}}"
                                     data-large="{{asset('/storage/'.$gallery->location)}}"
                                     data-retina="{{asset('/storage/'.$gallery->location)}}">
                            </div>

                            @endforeach



                        </div>
                        <div class="sp-thumbnails">
                            @foreach($galleries as $gallery)
                            <img alt="Image" class="sp-thumbnail" src="{{asset('/storage/'.$gallery->location)}}">

                            @endforeach

                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-lg-3">
                            <h3>Description</h3>
                        </div>









                        <div class="col-lg-9">
                            <h4>{{$tour->title}} - {{$tour->days}} Days</h4>





                            <p>

                                {{$tour->description}}

                            </p>

                            <div class="row">


                                <div class="col-md-6">
                                    <ul class="list_icons">
                                        <li><i class="icon_set_1_icon-6"></i> Upto 3 Stars</li>
                                        <li><i class="icon_set_2_icon-116"></i> Sightseeing</li>
                                        <li><i class=" icon_set_3_restaurant-5"></i> Breakfast</li>
                                    </ul>
                                </div>

                                <div class="col-md-6">
                                    <ul class="list_icons">
                                        <li><i class="icon_set_2_icon-115"></i> Stay Included</li>
                                        <li><i class="icon_set_2_icon-116"></i> Transfers</li>
                                        <li><i class="icon_set_2_icon-105"></i> Tour guide</li>
                                    </ul>
                                </div>

                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <h4>What's include</h4>
                                    <ul class="list_ok">
                                       @foreach($inclusions as $inclusion)
                                        <li>{{$inclusion->text}}</li>

                                           @endforeach


                                    </ul>
                                </div>

                                <div class="col-md-6">
                                    <h4>What's Exclude</h4>
                                    <ul class="list_ok">
                                        @foreach($exclusions as $exclusion)
                                            <li>{{$exclusion->text}}</li>

                                        @endforeach


                                    </ul>
                                </div>

                            </div>




                            <div class="row">


                            </div>
                            <!-- End row  -->
                        </div>


























                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <h3>Itinerary</h3>
                        </div>
                        <div class="col-lg-12">


                            <ul class="cbp_tmtimeline">

                                @foreach($itinaries as $itinary)
                                <li>
                                    <time class="cbp_tmtime" datetime="09:30"><span>Day</span> <span>{{$itinary->day}}</span>
                                    </time>
                                    <div class="cbp_tmicon timeline_icon_point"></div>
                                    <div class="cbp_tmlabel">

                                        <h2 style="color: #0b97c4">{{$itinary->title}}

                                            <span>
                                             <i style="font-size: 15px; color: #0b97c4" class="icon-location-6"></i>   {{$itinary->create_tour_to_itinary_to_cities->name}}

                                            </span >
                                        </h2>
                                        <div class="row">
                                            @foreach($itinary->create_tour_to_itinary_to_inclusions as $iconsotonary)

                                                <span style="padding: 10px; font-size: 15px;color: #0b97c4">
                                                    <i class="{{$iconsotonary->text}}"></i></span>


                                            @endforeach



                                        </div>
                                        <p>

                                            {{$itinary->description}}

                                        </p>


                                        <div style="padding: 20px" class="row">

                                            <div class="col-md-4">

                                                <a data-fancybox="gallery" href="{{asset('/storage/'.$itinary->img)}}"><img style="width: 180px;" src="{{asset('/storage/'.$itinary->img)}}"></a>


                                            </div>

                                            @foreach($itinary->create_tour_itinary_to_images as $img)
                                                <div class="col-md-4">
                                                    <a data-fancybox="gallery" href="{{asset('/storage/'.$img->location)}}"><img style="width: 180px;" src="{{asset('/storage/'.$img->location)}}"></a>


                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="row">
                                            @foreach($itinary->create_tour_to_itinary_to_places as $iticity)

                                                <span style="padding: 10px; font-size: 10px;">
                                                    <i class="icon-location-6"></i>
                                                    {{\App\Model\Place\CreatePlace::find($iticity->create_place_id)->name}}</span>


                                            @endforeach



                                        </div>




                                    </div>
                                </li>


                                @endforeach




                            </ul>











                        </div>
                    </div>
                    <hr>







                </div>
                <!--End  single_tour_desc-->

                <aside class="col-lg-4" id="sidebar">
                    <p class="d-none d-xl-block d-lg-block d-xl-none">
                    <p class="d-none d-md-block d-block d-lg-none"><a class=" "
                                                                      href="{{route('front.map.id',$tour->id)}}" target="_blank" aria-expanded="false"
                                                                      data-text-original="View on map">
                            View on map</a></p>

                        <a class="btn_map"
                           href="{{route('front.map.id',$tour->id)}}" target="_blank"
                          >View on map</a>
                    </p>
                    <div class="theiaStickySidebar">
                        <div class="box_style_1 expose" id="booking_box">
                            <h3 class="inner">- Booking -</h3>

                            <form action="{{route('front.collect.leads')}}"

                                  method="POST"
                                  enctype="multipart/form-data"
                            >

                                @csrf

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><i class="icon-person"></i>First Name</label>
                                        <input class=" form-control" name="fname" placeholder="Name" type="text">
                                        <input class=" " name="create_tour_id" value="{{$tour->id}}" type="hidden">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><i class="icon-person"></i>Last Name</label>
                                        <input class=" form-control" name="lname" placeholder="Name" type="text">
                                        <input class=" " placeholder="Name" type="hidden">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><i class="fa fa-envelope"></i> Email</label>
                                        <input class=" form-control" placeholder="Email" name="email" type="email">
                                    </div>
                                </div>


                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><i class="icon-phone-1"></i> Phone</label>
                                        <input class=" form-control" placeholder="Phone" name="phone" type="text">
                                    </div>
                                </div>

                            </div>

                                <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label><i class="icon-calendar-7"></i> Select a date</label>
                                        <input class="date-pick form-control" name="date" data-date-format="M d, D" type="date">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Adults</label>
                                        <div class="numbers-row">
                                            <input type="text" value="1" n id="adults" class="qty2 form-control"
                                                   name="adult">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Children</label>
                                        <div class="numbers-row">
                                            <input type="text" value="0" id="children" class="qty2 form-control" name="children">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <table class="table table_summary">
                                <tbody>


                                </tbody>
                            </table>
                            <button type="submit" class="btn_full">Customize & Get Quotes</button>

                            </form>
                        </div>
                        <!--/box_style_1 -->
                    </div>
                    <!--/sticky -->

                </aside>
            </div>
            <!--End row -->
        </div>
        <!--End container -->

        <div id="overlay"></div>
        <!-- Mask on input focus -->

    </main>
    <!-- End main -->
@stop



@section('footer')


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>



    <script src="{{asset('frontend/js/jquery.sliderPro.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function ($) {
            $('#Img_carousel').sliderPro({
                width: 960,
                height: 500,
                fade: true,
                arrows: true,
                buttons: false,
                fullScreen: false,
                smallSize: 500,
                startSlide: 0,
                mediumSize: 1000,
                largeSize: 3000,
                thumbnailArrows: true,
                autoplay: false
            });
        });
    </script>

    <!-- Fixed sidebar -->
    <script src="{{asset('/frontend/js/theia-sticky-sidebar.js')}}"></script>
    <script>
        jQuery('#sidebar').theiaStickySidebar({
            additionalMarginTop: 80
        });
    </script>
@stop
