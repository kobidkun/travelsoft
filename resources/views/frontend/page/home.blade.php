@extends('frontend.index')

@section('content')





    <main>

        @include('frontend.component.slider', ['sliders' => $sliders])


        <div class="container margin_60">

            <div class="main_title">
                <h2> <span>Top</span> Tours</h2>
                <p>.</p>
            </div>

            <div class="row">


                @foreach($tours as $tour)

                    <div class="col-lg-4 col-md-6 wow zoomIn">
                        <div class="tour_container">
                            <div class="ribbon_3 popular"><span>Popular</span></div>
                            <div class="img_container">
                                <a href="{{route('front.details',$tour->id)}}">
                                    {{--<img src="{{asset('/storage/'.$tour->img_sm)}}"  width="800" height="533" class="img-fluid" alt="Image">--}}
                                    <img src="{{asset('/storage/'.$tour->img_sm)}}"

                                         width="800" height="533" class="img-fluid" alt="Image">
                                    <div class="short_info">
                                        <i class="icon_set_1_icon-37"></i>{{$tour->create_cities->name}}

                                        <span class="price"><sup>
                                                <i class=" icon_set_1_icon-53"></i> {{$tour->days}} Days
                                            </sup> </span>

                                    </div>
                                </a>
                            </div>
                            <div class="tour_title" style="height: 60px">
                                <h3><strong>{{$tour->title}} | {{$tour->days}} Days</strong> </h3>
                                <div class="rating">



                                </div><!-- end rating -->

                            </div>
                        </div><!-- End box tour -->
                    </div><!-- End col -->

                @endforeach



            </div><!-- End row -->

            <p class="text-center nopadding">
                <a href="{{route('front.all')}}" class="btn_1 medium"><i class="icon-eye-7"></i>View all tours ({{$counttours}}) </a>
            </p>
        </div><!-- End container -->





        <div class="white_bg">
            <div class="container margin_60">
                <div class="main_title">
                    <h2> <span>Popular</span> Themes</h2>
                    <p>

                    </p>
                </div>
                <div class="row add_bottom_45">
                    <div class="col-lg-4 other_tours">
                        <ul>
                            <li><a href="{{route('front.by.theme','Honeymoon')}}"><i class="icon_set_1_icon-82"></i>Honeymoon <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Family')}}"><i class="icon_set_1_icon-64"></i>Family <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Friends / Group')}}"><i class="icon_set_1_icon-20"></i>Friends / Group <span></span></a>
                            </li>

                        </ul>
                    </div>
                    <div class="col-lg-4 other_tours">
                        <ul>
                            <li><a href="{{route('front.by.theme','Solo')}}"><i class="icon_set_1_icon-70"></i>Solo <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Adventure')}}"><i class="icon_set_1_icon-30"></i>Adventure <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Nature')}}"><i class="icon_set_1_icon-37"></i>Nature <span></span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-4 other_tours">
                        <ul>
                            <li><a href="{{route('front.by.theme','Wildllife')}}"><i class="icon_set_1_icon-24"></i>Wildllife <span></span></a>
                            </li>
                            <li><a href="{{route('front.by.theme','Religious')}}"><i class="icon_set_1_icon-2"></i>Religious <span></span></a>
                            </li>

                        </ul>
                    </div>
                </div>
                <!-- End row -->

                <div class="banner colored">
                    <h4>Top tours <span>from Darjeeling Sikkim Nepal & NE India</span></h4>
                    <p>

                    </p>
                    <a href="{{route('front.all')}}" class="btn_1 white">Read more</a>
                </div>

                <div class="row">
                    <div class="col-lg-3 col-md-6 text-center">
                        <p>
                            <a href="#"><img src="/images/services/tour.png" alt="Pic" class="img-fluid"></a>
                        </p>
                        <h4><span>Tour </span> packages</h4>

                    </div>



                    <div class="col-lg-3 col-md-6 text-center">
                        <p>
                            <a href="#"><img src="/images/services/car.png" alt="Pic" class="img-fluid"></a>
                        </p>
                        <h4><span>Car</span>Bookings</h4>

                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <p>
                            <a href="#"><img src="/images/services/hotel.png" alt="Pic" class="img-fluid"></a>
                        </p>
                        <h4><span>Hotel</span> bookings</h4>

                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <p>
                            <a href="#"><img src="/images/services/flight.png" alt="Pic" class="img-fluid"></a>
                        </p>
                        <h4><span>Flight</span> bookings</h4>

                    </div>
                </div>
                <!-- End row -->

            </div>
            <!-- End container -->
        </div>
        <!-- End white_bg -->





        <section class="full-width">







                @include('frontend.component.homepagecities',['cities' => $cities])




        </section>










        <div class="container margin_60">

            <div class="main_title">
                <h2>Some <span>good</span> reasons</h2>

            </div>

            <div class="row">

                <div class="col-lg-4 wow zoomIn" data-wow-delay="0.2s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-41"></i>
                        <h3><span>+1200</span> Premium tours</h3>

                        <a href="#" class="btn_1 outline">Read more</a>
                    </div>
                </div>

                <div class="col-lg-4 wow zoomIn" data-wow-delay="0.4s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-30"></i>
                        <h3><span>+10000</span> Customers</h3>

                        <a href="#" class="btn_1 outline">Read more</a>
                    </div>
                </div>

                <div class="col-lg-4 wow zoomIn" data-wow-delay="0.6s">
                    <div class="feature_home">
                        <i class="icon_set_1_icon-57"></i>
                        <h3><span>H24 </span> Support</h3>

                        <a href="#" class="btn_1 outline">Read more</a>
                    </div>
                </div>

            </div>
            <!--End row -->

            <hr>


        </div>
        <!-- End container -->


        <section class="full-width">
            <div class="">

                <style>
                    .bg-full-css{
                        height: 450px;


                    }


                    .content-bg-full{
                        text-align: center;
                    }
                    .heading-bg-full-text{
                        padding: 50px;
                        font-size: 65px;
                        text-transform: uppercase;
                        font-weight: bold;
                        color: #fff;
                        font-family: Comic Sans MS, Comic Sans, cursive	;
                    }

                    .bg-full-button a{

                        padding: 15px;
                        border-style: solid;
                        border-width: 3px;
                        color: white;
                        border-color: white;
                        font-weight: 500;
                        font-size: 25px;
                        background-color: rgba(0,0,0,0);
                    }

                    .slick-prev {



                        bottom: 0;
                        margin-top: 50px;
                        padding: 5px;
                        border-style: solid;
                        border-width: 1px;
                        color: white;
                        border-color: white;
                        font-weight: 400;
                        font-size: 10px;
                        background-color: rgba(0,0,0,0);
                        text-align: center;
                    }

                    .slick-next {


                        bottom: 0;
                        margin-top: 50px;
                        padding: 5px;
                        border-style: solid;
                        border-width: 1px;
                        color: white;
                        border-color: white;
                        font-weight: 400;
                        font-size: 10px;
                        background-color: rgba(0,0,0,0);
                        text-align: center;

                    }


                </style>

                <div class="your-class">


                    <div class="bg-full-css" style="
                     background:  linear-gradient(90deg, rgba(0,0,0,0.5) 59%, rgba(0,0,0,0.5) 100%), url('../../../frontend/img/city/darjeeling.jpg') ">
                        <div class="content-bg-full">
                            <div class="">
                                <h3 class="heading-bg-full-text"> Darjeeling</h3>

                                <div class="bg-full-button">
                                    <a class="" href="">View Tours Packages</a>
                                </div>

                                <div class="bg-next-button">

                                        <button type="button" class="slick-prev">Previous</button>
                                        <button type="button" class="slick-next"> Next </button>

                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="bg-full-css" style="
                     background:  linear-gradient(90deg, rgba(0,0,0,0.5) 59%, rgba(0,0,0,0.5) 100%), url('../../../frontend/img/city/sikkim.jpg') ">
                        <div class="content-bg-full">
                            <div class="">
                                <h3 class="heading-bg-full-text"> Gangtok</h3>

                                <div class="bg-full-button">
                                    <a class="" href="">View Tours Packages</a>
                                </div>

                                <div class="bg-next-button">

                                    <button type="button" class="slick-prev">Previous</button>
                                    <button type="button" class="slick-next">Next</button>

                                </div>


                            </div>


                        </div>
                    </div>


                    <div class="bg-full-css" style="
                     background:  linear-gradient(90deg, rgba(0,0,0,0.5) 59%, rgba(0,0,0,0.5) 100%), url('../../../frontend/img/city/bhutan.jpg') ">
                        <div class="content-bg-full">
                            <div class="">
                                <h3 class="heading-bg-full-text"> Bhutan</h3>

                                <div class="bg-full-button">
                                    <a class="" href="">View Tours Packages</a>
                                </div>

                                <div class="bg-next-button">

                                    <button type="button" class="slick-prev">Previous</button>
                                    <button type="button" class="slick-next">Next</button>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </section>






        <!-- End section -->

        <div class="container margin_60">

            <div class="main_title">
                <h2>What <span>customers </span>says</h2>

            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="review_strip">

                        <h4>Kamal Nath</h4>
                        <p>

                            "First let me thank them for making our trip memorable and comfortable. Travel Next is very good and i surely suggest this to our friends."

                        </p>
                        <div class="rating">
                            <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class=" icon-star voted"></i><i class=" icon-star voted"></i>
                        </div>
                    </div>
                    <!-- End review strip -->
                </div>

                <div class="col-lg-6">
                    <div class="review_strip">

                        <h4>Sumit Sisodia</h4>
                        <p>

                          "It was an awesome trip which we had, one of the best travel agents I have experienced, what they promise they have provided more than that, Special thanks to to you for your excellent support from the start till end"
                           </p>
                        <div class="rating">
                            <i class="icon-star voted"></i><i class="icon-star voted"></i><i class="icon-star voted"></i><i class=" icon-star voted"></i><i class=" icon-star-empty"></i>
                        </div>
                    </div>
                    <!-- End review strip -->
                </div>
            </div>




        </div>

        <section class="promo_full">
            <div class="promo_full_wp magnific">
                <div>
                    <h3>Witness Beautiful Sikkim</h3>

                    <a href="https://www.youtube.com/watch?v=jogvLSc2DFw" class="video"><i class="icon-play-circled2-1"></i></a>
                </div>
            </div>
        </section>


        <div class="container margin_60">


            <div class="row">
                <div class="col-md-6">
                    <img src="/images/services/plan.jpg" alt="Laptop" class="img-fluid laptop">
                </div>
                <div class="col-md-6">
                    <h3><span>Get started</span> with {{env('SITENAME')}}</h3>

                    <ul class="list_order">
                        <li><span>1</span>Select your preferred tours</li>
                        <li><span>2</span>Check Details</li>
                        <li><span>3</span>Your Booking Confirmed</li>
                    </ul>
                    <a href="{{route('front.all')}}" class="btn_1">Start now</a>
                </div>
            </div>
            <!-- End row -->


        </div>


    </main>
@stop


@section('header')
    <link href="{{asset('/frontend/js/slick/slick-1.8.1/slick/slick.css')}}" rel="stylesheet">
    <link href="{{asset('/frontend/layerslider/css/layerslider.css')}}" rel="stylesheet">

@stop

@section('footer')

    <script src="{{asset('frontend/layerslider/js/greensock.js')}}"></script>
    <script src="{{asset('frontend/layerslider/js/layerslider.transitions.js')}}"></script>
    <script src="{{asset('frontend/layerslider/js/layerslider.kreaturamedia.jquery.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            'use strict';
            $('#layerslider').layerSlider({
                autoStart: true,
                responsive: true,
                responsiveUnder: 1280,
                layersContainer: 1170,
                skinsPath: 'layerslider/skins/'
                // Please make sure that you didn't forget to add a comma to the line endings
                // except the last line!
            });
        });
    </script>

    <script>
        $('input.date-pick').datepicker('setDate', 'today');
        $('input.time-pick').timepicker({
            minuteStep: 15,
            showInpunts: false
        })
    </script>



    <!-- Check box and radio style iCheck -->
    <script>
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-grey',
            radioClass: 'iradio_square-grey'
        });
    </script>

    <script src="{{asset('/frontend/js/slick/slick-1.8.1/slick/slick.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.your-class').slick({
                autoplay: true,
                autoplaySpeed: 500,
                prevArrow: $('.slick-prev'),
                nextArrow: $('.slick-next')
            });
        });
    </script>

@stop
