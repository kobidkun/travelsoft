
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Best Tours & Travel booking platform for Gangtok Darjeeling and North East India.">
    <meta name="author" content="tecions">
    <meta name="theme-color" content="#0072bc">




<!-- MINIFIED -->
    {!! SEO::generate(true) !!}



    <!-- Favicons-->
    <link rel="shortcut icon" href="{{asset('/images/logo/fabicon.png')}}" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="{{asset('/images/logo/fabicon.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="{{asset('/images/logo/fabicon.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="{{asset('/images/logo/fabicon.png')}}">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="{{asset('/images/logo/fabicon.png')}}">

    <!-- COMMON CSS -->
    <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/style.css')}}" rel="stylesheet">


    <link href="{{asset('frontend/css/vendors.css')}}" rel="stylesheet">


    <link href="{{asset('frontend/css/colors/color-blue.css')}}" rel="stylesheet">

    <!-- CUSTOM CSS -->
    <link href="{{asset('frontend/css/custom.css')}}" rel="stylesheet">

@yield('header')





</head>

<body>

<div id="preloader">
    <div class="sk-spinner sk-spinner-wave">
        <div class="sk-rect1"></div>
        <div class="sk-rect2"></div>
        <div class="sk-rect3"></div>
        <div class="sk-rect4"></div>
        <div class="sk-rect5"></div>
    </div>
</div>
<!-- End Preload -->

<div class="layer"></div>
<!-- Mobile menu overlay mask -->


@include('frontend.component.header')


@yield('content')


<!-- End main -->

@include('frontend.component.footer')

<div id="toTop"></div><!-- Back to top button -->





<!-- Common scripts -->
<script src="{{asset('frontend/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{asset('frontend/js/common_scripts_min.js')}}"></script>
<script src="{{asset('frontend/js/functions.js')}}"></script>


<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ca26f1ad6b77cfd"></script>


<script>
    (function(e,t,n,i,s,a,c){e[n]=e[n]||function(){(e[n].q=e[n].q||[]).push(arguments)}
    ;a=t.createElement(i);c=t.getElementsByTagName(i)[0];a.async=true;a.src=s
    ;c.parentNode.insertBefore(a,c)
    })(window,document,"galite","script","https://cdn.jsdelivr.net/npm/ga-lite@2/dist/ga-lite.min.js");

    galite('create', 'UA-133545673-1', 'auto');
    galite('send', 'pageview');
</script>

<script>
    var EMAIL = 'siliguri.travelnext@gmail.com';


    $('#emailreplace').html(EMAIL.toLowerCase());


</script>

@yield('footer')

</body>

</html>
