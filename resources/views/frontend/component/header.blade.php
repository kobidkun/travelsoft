<!-- Header================================================== -->


<style>
    .btnheadercss{
        border: none;
        color: #000000;
        font-weight: 300;
        text-transform: lowercase!important;



    }
</style>




<header id="plain">
    <div id="top_line">
        <div class="container">
            <div class="row">
                <div class="col-6">


                    <a href="tel:{{env('PHONE')}}" style="border: none; color: #000000;font-weight: 300" class="btn_1 outline">
                        <i class="icon-phone"></i><strong>{{env('PHONE')}} / {{env('PHONE2')}}</strong>
                    </a>

                </div>
                <div class="col-6">
                    <ul id="top_links" style="font-variant: small-caps!important;">
                        <a href="mailto:{{env('EMAIL')}}"
                           style=" " class=" btnheadercss">
                            <i class="icon-email"></i>
                            <span style=" text-transform: lowercase!important;" id="emailreplace"></span>
                        </a>
                      </ul>
                </div>
            </div><!-- End row -->
        </div><!-- End container-->
    </div><!-- End top line-->

@include('frontend.component.menu')
</header><!-- End Header -->


