<footer class="">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3>Need help?</h3>
                <a href="{{env('PHONE')}}" id="phone">{{env('PHONE')}}</a>
                <a href="mailto:{{env('EMAIL')}}" id="email_footer">{{env('EMAIL')}}</a>
            </div>
            <div class="col-md-3">
                <h3>About</h3>
                <ul>
                    <li><a href="{{route('front.about')}}">Blog</a></li>
                    <a href="https://blog.travelnext.in" target="_blank" class="">Blog
                    </a>
                    <li><a href="{{route('front.contact')}}">Contact Us</a></li>

                    <li><a href="{{route('front.terms')}}">Terms & Conditions</a></li>
                    <li><a href="{{route('front.pirvacy')}}">Privacy Policy</a></li>

                </ul>
            </div>
            <div class="col-md-3">
                <h3>Discover Places</h3>
                <ul>
                    <li><a href="{{route('front.by.city','Darjeeling')}}">Darjeeling</a></li>
                    <li><a href="{{route('front.by.city','Gangtok')}}">Gangtok</a></li>
                    <li><a href="{{route('front.by.city','Bhutan')}}">Bhutan</a></li>
                    <li><a href="{{route('front.by.city','Nepal')}}">Nepal</a></li>
                    <li><a href="{{route('front.by.city','North East')}}">North East India</a></li>
                    <li><a href="{{route('front.by.city','dooars')}}">Dooars</a></li>
                    <li><a href="{{route('front.by.city','Nepal')}}">Nepal</a></li>
                    <li><a href="{{route('front.by.city','Andaman')}}">Andaman</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <h3>Member of</h3>
                <ul>
                    <li>
                        <img class="img-responsive" src="{{asset('/images/affilation/eotta.png')}}" width="200px;" alt="">
                    </li>

                    <br>

                    <li>
                        <img class="img-responsive" src="{{asset('/images/affilation/natalogo-1.png')}}" width="150px;" alt="">
                    </li>
                </ul>

            </div>
        </div><!-- End row -->
        <div class="row">
            <div class="col-md-12">
                <div id="social_footer">
                    <ul>
                        <li><a href="#"><i class="icon-facebook"></i></a></li>
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-google"></i></a></li>
                        <li><a href="#"><i class="icon-instagram"></i></a></li>
                        <li><a href="#"><i class="icon-pinterest"></i></a></li>
                        <li><a href="#"><i class="icon-vimeo"></i></a></li>
                        <li><a href="#"><i class="icon-youtube-play"></i></a></li>
                    </ul>
                    <p>© {{env('SITENAME')}} {{date('Y')}}</p>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End container -->
</footer><!-- End footer -->
