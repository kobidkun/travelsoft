{{--


<!-- Slider -->
<div class="tp-banner-container">
    <div class="tp-banner">
        <ul>

            @foreach($sliders as $slider)

            <!-- SLIDE  -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Intro Slide">
                <!-- MAIN IMAGE -->
                <img src="{{asset('frontend/img/slides_bg/dummy.png')}}" alt="{{$slider->title}}"
                     data-lazyload="{{asset('/storage/'.$slider->imgurl)}}"
                     data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                <!-- LAYER NR. 1 -->
                <div class="tp-caption white_heavy_40 customin customout text-center text-uppercase"
                     data-x="center" data-y="center" data-hoffset="0" data-voffset="-20"
                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="1000" data-start="1700" data-easing="Back.easeInOut" data-endspeed="300" style="z-index: 5; max-width: auto; max-height: auto; white-space: nowrap;">
                    {{$slider->title}}
                </div>
                <!-- LAYER NR. 2 -->
                <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0 text-center"
                     data-x="center" data-y="center" data-hoffset="0" data-voffset="15"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500" data-start="2600" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.05" data-endelementdelay="0.1" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">
                    <div style="color:#ffffff; font-size:16px; text-transform:uppercase">
                        {{$slider->desc}}</div>
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0" data-x="center" data-y="center"
                     data-hoffset="0" data-voffset="70"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-speed="500" data-start="2900" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-linktoslide="next" style="z-index: 12;"><a href='{{route('front.by.city','sikkim')}}' class="button_intro">View tours</a> <a href='{{route('front.by.city','sikkim')}}' class=" button_intro outline">Read more</a>
                </div>
            </li>

            @endforeach





        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>
<section id="search_container">
    <div id="search">
        <ul class="nav nav-tabs">
            <li><a href="#tours" data-toggle="tab" class="active show">Tours</a></li>
            <li><a href="#transfers" data-toggle="tab">Car Booking</a></li>

        </ul>

        <div class="tab-content">
            <div class="tab-pane active show" id="tours">
                <h3>Quick Tours Booking</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" id="" name="" placeholder="First Name">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" id="" name="" placeholder="Last Name">
                        </div>
                    </div>



                </div>



                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" class="form-control" id="" name="" placeholder="Phone">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" id="" name="" placeholder="Email">
                        </div>
                    </div>



                </div>
                <!-- End row -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><i class="icon-calendar-7"></i> Date</label>
                            <input class="date-pick form-control" data-date-format="M d, D" type="text">
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-3 col-6">
                        <div class="form-group">
                            <label>Adults</label>
                            <div class="numbers-row">
                                <input type="text" value="1" id="adults" class="qty2 form-control" name="adults">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-3 col-6">
                        <div class="form-group">
                            <label>Children</label>
                            <div class="numbers-row">
                                <input type="text" value="0" id="children" class="qty2 form-control" name="children">
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End row -->
                <hr>
                <button class="btn_1 green"><i class="icon-search"></i>Book now</button>
            </div>
            <!-- End rab -->

            <div class="tab-pane" id="transfers">
                <h3>Car Booking</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" id="" name="" placeholder="First Name">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" id="" name="" placeholder="Last Name">
                        </div>
                    </div>



                </div>



                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" class="form-control" id="" name="" placeholder="Phone">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" id="" name="" placeholder="Email">
                        </div>
                    </div>



                </div>
                <!-- End row -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><i class="icon-calendar-7"></i> Date</label>
                            <input class="date-pick form-control" data-date-format="M d, D" type="text">
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-3 col-6">
                        <div class="form-group">
                            <label>Adults</label>
                            <div class="numbers-row">
                                <input type="text" value="1" id="adults" class="qty2 form-control" name="adults">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-3 col-6">
                        <div class="form-group">
                            <label>Children</label>
                            <div class="numbers-row">
                                <input type="text" value="0" id="children" class="qty2 form-control" name="children">
                            </div>
                        </div>
                    </div>

                </div>
                <hr>
                <button class="btn_1 green"><i class="icon-search"></i>Book now</button>
            </div>

        </div>
    </div>
</section>
<!-- End Slider -->

--}}






<!-- End hero -->

<div id="full-slider-wrapper">
    <div id="layerslider" style="width:100%;height:700px;">
        <!-- first slide -->
        <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:5;">
            <img src="/frontend/slider/9.jpeg" class="ls-bg" alt="Slide background">
            <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Affordable Tours Queen of Hills</h3>
            <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">Tours / Hotels / Cab</p>
            <a class="ls-l button_intro_2 outline" style="top:420px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='{{route('front.by.city','Darjeeling')}}'>View Tours</a>
        </div>

        <!-- second slide -->
        <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:85;">
            <img src="/frontend/slider/10.jpg" class="ls-bg" alt="Slide background">
            <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">100+ Tour Packages to Sikkim</h3>
            <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">Tours / Hotels / Cab</p>
            <a class="ls-l button_intro_2 outline" style="top:420px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='{{route('front.by.city','Gangtok')}}'>View Tours</a>
        </div>

        <!-- third slide -->
        <div class="ls-slide" data-ls="slidedelay:5000; transition2d:103;">
            <img src="/frontend/slider/8.jpeg" class="ls-bg" alt="Slide background">
            <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Unforgettable Tours to North East India </h3>
            <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">Tours / Hotels / Cab</p>
            <a class="ls-l button_intro_2 outline" style="top:420px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='{{route('front.by.city','North East')}}'>View Tours</a>
        </div>

        <!-- fourth slide -->
        <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:14;">
            <img src="/frontend/slider/7.jpeg" class="ls-bg" alt="Slide background">
            <h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Best Travel Destination to Bhutan</h3>
            <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">Tours / Hotels / Cab</p>
            <a class="ls-l button_intro_2 outline" style="top:420px; left:50%;white-space: nowrap;" data-ls="durationin:2000;delayin:1400;easingin:easeOutElastic;" href='{{route('front.by.city','Bhutan')}}'>View Tours</a>
        </div>

    </div>
</div>


