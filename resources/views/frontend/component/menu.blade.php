<style>
    .main-menu > ul > li > a{
        font-weight: 600!important;
        size: 63px!important;
    }
</style>



<div class="container">
    <div class="row">
        <div class="col-3">
            <div id="logo_home">
                <h1><a href="{{route('front.home')}}" title="{{env('SITENAME')}}">{{env('SITENAME')}}</a></h1>
            </div>
        </div>
        <nav class="col-9">
            <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
            <div class="main-menu">
                <div id="header_menu">

                </div>
                <a href="#" class="open_close" id="close_in"><i class="icon_set_1_icon-77"></i></a>
                <ul class="menu-style-cystom">
                    <li class="submenu">
                        <a href="{{route('front.home')}}" class="show-submenu">
                            Home
                        </a>

                    </li>

                    <li class="submenu">
                        <a href="{{route('front.about')}}" class="">About
                        </a>

                    </li>


                    <li class="submenu">
                        <a href="/" class="show-submenu">Popular Tours <i class="icon-down-open-mini"></i></a>
                        <ul>
                            <li><a href="{{route('front.all')}}">All tours list</a></li>

                        </ul>
                    </li>


                    <li class="submenu">
                        <a href="javascript:void(0);" class="show-submenu">Popular Destinations <i class="icon-down-open-mini"></i></a><ul>
                            <li><a href="{{route('front.by.city','Darjeeling')}}">Darjeeling</a></li>
                            <li><a href="{{route('front.by.city','Gangtok')}}">Gangtok</a></li>
                            <li><a href="{{route('front.by.city','Bhutan')}}">Bhutan</a></li>
                            <li><a href="{{route('front.by.city','Nepal')}}">Nepal</a></li>
                            <li><a href="{{route('front.by.city','North East')}}">North East India</a></li>
                            <li><a href="{{route('front.by.city','dooars')}}">Dooars</a></li>
                            <li><a href="{{route('front.by.city','Nepal')}}">Nepal</a></li>
                            <li><a href="{{route('front.by.city','Andaman')}}">Andaman</a></li>

                        </ul>
                    </li>

                    <li class="submenu">
                        <a href="{{route('front.car.all')}}" class="">Car Rentals
                            </a>

                    </li>
                    <li class="submenu">
                        <a href="https://blog.travelnext.in" target="_blank" class="">Blog
                        </a>

                    </li>


                    <li class="submenu">
                        <a href="{{route('front.contact')}}" class="">Contact
                            </a>

                    </li>





                </ul>
            </div><!-- End main-menu -->

        </nav>
    </div>
</div><!-- container -->
